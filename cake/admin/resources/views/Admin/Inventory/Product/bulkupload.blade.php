
<title>Product Bulk Upload </title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product Bulk Upload
   </h1>
   <ol class="breadcrumb">
      <a href="{{route('product.index')}}" class="btn btn-primary">View Product </a>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Create Product</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{URL::to('bulkupload')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="box-body">

               <div class="col-md-6">
                  <div class="form-group">
                     <label>Upload File</label>
                     <input type="file" class="form-control" name="file" >
                  </div>
               </div>               
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Upload</button>
            </div>
         </form>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection
<script type="text/javascript">
   function categorys(element){
    var category = $(element).val();    
    if(category){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_category')}}/"+category+'/edit',
           success:function(res){               
            if(res){
                $("#sub_service").empty();
                $("#sub_service").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#sub_service").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">
   function subcategoryss(element){
    var subcategorys = $(element).val();    
    if(subcategorys){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_sub_category')}}/"+subcategorys+'/edit',
           success:function(res){               
            if(res){
                $("#subsubcategory").empty();
                $("#subsubcategory").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#subsubcategory").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>