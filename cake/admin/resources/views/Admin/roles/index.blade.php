
<title>{{'Dashboard'}}</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
            <h1 class="m-0 text-dark">Role Management</h1>
          <ol>
                 @can('role-create')
                    <a class="btn btn-success" href="{{ route('roles.create') }}" style="float: right;"> Create New Role</a>
                  @endcan
          </ol>
</section>
<section class="content">
   <div class="row">
      <div class="col-xs-12">
         <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sno</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                            @can('role-edit')
                                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                            @endcan
                            @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                              <!--   {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!} -->
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
         </div>
      </div>
   </div>
</section>

@stop