    <script src="{{URL::To('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{URL::To('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::To('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{URL::To('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{URL::To('dist/js/adminlte.min.js')}}"></script>
    <script src="{{URL::To('dist/js/demo.js')}}"></script>
    <script src="{{URL::To('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::To('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::To('bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{URL::To('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{URL::to('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{URL::to('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script> 
    <script src="{{URL::to('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{URL::to('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{URL::to('plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script src="{{URL::to('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
    <script src="{{URL::to('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
    <!-- Select2 -->
    <script src="{{URL::to('bower_components/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- Datatables -->
   <script type="text/javascript" src="{{url::to('/js/dataTables.buttons.min.js')}}"></script>
   <script type="text/javascript" src="{{url::to('js/buttons.flash.min.js')}}"></script>
  <script type="text/javascript" src="{{url::to('js/jszip.min.js')}}"></script>
  <script type="text/javascript" src="{{url::to('js/pdfmake.min.js')}}"></script>
  <script type="text/javascript" src="{{url::to('js/vfs_fonts.js')}}"></script>
  <script type="text/javascript" src="{{url::to('js/buttons.html5.min.js')}}"></script>
  <script type="text/javascript" src="{{url::to('js/buttons.print.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script>
  $(function () {
        //Initialize Select2 Elements
    $('.select2').select2()
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
  })
</script>

   <script type="text/javascript">
  $(document).ready(function() {
    $('#example12').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    } );
} );
  </script>
  
<script>
  $(function () {
    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
  })
</script>
<script>
  $(function () {
    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
  })
</script>

<script>
  $(function () {
    $('.textarea').wysihtml5()
  })
</script>

@include('layouts.notification')