<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

Route::get('/', 'website\WebsiteController@index')->name('home');
Route::get('/about', 'website\WebsiteController@about');
Route::get('/contact', 'website\WebsiteController@contact');
Route::post('/contact', 'website\WebsiteController@contact_store');
Route::get('/term_and_condition', 'website\WebsiteController@term_and_condition');
Route::get('/return_policies', 'website\WebsiteController@return');
Route::get('/policies', 'website\WebsiteController@policies');
	
/*auth routes*/
Route::get('/user_login', 'website\login\Login_registrationController@user_login');
Route::get('/user_register', 'website\login\Login_registrationController@user_register');
Route::post('/register', 'website\login\Login_registrationController@register');
Route::post('/send_otp', 'website\login\Login_registrationController@login');
Route::post('/verification', 'website\login\Login_registrationController@otp_verification');
Route::get('auth/logout','website\login\Login_registrationController@logout');


/*product list*/
Route::get('/products/{id}', 'website\ProductListController@store');
Route::get('/productsearch/{id?}', 'website\ProductListController@store');

Route::get('/products_subcat/{id}', 'website\ProductListController@sub_categories');
Route::get('/productscat/{id}', 'website\ProductListController@categories');
Route::get('/sub_cat/{id}', 'website\ProductListController@sub_cat');
Route::get('/child/{id}', 'website\ProductListController@child');
Route::get('/products', 'website\ProductListController@products');
Route::post('/colors', 'website\ProductListController@color');

/*products description*/
Route::get('/specification/{id}', 'website\SpecificationController@single_product');
Route::get('amount/{id}','website\SpecificationController@amount')->name('amount');

/*wishlist*/
Route::get('/wishlist/{id}', 'website\WishlistController@store')->name('wishlist');
Route::get('/wishlistview', 'website\WishlistController@view')->name('wishlistview');
Route::get('/deletewishlist/{id}','website\WishlistController@delete');

/*cart*/
Route::post('addtocart','website\CartController@store')->name('addtocart');	
Route::post('update/cart', 'website\CartController@update_cart');
Route::get('deletecart/{id}','website\CartController@delete');
Route::get('addtocartview','website\CartController@view')->name('addtocartview');

Route::get('update/image', 'website\ProductListController@image');

/*checkout*/
Route::get('/checkoutview', 'website\CheckoutController@checkout')->name('checkoutview');
Route::post('/checkoutstore', 'website\CheckoutController@store');
	
/*razorpay*/
Route::post('checkoutpayment','website\CheckoutController@checkoutpayment');
Route::post('razorpay','website\RazorPayController@start_payment');
Route::post('payment','website\RazorPayController@complete');

/*my Account*/
Route::get('myaccount','website\MyaccountController@index');
Route::post('myaccountstore','website\MyaccountController@store');
Route::get('/bill/{id}', 'website\MyaccountController@bill');










