@extends('layouts.app')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner bg_cover" style="background-image: url(assets/images/page-banner-4.jpg);">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Register</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">register</li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Login Register Start ======-->

    <section class="login-register-area pt-75 pb-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title">Register Your Account</h4>

                        <div class="login-register-form">
                            <form action="{{ URL::to('register') }}" method="POST">
                                @CSRF
                                <div class="single-form">
                                    <label>Name *</label>
                                    <input type="text" name="name">
                                </div>
                                <div class="single-form">
                                    <label>Email *</label>
                                    <input type="email" name="email">
                                </div>

                                <div class="single-form">
                                    <label>mobile *</label>
                                    <input type="text" name="mobile">
                                </div>
                                <div class="single-form">
                                    <button type="submit" class="main-btn btn-block">Submit</button>
                                </div>
                                <div class="single-form">
                                    <label>have account ?</label>
                                    <a href="{{ URL::to('user_login') }}" class="main-btn main-btn-2 btn-block">Go to Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>

    <!--====== Login Register Ends ======-->
@endsection
