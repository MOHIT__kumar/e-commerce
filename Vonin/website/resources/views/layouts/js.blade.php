    <!--====== Jquery js ======-->
    <script src="{{ URL::to('assets\js\vendor\jquery-3.5.1.min.js') }}"></script>
    <script src="{{ URL::to('assets\js\vendor\modernizr-3.7.1.min.js') }}"></script>
    
    <!--====== All Plugins js ======-->
    <!-- <script src="{{ URL::to('assets/js/plugins/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/popper.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/slick.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/modernizr.custom.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.dlmenu.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.paroller.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.countdown.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/select2.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/photoswipe.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.elevateZoom.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/masonry.pkgd.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/jquery.sticky-sidebar.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/plugins/ajax-contact.js') }}"></script>
 -->
    <script src="{{ URL::to('assets\js\plugins\parallax.min.js') }}"></script>

    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

    <script src="{{ URL::to('assets\js\plugins.min.js') }}"></script>

    
    <!--====== Main Activation  js ======-->
    <script src="{{ URL::to('assets\js\main.js') }}"></script>

     <!-- snackbar -->
    <script src="{{URL::TO('assets/snackbar/snackbar.min.js')}}"></script>
    <script src="{{URL::to('assets/snackbar/custom-snackbar.js')}}"></script>

     <!--====== Google Map js ======-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQ5y0EF8dE6qwc03FcbXHJfXr4vEa7z54"></script>
    <script src="{{URL::to('assets\js\map-script.js')}}"></script>

    