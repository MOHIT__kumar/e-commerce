
    <footer class="footer-area pt-50 pb-55">
        <div class="footer-widget">
            <div class="container footer-container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer-logo-copyright mt-30">
                            <a href="{{URL::to('/')}}">
                                <img src="{{URL::to('assets\images\w-vonin.png')}}" style="width: 130px" alt="Logo" />
                                <!-- <h2 class="text-white">VONiN</h2> -->
                            </a>
                            <p>
                                &copy; Designed and Developed by
                                <a href="https://www.techosoft.in/">Techosoft Technologies</a>
                            </p>
                        </div>
                        <div class="footer-social mt-30">
                            <ul class="social">
                                <li>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="footer-link-wrapper flex-wrap">
                            <div class="footer-link mt-30">
                                <h5 class="footer-title">Useful Link</h5>
                                <ul class="link">
                                    <li><a href="{{URL::to('return_policies')}}">Return and Refund Policies</a></li>
                                    <li><a href="{{URL::to('policies')}}">Privacy Policies</a></li>
                                    <li><a href="{{URL::to('term_and_condition')}}">Terms & Conditions</a></li>
                                </ul>
                            </div>
                            <div class="footer-link mt-30">
                                <h5 class="footer-title">Company</h5>
                                <ul class="link">
                                    <li><a href="{{URL::to('about')}}">About Us</a></li>
                                    <li><a href="{{URL::to('contact')}}">Contact Us</a></li>
                                </ul>
                            </div>
                            <div class="footer-link mt-30">
                                <h5 class="footer-title">Profile</h5>
                                <ul class="link">
                                    <li><a href="{{URL::to('myaccount')}}">My Account</a></li>
                                    <li><a href="{{URL::to('checkoutview')}}">Checkout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--====== Footer Ends ======