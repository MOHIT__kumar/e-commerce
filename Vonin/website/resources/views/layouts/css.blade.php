
    <meta charset="utf-8">
    
    <!--====== Title ======-->
    <title>Vonin E-commerce</title>
    
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ URL::to('assets\images\favicon.png') }}" type="image/png">


    <!-- CSS
    ============================================ -->

    <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->

    <!-- <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/default.css') }}">
 -->

    <!--===== Plugins CSS (All Plugins Files) =====-->
    <!-- <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/component.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/select2.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/photoswipe.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/plugins/photoswipe-default-skin.css') }}">
     -->
    
    <!--====== Main Style CSS ======-->
    <!-- <link rel="stylesheet" href="{{ URL::to('assets/css/style.css') }}"> -->

    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

    <link rel="stylesheet" href="{{ URL::to('assets\css\vendor\plugins.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets\css\style.min.css') }}"> 

    <!-- snackbar -->
    <link href="{{URL::to('assets/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css" />