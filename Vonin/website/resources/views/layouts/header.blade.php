<head>
  <style>
    @media(max-width: 990px){
    .show-icon{
      margin-top: -10px;
    position: absolute;
    }  
    }
    @media(min-width: 995px){
         .show-icon{
           margin-top: -25px;
         position: relative;
         /*float: right;*/
         width:40%;
         margin-left:auto;
         }
    }
    
  </style>
</head>

 <!--====== Header Start ======-->
    <header class="header-area">
      <div class="header-navbar">
        <div class="container">
          <div class="header-logo mt-3 text-center">
            <a href="{{URL::to('/')}}">
              <img src="{{URL::to('assets\images\vonin2.png')}}"class="pb-2 pb-md-0 pb-lg-0" style="width: 130px"  alt="Logo" />
              <!-- <h2>VONiN</h2> -->
            </a>
          </div>
          <div class="header-meta d-lg-block show-icon">
            <ul class="meta">
                @guest  
              <li>
                <a class="" href="{{URL::to('addtocartview')}}">
                  <!-- <i class="far fa-Shopping-cart"></i> -->
                  <i class="far fa-Shopping-bag"></i>
                  
                </a>
              </li>
              <li>
                <a class="" href="{{URL::to('wishlistview')}}">
                  <i class="far fa-heart"></i>
                </a>
              </li>
                @else
    @php
    $count = DB::table('wishlists')->where('user_id',Auth::user()->id)->count();
    $cart = DB::table('carts')->where('auth_user_id',Auth::user()->id)->count();
    @endphp 
                <li>
                <a class="" href="{{URL::to('addtocartview')}}">
                  <i class="far fa-Shopping-bag"></i>
                @if($cart>0)
                  <span>{{$cart}}</span>
                @else
                @endif
                </a>
              </li>
              <li>
                <a class="" href="{{URL::to('wishlistview')}}">
                  <i class="far fa-heart"></i>
                  @if($count>0)
                  <span>{{$count}}</span>
                @else
                @endif
                </a>
              </li>

                @endguest
              <li>
                <form action="{{URL::to('products')}}" method="get">
                <a class="search-toggle" href="javascript:void(0)">
                  <i class="far fa-search"></i>
                </a>
              </form>
              </li>
              <li>
                <a class="sidebar-toggle" href="javascript:void(0)">
                  <i class="fal fa-bars"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="header-wrapper d-flex justify-content-between align-items-center"
          >
            <div class="header-menu site-nav d-none d-lg-flex m-auto">
              <ul class="main-menu">
                @php
                  $categories=DB::table('categories')->get(); 
                @endphp
                  
                @foreach($categories as $cat)

                <li class="static">
                  <a href="{{URL::to('productscat',$cat->id)}}">{{$cat->category}}</a>
                  <ul class="sub-menu sub-mega-menu flex-wrap">
                    @php 
                      $parent = DB::table('sub__categories')->where('category_id',$cat->id)->get(); 
                    @endphp

                    @foreach($parent as $sub)
                    <li>
                      <a class="menu-title" href="{{URL::to('sub_cat',$sub->id)}}">{{$sub->sub_category}}</a>
                      <ul class="sub-mega-item">
                      @php 
                        $child = DB::table('sub__sub__categories')->where('sub_category_id',$sub->id)->get();
                      @endphp 
                      @foreach($child as $sub_sub)
                        <li><a href="{{URL::to('child',$sub_sub->id)}}">{{$sub_sub->sub_sub_category}}</a></li>
                      @endforeach
                    </li>
                      </ul>
                      @endforeach
                      </ul>
                  </li>
                @endforeach
                <li><a href="{{URL::to('about')}}">About Us </a></li>
                <li><a href="{{URL::to('contact')}}">Contact</a></li>
                <li><a href="{{URL::to('myaccount')}}">My Account</a></li>
              </ul>
            </div>

          </div>
        </div>
        <div id="dl-menu" class="dl-menuwrapper d-lg-none">
          <button class="dl-trigger"></button>
          <ul class="dl-menu">
            @php
              $categories=DB::table('categories')->get(); 
            @endphp

            @foreach($categories as $cat)
            <li>
              <a href="#">{{$cat->category}}</a>
              <ul class="dl-submenu">
                @php 
                  $parent = DB::table('sub__categories')->where('category_id',$cat->id)->get(); 
                @endphp
                @foreach($parent as $sub)
                <li>
                  <a href="#">{{$sub->sub_category}}</a>
                    @php 
                      $child = DB::table('sub__sub__categories')->where('sub_category_id',$sub->id)->get();
                    @endphp
                    
                  <ul class="dl-submenu">
                      @foreach($child as $sub_sub)
                    <li>
                      <a href="{{URL::to('child',$sub_sub->id)}}">{{$sub_sub->sub_sub_category}}</a>
                    </li>
                    @endforeach
                  </ul>
                    
                </li>
                @endforeach
              </ul>
            </li>
            @endforeach
              <li><a href="{{URL::to('about')}}">About Us </a></li>
                <li><a href="{{URL::to('contact')}}">Contact</a></li>
                <li><a href="{{URL::to('myaccount')}}">My Account</a></li>
          </ul>
        </div>
      </div>
    </header>
    <!--====== Header Ends ======-->

     <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i
        ></a>
            <div class="search-form">
                <label>Start typing and press Enter to search</label>
                <div class="search-input">
                    <form action="{{URL::to('productsearch')}}">
                        <input type="text" placeholder="Search entire store…" name="search" />
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    <!--====== Off Canvas Cart Start ======-->


    <!--====== Off Canvas Cart Ends ======-->

    <!--====== Off Canvas Sidebar Start ======-->
    
    <!--====== Off Canvas Sidebar Ends ======-->