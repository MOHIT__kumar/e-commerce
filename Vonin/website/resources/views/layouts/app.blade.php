<!DOCTYPE html>
<html>
<head>
    @include('layouts.css')
</head>
<body>
    <!--====== preloader start ======-->
        <div class="preloader">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    <!--====== preloader Ends ======-->

    @include('layouts.header')

        @yield('content')

    @include('layouts.footer')

    @include('layouts.js')
    @yield('page-js')
    @yield('product-color-js')

    @yield('addtocart-page-js')
    @yield('checkoutpage-js')

    @include('layouts.notification')

</body>
</html>