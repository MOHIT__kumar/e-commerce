@extends('layouts.app')
@section('content')
    <!--====== Page Banner Start ======-->
    <section class="page-banner bg_cover" style="background-image:url('page-banner.jpg')">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Shop Sidebar</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
                    <li class="breadcrumb-item active">Shop</li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Shop Page Start ======-->

    <section class="shop-page pt-30 pb-80">
        <div class="container-fluid shop-container">
            <div class="row flex-md-row-reverse justify-content-between">
                <div class="col-lg-12">
                    <div class="shop-product">
                        <div class="row px-3">
                          @foreach($products as $data)
                            <div class="product-col col-xl-3 col-lg-4 col-sm-6 mb-25">
                                <div class="single-product">
                                    <div class="product-image">
                                          <div class="image">
                                        <img class="product-1" id="image{{$data->id}}" src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}"/>
                                        <a class="link" href="{{URL::to('specification',$data->id)}}"></a>
                                    </div>
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <h4 class="title">
                                            <a href="{{URL::to('specification',$data->id)}}">{{$data->title}}
                                            </a>
                                            </h4>
                                        </div>
                                       <!-- <div class="product-price">
                                            <span class="price">
                                                <div>
                                                    <button id="clothing1{{$data->id}}"
                                                onclick="change1('1{{$data->id}}')"
                                                      class="choose-col"
                                                      style="background-color: red"
                                                    ></button>
                                                    <button id="clothing2{{$data->id}}"
                                                onclick="change2('2{{$data->id}}')"
                                                      class="choose-col"
                                                      style="background-color: blue"
                                                    ></button>
                                                    <button id="clothing3{{$data->id}}"
                                                onclick="change3('3{{$data->id}}')"
                                                    class="choose-col"
                                                    style="background-color: green"
                                                    ></button></div
                                                ></span>
                                              </div>-->
                                          </div>
                                      </div>
                                  </div>
                                @endforeach
                            </div>
                        </div>
                         {{ $products->links() }}

            </div>
        </div>
    </div>
    </section>

    <!--====== Shop Page Ends ======-->
@endsection

@section('product-color-js')
 <script>
   function change1(id)
   {
    var r  = '#clothing'+id;

    var color = $( r ).css( "background-color" );
    // console.log(img_id)

    $.ajax({
            url: "{{URL::TO('colors')}}",
            type: 'post',
            dataType: 'json',
            data: { 
                    "_token": "{{ csrf_token() }}",
                    color_id    : id,
                    color       : color,
            },

            success: function (data) 
            {
                if(id.length == 2){
                    var img_id = "";
                    var i;
                        for (i = 1; i < data['color_id'].length; i++) {
                            img_id = data['color_id'] % 10;
                            }
                    }

                else{
                        var img_id = "";
                        var i;

                        for (i = 1; i < id.length; i++) {
                        img_id = id % 100;
                        }  
                    } 

                $("#image"+img_id).attr('src',"{{Config::get('app.admin_url')}}/img/product/"+data['data']);
            },
            error: function () {
                alert('Product is not available in this color');
            }
        });
   }

    function change2(id)
   {
    var g  = '#clothing'+id;

    var color = $( g ).css( "background-color" );
    // console.log(id)

    $.ajax({
            url: "{{URL::TO('colors')}}",
            type: 'post',
            dataType: 'json',
            data: { 
                    "_token": "{{ csrf_token() }}",
                    color_id    : id,
                    color       : color,
            },

            success: function (data) 
            {
                if(id.length == 2){
                    var img_id = "";
                    var i;
                        for (i = 1; i < data['color_id'].length; i++) {
                            img_id = data['color_id'] % 10;
                            }
                    }

                else{
                        var img_id = "";
                        var i;

                        for (i = 1; i < id.length; i++) {
                        img_id = id % 100;
                        }  
                    } 

                $("#image"+img_id).attr('src',"{{Config::get('app.admin_url')}}/img/product/"+data['data']);
            },
            error: function () {
                alert('Product is not available in this color');
            }

        });
   }

    function change3(id)
   {
    var b  = '#clothing'+id;

    var color =$( b ).css( "background-color" );
    // console.log(id)

    $.ajax({
            url: "{{URL::TO('colors')}}",
            type: 'post',
            dataType: 'json',
            data: { 
                    "_token": "{{ csrf_token() }}",
                    color_id    : id,
                    color       : color,
            },

            success: function (data) 
            {
                if(id.length == 2){
                    var img_id = "";
                    var i;
                        for (i = 1; i < data['color_id'].length; i++) {
                            img_id = data['color_id'] % 10;
                            }
                    }

                else{
                        var img_id = "";
                        var i;

                        for (i = 1; i < id.length; i++) {
                        img_id = id % 100;
                        }  
                    } 
                    
                $("#image"+img_id).attr('src',"{{Config::get('app.admin_url')}}/img/product/"+data['data'] );
            },
            error: function () {
                alert('Product is not available in this color');
            }

        });
   }
 </script>

@endsection
