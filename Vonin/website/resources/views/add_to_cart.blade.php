@extends('layouts.app')
@section('content')
	<!--====== Page Banner Start ======-->
		@php
				$subtotal=0;
		@endphp
    <section
      class="page-banner bg_cover"
      style="background-image: url(assets/images/page-banner-4.jpg)">
      <div class="container">
        <div class="page-banner-content text-center">
          <h2 class="title">Cart</h2>
          <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Cart</li>
          </ol>
        </div>
      </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Cart Start ======-->

    <section class="cart-page pt-80 pb-80">
      <div class="container">
      	<div class="row">
        <div class="cart-table table-responsive col-lg-9">
          <table class="table">
            <thead>
              <tr>
                <th class="delete"></th>
                <th class="product">Product</th>
                <!--<th class="price">color</th>-->
                <th class="price">Price</th>
                <th class="quantity">Quantity</th>
                <th class="Total">Total</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($carts as $data)
              <tr>
                <td class="delete">
                  <a class="product-delete" href="{{URL::To('deletecart',$data->id)}}"
                    ><i class="far fa-trash-alt"></i
                  ></a>
                </td>
                <td class="product">
                  <div class="cart-product">
                    <div class="product-image">
                      <img src="\img\product\{{$data->main_image}}" alt="" />
                    </div>
                    <div class="product-content">
                      <h5 class="title">
                        {{$data->product_name}}
                      </h5>
                    </div>
                  </div>
                </td>
                <td class="price" id="price{{$data->id}}">
                  <p class="cart-price">{{$data->price/$data->qty}}</p>
                </td>
                <td class="quantity">
                  <div class="product-quantity d-inline-flex"  onclick="plus('{{$data->id}}')">
                    <button type="button" class="sub">
                      <i class="fal fa-minus"></i>
                    </button>
                    <input type="text" min="1" max="100" step="1" value="{{$data->qty}}" id="qty{{$data->id}}"/>
                    <button type="button" class="add">
                      <i class="fal fa-plus"></i>
                    </button>
                  </div>
                </td>
                <td class="Total" id="total{{$data->id}}">
                  <p class="cart-price">{{$data->price}}</p>
                </td>
                <input type="hidden" id="subtotal" name="subtotal" value="{{$subtotal+=$data->price}}">
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      
          <div class="col-lg-3">
              <div class="cart-title">
                <h4 class="title">Cart totals</h4>
              </div>
              <div class="cart-total-table mt-25">
                <table class="table">
                  <tbody>
                    <tr>
                      <td>Subtotal</td>
                      <td><div id="subtotalamount">{{$subtotal}}</div></td>
                    </tr>
                    <tr>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="cart-total-btn mt-20">
                <a href="{{URL::To('checkoutview')}}" class="main-btn btn-block">Proceed To Checkout</a>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!--====== Cart Ends ======-->

@endsection

@section('addtocart-page-js')

 <script>
   function plus(id)
   {
    var p = '#price'+id;
    var q   = '#qty'+id;
    var t  = "#total"+id;
    var price = $(p).text();
    var qty  = $(q).val();
    $(t).text(price * qty);
      // console.log(qty)
    var ids = [];
    @foreach($carts as $data)
      ids.push("{{$data->id}}");
    @endforeach

    var length=ids.length;
    var total_amount = 0;
    for(i=0; i<length; i++){
      var total = "#total"+ids[i];
      
      total_amount = total_amount + parseInt($(total).text());
    }
    $('#subtotalamount').text(total_amount);
    // console.log(total_amount );

    $.ajax({
                url: "{{URL::TO('update/cart')}}",
                type: 'post',
                dataType: 'json',
                data: { 
                        "_token": "{{ csrf_token() }}",
                        id          : id,
                        qty         : qty,
                        price       : price,
                },
        });
    }
 </script>
@endsection