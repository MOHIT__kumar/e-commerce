@extends('layouts.app')
@section('content')
<!--====== Page Banner Start ======-->
<section class="page-banner bg_cover" style="background-image: url('../assets/images/page-banner.jpg')">
   <div class="container">
      <div class="page-banner-content text-center">
         <h2 class="title">Product description</h2>
         <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
            <li class="breadcrumb-item">
               <a href="shop-sidebar.html">Shop</a>
            </li>
            <li class="breadcrumb-item active">Check out</li>
         </ol>
      </div>
   </div>
</section>
<!--====== Page Banner Ends ======-->
<!--====== Product Simple Start ======-->
<section class="product-simple-area pt-20">
   <div class="container">
   <div class="row">
      <div class="col-lg-6">
         <div class="product-simple-image mt-50">
            <div class="product-simple-preview-image">
               <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="" />
            </div>
            @foreach($images as $img)
            <div class="product-simple-preview-image">
               <img src="{{Config::get('app.admin_url')}}/img/product/{{$img->files}}" alt="" />
            </div>
            @endforeach
         </div>
      </div>
      <div class="col-lg-6">
         <div class="product-simple-details product-simple-sticky mt-50">
            <h4 class="title">{{$data->title}}</h4>
            <div class="product-price">
               <span class="price" id="amount">₹ {{$productsizes->amount}}</span>
            </div>
            <div class="product-quantity-cart-wishlist-compare flex-wrap">
              <div class="row  w-100">
              <form action="{{URL::to('addtocart')}}" class="col-lg-7" method="post">
                  @CSRF
                  <div class="product-cart">
                     <input type="number" id="qty" class="form-control" value="1" min="1" max="10" step="1" data-decimals="0" name="qty" required="">
                  </div>
                  <div class="product-cart">
                     <div class="select-custom">
                        <select name="size" id="size" class="form-control">
                           @foreach($size as $size)
                           <option value="{{$size->id}}" name="{{$size->size}}">{{$size->size}}</option>
                           @endforeach
                           <!-- <option id="customize">customize</option> -->
                        </select><br>
                        <div class="customize"></div>
                     </div>
                     <div class="product-cart">
                        <input type="hidden" name="product_id" value="{{$data->id}}">
                        <input type="hidden" name="name" value="{{$data->title}}">
                        <div class="product-price">
                           <input type="hidden" name="amount" id="amt" value="{{$productsizes->amount}}">
                        </div>
                        <div class="row">
                           @guest
                           <a href="{{URL::to('user_login')}}" class="main-btn">Add to Cart</a>
                           @else
                           <button type="submit" class="main-btn" style="margin-top: 20px;">Add to Cart</button>
                           @endguest
                           <a href="{{URL::to('wishlist',$data->id)}}" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" class="product-wishlist"><i class="far fa-heart"></i></a>
                        </div>
                     </div>
                 </div>
               </form>
               
              </div>
               <div class="product-description">
                  <h4>Product Information</h4>
                  <br>
                  {!!html_entity_decode ($data->description)!!}
                  <div class="product-meta">
                     <p>Categories: <a href="#">Metro,</a> <a href="">
                        @if($data->category_id == 2)
                        Men
                        @elseif($data->category_id == 1)
                        Women
                        @else
                        Kids
                        @endif
                        </a>
                     </p>
                     <p>
                        Tags: <a href="#">Dress,</a> <a href="#">Fashion</a>
                        <a href="#">Furniture,</a> <a href="#">Lookbook</a>
                     </p>
                  </div>
                  <div class="product-share">
                     <ul class="social">
                        <li>
                           <a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                           <a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                           <a href="#"><i class="fab fa-linkedin"></i></a>
                        </li>
                        <li>
                           <a href="#"><i class="fab fa-pinterest-p"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--====== Product Simple Ends ======-->
<!--====== Related Products Start ======-->
<section class="product-area pt-145 pb-145">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-lg-10">
            <div class="section-title text-center">
               <h2 class="title">Related Products</h2>
            </div>
         </div>
      </div>
      <section class="category-area pt-50">
         <div class="container-fluid custom-container">
            <div class="row">
               @foreach($products as $data)
               <div class="col-md-4">
                  <div class="category-item-2">
                     <a href="{{URL::to('specification',$data->id)}}">
                        <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="" />
                        <!-- <div class="category-content">
                           <h4 class="title">Man</h4>
                           </div> -->
                     </a>
                  </div>
                  <br>
               </div>
               @endforeach
            </div>
         </div>
      </section>
   </div>
</section>
<!--====== Related Products Ends ======-->
@endsection
@section('page-js')
<script type="text/javascript">
   $(document).ready(function() {
       // alert('working');
       $("#size").change(function() {
           var id = $("#size").val();
                      // console.log(id);
              if (id == 4)
            {
              // alert('cvb');
              $(".customize").append(
                '<input type="text" class="form-control" placeholder="Enter Your Breast size in inches" style="width: 170%; margin-bottom:1rem" name="n[]" multiple="" required>',
                '<input type="text" class="form-control" style="width: 170%; margin-bottom:1rem" placeholder="Enter Your waist size in inches" name="n[]" multiple="" required>',
                '<input type="text" class="form-control" placeholder="Enter Your height in inches like 5.4 inches" style="width: 170%; margin-bottom:1rem" name="n[]" multiple="" required>');

                  $.ajax({
                   type: "GET",
                   url: "{{URL::To('amount')}}" + '/' + id,
                   success: function(result) {
                       $("#amount").empty();
                       $.each(result, function(value) {
                           $("#amount").append('₹' + value);
                           $("#amt").empty();
                           $("#amt").append('<input type="hidden" id="amount" name="amount" value="' + value + '">');
                           // alert(amount);
                       });
                   }
               });
            }
            else{
                $(".customize").hide();
               $.ajax({
                   type: "GET",
                   url: "{{URL::To('amount')}}" + '/' + id,
                   success: function(result) {
                       $("#amount").empty();
                       $.each(result, function(value) {
                           $("#amount").append('₹' + value);
                           $("#amt").empty();
                           $("#amt").append('<input type="hidden" id="amount" name="amount" value="' + value + '">');
                           // alert(amount);
                       });
                   }
               });
         }
       });
   });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
   var slideIndex = 1;
   showSlides(slideIndex);
   
   function plusSlides(n) {
     showSlides(slideIndex += n);
   }
   
   function currentSlide(n) {
     showSlides(slideIndex = n);
   }
   
   function showSlides(n) {
     var i;
     var slides = document.getElementsByClassName("mySlides");
     var dots = document.getElementsByClassName("dot");
     if (n > slides.length) {slideIndex = 1}    
     if (n < 1) {slideIndex = slides.length}
     for (i = 0; i < slides.length; i++) {
         slides[i].style.display = "none";  
     }
     for (i = 0; i < dots.length; i++) {
         dots[i].className = dots[i].className.replace(" active", "");
     }
     slides[slideIndex-1].style.display = "block";  
     dots[slideIndex-1].className += " active";
   }
</script>
@endsection