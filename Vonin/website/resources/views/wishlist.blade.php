@extends('layouts.app')
@section('content')
    <!--====== Page Banner Start ======-->
        @php
                $subtotal=0;
        @endphp
    <section
      class="page-banner bg_cover"
      style="background-image: url(assets/images/page-banner-4.jpg)"
    >
      <div class="container">
        <div class="page-banner-content text-center">
          <h2 class="title">Wishlist</h2>
          <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Wishlist</li>
          </ol>
        </div>
      </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Cart Start ======-->

    <section class="cart-page pt-80 pb-80">
      <div class="container">
        <div class="cart-table table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="product">S no.</th>
                <th class="product">Image</th>
                <th class="product">Product</th>
                <th class="quantity">Action</th>
              </tr>
            </thead>
            <tbody>
              @php $i=1; @endphp
                @foreach($wish as $data)
              <tr>
                <td>
                  {{$i++}}
                </td>
                <td class="product">
                  <div class="cart-product">
                    <div class="product-image">
                      <a href="{{URL::to('specification',$data->id)}}">
                      <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="" />
                    </a>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="cart-product">
                    <div class="product-content">
                      <h5 class="title">
                        <a href="{{URL::to('specification',$data->id)}}"
                          >{{$data->title}}</a
                        >
                      </h5>
                    </div>
                  </div>
                </td>

                <td class="delete">
                  <a class="product-delete" style="    margin-left: 45%;" href="{{URL::To('deletewishlist',$data->id)}}"
                    ><i class="far fa-trash-alt"></i
                  ></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>

    <!--====== Cart Ends ======-->   

@endsection
