@extends('layouts.app')
@section('content')
	
    <!--====== Page Banner Start ======-->

    <section
      class="page-banner bg_cover"
      style="background-image: url(assets/images/page-banner-4.jpg)"
    >
      <div class="container">
        <div class="page-banner-content text-center">
          <h2 class="title">Checkout</h2>
          <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Checkout</li>
          </ol>
        </div>
      </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Checkout Start ======-->

    <section class="checkout-page pt-50 pb-80">
      <div class="container">

        <form action="{{URL::to('checkoutstore')}}" method="post">
        	@csrf
          <h4>Step 1:</h4>
          <div class="row">
            <div class="col-lg-7">
              <div class="checkout-form mt-30">
                <div class="checkout-title">
                  <h4 class="title">Billing details</h4>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>First name *</label>
                      <input type="text" name="fname" value="{{$address->name}}" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Last name *</label>
                      <input type="text" name="lname" />
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>Street address *</label>
                      <input
                        type="text"
                        placeholder="House number and street name" name="address1" value="{{$address->address1}}"
                      />
                      <input
                        type="text"
                        placeholder="Apartment, suite, unit etc. (optional)" name="address2" value="{{$address->address2}}"
                      />
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>Town / City *</label>
                      <input type="text" name="city" value="{{$address->city}}"/>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>District *</label>
                      <input type="text" name="state" value="{{$address->state}}"/>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>Country *</label>
                      <input type="text" name="country" value="{{$address->country}}"/>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>Postcode / ZIP</label>
                      <input type="text" name="postcode" value="{{$address->pincode}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Phone *</label>
                      <input type="text" name="mobile" value="{{$address->mobile}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Email address *</label>
                      <input type="text" name="email" value="{{$address->email}}"/>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="single-form">
                  <button class="main-btn btn-block" type="submit">Submit</button>
                    </div>
                  </div>
                </div>
        	</form>

              </div>
            </div>
            <div class="col-lg-5 pt-lg-0 pt-md-4 pt-5">
              <h4>Step 2:</h4>
              <div class="checkout-review-order mt-30">
                <div class="checkout-title">
                  <h4 class="title">Your Order</h4>
                </div>

                <div class="checkout-review-order-table mt-15">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="Product-name">Product</th>
                        <th class="Product-price">Total</th>
                      </tr>
                    </thead>
                    <tbody>

				        <div class="checkout-info mt-30">
				            <div class="card-body">
				              <form action="{{URL::to('checkoutview')}}">
				                <div class="row">
				                  <div class="col-lg-8">
				                    <div class="single-form">
				                      <input type="text" placeholder="Coupon code" id="checkout-discount-input" name="Voucher" />
				                      <p>
                                     @if(isset($_GET['Voucher']))
                                        <a href="{{URL::to('checkoutview')}}">You applied this coupan  {{$_GET['Voucher']}}</a>
                                    @endif</p>
				                    </div>
				                  </div>
				                  <div class="col-lg-4">
				                    <div class="single-form">
				                      <button class="main-btn">Submit</button>
				                    </div>
				                  </div>
				                </div>
				              </form>
				            </div>
				        </div><br>
                      <tr>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td class="Product-name">
                          <p>Subtotal</p>
                        </td>
                        <td class="Product-price">
                          <p>{{$final_amount}}</p>
                        </td>
                      </tr>

                      <tr>
                        <td class="Product-name">
                          <p>Discount</p>
                        </td>
                        <td class="Product-price">
                          <p>-{{$discount_amount}}</p>
                        </td>
                      </tr>
                      <tr>
                        <td class="Product-name">
                          <p>Shipping</p>
                        </td>
                        <td class="Product-price">
                        	@php
                                $pincode_amount=0;
                                @endphp
                                @if($pincode)
                                    @if($pincode->max_delivery_charge < $final_amount)
                                        0
                                    @else
                                    @php

                                    $pincode_amount = $pincode->delivery_charge;
                                    @endphp
                                        {{$pincode->delivery_charge}}
                                    @endif
                                @else
                                    Currently we are not deliver this address
                                @endif
                        </td>
                      </tr>
                      <tr>
                        <td class="Product-name">
                          <p>Total</p>
                        </td>
                        <td class="total-price">
                          <p>{{($final_amount-$discount_amount)+$pincode_amount}}</p>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>

                <div class="checkout-payment">
            	<form action="{{URL::To('razorpay')}}" method="post">
                	@csrf
                  <ul>
                    <li>
                    	<div class="card-header" id="heading-5">
                                    <h4 class="card-title">
                                        <input type="radio" name="payment_method" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" id="online" aria-controls="collapse-5" onclick="click()" value="1">
                                            Online Payments
                                    </h2>     
                                    <h4 class="card-title">
                                        <input type="radio" name="payment_method" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" id="cod" aria-controls="collapse-3" onclick="click()" value="0">
                                            Cash on delivery   
                                    </h2>
                                </div>
                    </li>
                  </ul><br>
                        <input type="hidden" name="product_amount" value="{{($final_amount-$discount_amount)+$pincode_amount}}">
                        <input type="hidden" name="delivery_charges" value="{{$pincode_amount}}">
                        <input type="hidden" name="discount_amount" value="{{$discount_amount}}">
                  <button class="main-btn btn-block" type="submit">Place Order</button>
              </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>

    <!--====== Checkout Ends ======-->
@endsection

@section('checkoutpage-js')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<button id="rzp-button1" hidden="">Pay</button>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
@if($response ?? "")
<script>
    var options = {
        "key": "{{$response['razorpayId']}}", // Enter the Key ID generated from the Dashboard
        "amount": "{{$response['amount']}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "{{$response['currency']}}",
        "name": "{{$response['name']}}",
        "description": "{{$response['massage']}}",
        "image": "{{URL::TO('img/logo1.png')}}",
        "order_id": "{{$response['orderId']}}", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
        "handler": function (response){

            $.ajax({
                url: "{{URL::TO('/payment')}}",
                type: 'post',
                dataType: 'json',
                data: { 
                        "_token": "{{ csrf_token() }}",
                        rzp_payment_id      : response.razorpay_payment_id, 
                        rzp_order_id        : response.razorpay_order_id,
                        rzp_signature       : response.razorpay_signature,
                        name                : "{{$response['name']}}",
                        email               : "{{$response['email']}}",
                        mobile              : "{{$response['mobile']}}",
                        amount              : "{{$response['amount']}}",
                        address             : "{{$response['address']}}",
                        massage             : "{{$response['massage']}}",
                        delivery            : "{{$response['delivery']}}",
                        discount_amount     : "{{$response['discount_amount']}}",
                        total_amount        : "{{$response['total_amount']}}",
                    }, 

                success: function (msg) {
                    // sweet alert
                    swal({
                        title: "Thanks For Orders!",
                        text: "Order Sucessfull!",
                        type: "success"
                    }).then(function() {
                        window.location = "{{URL::to('/')}}";
                    });
                }
            });


        },
        "prefill": {
            "name": "{{$response['name']}}",
            "email": "{{$response['email']}}",
            "contact": "{{$response['mobile']}}"
        },
        "notes": {
            "address": "{{$response['address']}}"
        },
        "theme": {
            "color": "#F37254"
        }
    };
    var rzp1 = new Razorpay(options);
    window.onload = function(){
        document.getElementById('rzp-button1').click();
    }
    document.getElementById('rzp-button1').onclick = function(e){
        rzp1.open();
        e.preventDefault();
    }
</script>
@endif

@endsection

