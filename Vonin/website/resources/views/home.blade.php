@extends('layouts.app')
@section('content')
    
    <!--====== Slider Start ======-->
     <section
      class="slider-area slider-02 slider-active px-3"
      data-ride="carousel"
      data-interval="true">
      @foreach($banners as $banner)
      <div
        class="single-slider bg_cover d-flex align-items-center paroller"
        style="background-image: url({{Config::get('app.admin_url')}}/img/banner/{{$banner->image}}););background-position: right;"
        data-paroller-factor="-0.15"
        data-paroller-type="foreground"
        data-paroller-direction="vertical"
      >
        <div class="container">
          <div class="slider-content-2-2 text-center">
            <h5 class="sub-title">
              <span data-animation="slideInDown" data-delay="0.6s">
                End Mid Season
              </span>
            </h5>
            <h1 class="title">
              <span data-animation="slideInUp" data-delay="1s">Up to 50%</span>
            </h1>
            <div class="slider-btn">
              <a
                data-animation="zoomIn"
                data-delay="1.6s"
                href="{{URL::to('products')}}"
                class="main-btn"
              >
                Shop Now
              </a>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </section>
    <!--====== Slider Ends ======-->

    <!--====== Category Start ======-->

    <section class="category-area pt-5">
      <div class="section-title text-center pb-30">
              <h2 class="title">Categories</h2>
            </div>
      <div class="category-wrapper d-flex flex-wrap">
        @foreach($sub_categories as $cat)
        <div class="category-item p-3">
          <a href="{{URL::to('products_subcat',$cat->id)}}">
            <img
            src="{{Config::get('app.admin_url')}}/img/category/{{$cat->image}}" class="cat-image"
              alt=""
            />
            <div class="category-content">
              <h4 class="title py-2" style="color: black;background-color: #ffffff8a; letter-spacing: 1px">{{$cat->sub_category}}</h4>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </section>

    <!--====== Category Ends ======-->

    <!--====== Collections Start ======-->

    
    <!--====== Collections Start ======-->

    <section class="product-area new-arrival">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <div class="section-title text-center pb-30">
              <h2 class="title">Top Collection</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid custom-container">
        <div class="tab-content">
          <div class="tab-pane fade show active" id="all">
            <div class="row">
    @foreach($products as $product)
            @if($product->id < 4)
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="single-product mt-20">
                  <div class="product-image">
                    <div class="image">
                      <img
                        class="product-1 prod-img" 
                        src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}"
                        alt="product"
                      />
                      <a class="link" href="{{URL::to('specification',$product->id)}}"></a>
                    </div>
                  </div>
                  <div class="product-content d-flex justify-content-between">
                    <div class="product-title">
                      <h4 class="title">
                        <a href="{{URL::to('specification',$product->id)}}"
                          >{{$product->title}}</a
                        >
                      </h4>
                    </div>
                    <div class="product-price">
                      <!-- <span class="price">£150.00</span> -->
                </div>
              </div>
              </div>
              </div>
              @endif
    @endforeach
            </div>
          </div>
        </div>
        <div class="product-btn text-center mt-50">
          <a href="{{URL::to('products')}}" class="view-product-2"
            >View All Product</a
          >
        </div>
      </div>
    </section>

    <!--====== Collections Ends ======-->

    <!--====== Collections Ends ======-->

    <!-- ===== confusion starts =======-->

    <div class="px-3">
      <section class="banner-area-2">
        <!-- <div class="row justify-content-center">
          <div class="col-lg-6">
            <div class="section-title text-center pb-35">
              <h6 class="title">PLAN YOUR WEDDING</h6>
            </div>
          </div>
        </div> -->
        <div class="row">
          <div class="col-md-7">
            <div class="banner-content-2 banner-3 text-center">
              <h2 class="title" style="line-height: 1.6">
                Let us help you to choose your wedding outfit
              </h2>
              <div class="banner-btn">
                <a href="{{URL::to('products')}}" class="main-btn">Open Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="banner-image bg_cover" style="
                background-image: url(https://img.pngio.com/girl-in-gown-png-1-png-image-girl-gown-png-730_1095.png);
              "></div>
            </div>
        </div>
    </section>
    </div>
    </section>

    <!-- ===== confusion ends======== -->


    <!-- product area start -->
    <section class="product-area new-arrival">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-30">
                        <h2 class="title">New Arrival</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid custom-container">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="all">
                    <div class="row">                   
                      @foreach($products as $product)
                      @if($product->id > 13 or $product->id > 16)
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="single-product mt-20">
                                <div class="product-image">
                                    <div class="image">
                                        <img class="product-1 prod-img" src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}"/>
                                        <a class="link" href="{{URL::to('specification',$product->id)}}"></a>
                                    </div>
                                </div>
                                <div class="product-content d-flex justify-content-between">
                                    <div class="product-title">
                                        <h4 class="title">  
                                            <a href="{{URL::to('specification',$product->id)}}">Polyamide Dress With Long Sleeves</a>
                                        </h4>
                                    </div>
                                <div class="product-price">
                                <!-- <span class="price">£150.00</span> -->
                                </div>
                              </div>
                            </div>
                        </div>  
                        @endif      
                      @endforeach
            </div>
          </div>
        </div>
        <div class="product-btn text-center mt-50">
          <a href="{{URL::to('products')}}" class="view-product-2"
            >View All Product</a
          >
        </div>
      </div>
    </section>
    <!-- product area ends -->

    <!--====== Video Start ======-->
    <div class="px-3 pb-3">
    <div
      class="video-area video-2 bg_cover mt-10"
      style="background-image: url(https://akns-images.eonline.com/eol_images/Entire_Site/201545/rs_634x634-150505154921-600-BeyonceGown.gif?fit=around|1080:540&output-quality=90&crop=1080:540;center,top)">
      <div class="container">
        <div class="video-play text-center">
          <a
            href="https://www.youtube.com/watch?v=A5VceFyAly8"
            class="play video-popup"
            ><i class="fas fa-play"></i
          ></a>
        </div>
      </div>
    </div>
   </div>
    <!--====== Video ends ======-->
    <!-- ========newsletter start======= -->
<!-- <div class="px-3"><br><br>

</div> -->
@endsection
