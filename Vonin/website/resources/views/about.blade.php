@extends('layouts.app')
@section('content')
    <!--====== Page Banner Start ======-->

    <section class="page-banner bg_cover" style="background-image: url(assets/images/page-banner-4.jpg)">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">About Vonin</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
                    <li class="breadcrumb-item active">About</li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

   <!--====== About Start ======-->

   <section class="about-area pt-50">
        <div class="container">
            <div class="row align-items-center" style="margin-top: -50px;">
                <div class="col-lg-6">
                    <div class="about-image mt-50">
                        <img src="assets\images\about1-bg-1.png" alt="" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-content mt-45">
                        <h2 class="title">
                            About Vonin                    </h2>
                        <p class="text-justify">
                        When writing your About Us page, you don’t want to say the same things that appear on your Home page, and a long, detailed, historical summary will bore the reader. You also don’t want to sound like you’re boasting. As in most things, the best answer is to find the middle ground.

Much like a resume you’d use to land a job, your About Us page is the place to toot your own horn. To avoid bragging or appearing pushy steer clear of hype.<br> Instead offer a straightforward presentation of the facts and figures.  Avoid superlatives.

For example, explain why people should do business with you. What benefits can you or<br>  your product provide that your prospects will value?

If you can, quantify those benefits.  Saying your product increases productivity by 18% is a lot more persuasive than simply saying it improves productivity.
                        </p>
                        <div class="about-signature pt-5">
                            
                            <p>Abhisheh • CEO Founder (Vonin)</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section><br>

    <!--====== About Ends ======-->

@endsection
