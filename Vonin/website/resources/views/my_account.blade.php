@extends('layouts.app')
@section('content')
    
    <!--====== Page Banner Start ======-->

    <section
      class="page-banner bg_cover"
      style="background-image: url(assets/images/page-banner-4.jpg)"
    >
      <div class="container">
        <div class="page-banner-content text-center">
          <h2 class="title">My Account</h2>
          <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{URL::TO('/')}}">Home</a></li>
            <li class="breadcrumb-item active">My Account</li>
          </ol>
        </div>
      </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== My Account Start ======-->

    <section class="my-account-page pt-50 pb-80">
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-md-4">
            <div class="my-account-menu mt-30">
              <ul class="nav account-menu-list flex-column">
                <li>
                  <a class="active" data-toggle="pill" href="#pills-dashboard"
                    ><i class="far fa-tachometer-alt-fast"></i> Dashboard</a
                  >
                </li>
                <li>
                  <a data-toggle="pill" href="#pills-order"
                    ><i class="far fa-Shopping-cart"></i> Order</a
                  >
                </li>
                <li>
                  <a data-toggle="pill" href="#pills-account"
                    ><i class="far fa-user"></i> User Details</a
                  >
                </li>
                <li>
                  <a href="{{URL::to('auth/logout')}}"
                    ><i class="far fa-sign-out-alt"></i> Logout</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <div class="col-xl-8 col-md-8">
            <div class="tab-content my-account-tab mt-30" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-dashboard">
                <div class="my-account-dashboard account-wrapper">
                  <h4 class="account-title">Dashboard</h4>
                  <div class="welcome-dashboard">
                    <p>
                      Hello, <strong>{{Auth::user()->name}}</strong> <br>(If Not
                      <strong>{{Auth::user()->name}} !</strong>
                      <a href="{{URL::to('auth/logout')}}">Logout</a> )
                    </p>
                  </div>
                  <p class="mt-25">
                    From your account dashboard. you can easily check & view
                    your recent orders, manage your shipping and billing
                    addresses and edit your password and account details.
                  </p>
                </div>
              </div>
              <div class="tab-pane fade" id="pills-order">
                <div class="my-account-order account-wrapper">
                  <h4 class="account-title">Orders</h4>
                  <div class="account-table text-center mt-30 table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Sno.</th>
                          <th>Order Id</th>
                          <th>Order Date</th>
                          <th>Transaction_id</th>
                          <th>Status</th>
                          <th>Amount</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php $i=1; @endphp
                        @foreach ($data as $datas)

                          <tr>
                            <td>
                              {{$i++}}
                            </td>
                            <td>
                              VN_{{$datas->id}}
                            </td> 
                            <td>
                              {{date('d-M-Y', strtotime($datas->created_at))}}
                            </td> 
                            <td>
                              @if($datas->transaction_id)
                              {{$datas->transaction_id}}
                              @else
                              COD
                              @endif
                            </td> 
                             <td>
                              @if($datas->status == 1)
                                Delivered
                              @elseif($datas->status == 0)
                                Processing
                             @elseif($datas->status == -2)
                                Reject
                              @else
                                New
                              @endif  
                            </td>
                            <td>
                              {{$datas->total_price}}
                            </td>
                            <td>
                              
                    <a href="{{URL::To('bill',$datas->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-print" aria-hidden="true"></i></a>
                            </td>
                          </tr>

                        @endforeach  

                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="pills-account">
                <div class="my-account-details account-wrapper">
                  <form action="{{URL::to('myaccountstore')}}" method="post">
                    @csrf
                  <h4 class="account-title">User Information</h4>
                  <div class="account-details">
                    <div class="row">
                      <div class="col-sm-6">
                    <div class="single-form">
                      <label>First name *</label>
                      <input type="text" name="fname" value="{{$address->name}}" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Last name *</label>
                      <input type="text" name="lname" />
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="single-form">
                      <label>Street address *</label>
                      <input
                        type="text"
                        placeholder="House number and street name" name="address1" value="{{$address->address1}}"
                      />
                      <input
                        type="text"
                        placeholder="Apartment, suite, unit etc. (optional)" name="address2" value="{{$address->address2}}"
                      />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Town / City *</label>
                      <input type="text" name="city" value="{{$address->city}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>District *</label>
                      <input type="text" name="state" value="{{$address->state}}"/>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Country *</label>
                      <input type="text" name="country" value="{{$address->country}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Postcode / ZIP</label>
                      <input type="text" name="postcode" value="{{$address->pincode}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Phone *</label>
                      <input type="text" name="mobile" value="{{$address->mobile}}"/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="single-form">
                      <label>Email address *</label>
                      <input type="text" name="email" value="{{$address->email}}"/>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="single-form">
                  <button class="main-btn btn-block" type="submit">Submit</button>
                    </div>
                  </div>
                    </div>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--====== My Account Ends ======-->
@endsection
