@extends('layouts.app')

@section('content')

  <section
      class="page-banner bg_cover"
      style="background-image: url(../assets/images/page-banner-4.jpg)"
    >
      <div class="container" >
        <div class="page-banner-content text-center">
            <h1 class="page-title">Invoice</h1>
          <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item active">My Account</li>
            <li class="breadcrumb-item">Bills</li>
          </ol>
        </div>
      </div>
    </section><br>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="text-right">Order # 12345</h3>
            </div>
                <div class="col-lg-4">
                    <address>
                    <strong>Vonin Store:</strong><br>
                        John Smith<br>
                        1234 Main<br>
                        Apt. 4B<br>
                        Springfield, ST 54321
                    </address>
                </div>
                <div class="col-lg-4">
                    <address>
                    <strong>Delivery Address:</strong><br>
                        {{$details->address}}<br>
                        {{$details->pincode}}<br>
                        Phone:{{$details->mobile}}
                    </address>
                </div>
                <div class="col-lg-4">
                    <address>
                        <strong>Payment Method:</strong><br>
                        @if($details->transaction_id)
                        Transaction id:{{$details->transaction_id}}
                        @else
                        Cash on Delivery
                        @endif<br>
                    </address>
                    <address>
                        <strong>Order Date:</strong><br>
                        {{date('d-M-Y', strtotime($details->created_at))}}<br><br>
                    </address>
                </div>
        </div>
    
        <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Sr.no.</strong></td>
                                    <td><strong>Item</strong></td>
                                    <td class="text-center"><strong>Price</strong></td>
                                    <td class="text-center"><strong>Quantity</strong></td>
                                    <td class="text-right"><strong>Totals</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                  @php $i=1; $subtotal=0;@endphp
                                  @foreach ($cart as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->title}}</td>
                                    <td class="text-center">{{$data->amount}}</td>
                                    <td class="text-center">{{$data->qty}}</td>
                                    <td class="text-right">{{$data->amount * $data->qty}}</td>
                                </tr>
                                @endforeach
                                
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"><strong>Discount Amount</strong></td>
                                    <td class="no-line">-{{$total->discount_amount}}</td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"><strong>Shipping</strong></td>
                                    <td class="no-line">{{$total->delivery_charges}}</td>
                                </tr><tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"><strong><br>Total</strong></td>
                                    <td class="thick-line"><br>{{$total->total_price}}</td>
                                </tr>
                            </tbody>
                        </table><br>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection