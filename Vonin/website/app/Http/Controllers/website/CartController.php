<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Carbon\Carbon;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $is_product_exist=DB::table('products')->where('id',$request->product_id)->count();
        // dd($request->all());
        // check product exist or not
        if($is_product_exist==1)
        {
            $oldcart=DB::table('carts')->where('auth_user_id',auth::user()->id)
                                       ->where('product_id',  $request->product_id)
                                       ->where('size', $request->size)
                                       ->first();
                // dd($oldcart);                                     
            if($oldcart) 
            {
                Session::flash('error', 'product Already in Your Cart');
                return back();                        
            }
            // insert new product
            else
            {
                $product=DB::table('products')->where('products.id',$request->product_id)
                  ->join('productsizes','productsizes.product_id','products.id')
                  ->select("products.*", 'productsizes.amount')
                  ->first();
        // dd($product);
                if($request->n)
                {

                DB::table('carts')->insert([
                    'product_id'    => $request->product_id,
                    'product_name'  => $product->title,
                    'size'          => $request->size,
                    'breast_size'   => $request->n[0],
                    'waist_size'    => $request->n[1],
                    'height'        => $request->n[2],
                    'qty'           => $request->qty,
                    'price'         => $product->amount * $request->qty,
                    'auth_user_id'  => auth::user()->id,
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);

            Session::flash('success', 'product added successfully');
            return back();
                }

                else
                {

                DB::table('carts')->insert([
                    'product_id'    => $request->product_id,
                    'product_name'  => $product->title,
                    'size'          => $request->size,
                    'qty'           => $request->qty,
                    'price'         => $product->amount * $request->qty,
                    'auth_user_id'  => auth::user()->id,
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);

            Session::flash('success', 'product added successfully');
            return back();
                }

            
            }
                               
        }
        else{
            Session::flash('error', 'Product id does not exist');
            return back();
        }

    }


    public function delete($id)
    {
        // dd($id);
        DB::table('carts')->where('id', $id)->delete();
        Session::flash('success', 'product remove  successfully');
        return redirect()->route('addtocartview');
    }

    public function view()
    {        
        $carts=DB::table('carts')->join('products','carts.product_id', 'products.id')
                                ->where('auth_user_id',auth::user()->id)
                                ->select("carts.*", 'products.main_image')
                                ->get();
                                // dd($carts);
        return view('add_to_cart',compact('carts'));
    }

    public function update_cart(request $request)
    {
        $response=DB::table("carts")->where("id", $request->id)->update([
                                'qty'  => $request->qty,
                                'price'=> $request->price * $request->qty
                            ]);
        // dd($response);
        return response()->json(['status'   => 1]);
    }
}
