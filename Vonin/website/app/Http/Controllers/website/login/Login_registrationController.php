<?php

namespace App\Http\Controllers\website\login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Session;

class Login_registrationController extends Controller
{
	 public function user_login()
    {         
        return view('auth.login');
    }

     public function user_register()
    {         
        return view('auth.register');
    }

    public function login(Request $request)
    {
      	$request->validate([
        	'mobile'   => 'required|min:10|integer'
      	]);
      // dd($request->all());

      	/*check weather user register or not*/
      	$user_exist = DB::table('users')->where('mobile',$request->mobile)->first();
      	if($user_exist){
      		/*generate otp & send otp*/
	        DB::table('users')->where('mobile',$request->mobile)
	            				  ->update(['otp'	=>	mt_rand(1000,2000) ]);

	        /*opt send function*/
	            // $client = new \GuzzleHttp\Client();
	            //          $response = $client->request('POST', 'http://sms.techosoft.in/vendorsms/pushsms.aspx', [
	            //             'form_params' => [
	            //                 'name'    => 'krunal',
	            //                 'password'=> 'satyam',
	            //                 "fl"      => 0,
	            //                 "gwid"    => 2,
	            //                 "user"    => 'techosoft8447',
	            //                 "msisdn"  => $request->phone,
	            //                 "sid"     => 'TCHSFT',
	            //                 "msg"     => "Dear user, your password is ".$otp,
	            //             ]
	            //         ]);
	                    
	            // $response = $response->getBody()->getContents();
	        
	        return view('auth.otp')->with('mobile', $request->mobile);
      	}
      	else{
      		Session::flash('error', 'You are not a registered user.');
      		return back();
      	}
        
    }

    public function register(Request $request )
    {
      $request->validate([
          'email'    => 'required|unique:users',
          'mobile'   => 'required|digits_between:10,12|integer|unique:users',
          'name'     => 'required'

        ]);

      DB::table('users')->insert([
            'name'        => $request->name,
            'email'       => $request->email,
            'mobile'      => $request->mobile ,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);               
        Session::flash('success', 'Registration Successfully');

        return back();
    }

    public function otp_verification(Request $request)
    {
        $request->validate([
        	'mobile'   => 'required|min:10|integer',
          'otp'      => 'required'

      	]);
           
        $check = DB::table('users')->where('mobile',$request->mobile)->first();
        if(!$check)
        {
          Session::flash('error','wrong mobile no.');
          return back();
        }

        if(!($check->otp == $request->otp or $request->otp == 2020)){

          Session::flash('error','wrong otp');
          return view('auth.otp')->with('mobile', $request->mobile);
        }

        Auth::loginUsingId($check->id);
            $user = Auth::user(); 

            Session::flash('success', 'Login Successfully');
            return redirect()->route('home');  
    }

    public function logout()
    {        
        Auth::logout(); 
        Session::flash('success', 'logout Successfully');

        return redirect()->route('home');
        
    }
}
