<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Carbon\Carbon;

class MyaccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
// 
    public function index()
    {
    	$address = DB::table('users')->where('users.id',Auth::user()->id)
                                     ->leftjoin('address','address.user_id','=','users.id')
                                     ->select('address.*')
                                     ->first();

        $order_data=DB::table('checkout_carts')->where('checkout_carts.user_id',Auth::user()->id)
        			->join('products','products.id','checkout_carts.product_id')
        			->get();

        $data = DB::table('checkout')->join('users','users.id','checkout.auth_id')
                                     ->where('auth_id',Auth::user()->id)
                                     ->select(
                                        'checkout.*',
                                        'users.name',
                                        'users.email'
                                    )
                                     ->paginate(10);
        // dd($data);

    	return view('my_account',compact('address','order_data','data'));
    }

    public function store(Request $request)
    {
        $is_address_exist=DB::table('address')->where('user_id',auth::user()->id)->count();
        // dd($is_address_exist);

         $request->validate([
            'fname'         =>      'required|min:2|max:30',
            'lname'         =>      'required|min:2|max:30',
            'country'       =>      'required',
            'address1'      =>      'required',
            'address2'      =>      'required',
            'city'          =>      'required',
            'state'         =>      'required',
            'postcode'      =>      'required',
            'mobile'        =>      'required|integer|digits_between:10,12',
            'email'         =>      'required|email',

        ]);
        if($is_address_exist==1)
        {
            DB::table("address")->where("user_id",auth::user()->id)
            ->update([             
            'user_id'       =>      auth::user()->id,
            'name'          =>      $request->fname.' '.$request->lname,
            'address1'      =>      $request->address1,
            'address2'      =>      $request->address2,
            'pincode'       =>      $request->postcode,
            'email'         =>      $request->email,
            'country'       =>      $request->country,
            'city'          =>      $request->city,
            'state'         =>      $request->state,
            'mobile'        =>      $request->mobile,
            'created_at'    =>      Carbon::now(),
            'updated_at'    =>      Carbon::now()

            ]);
            Session::flash('success', 'address updated successfully');
        }

        else{

        DB::table('address')->insert([
            'user_id'       =>      auth::user()->id,
            'name'          =>      $request->fname.' '.$request->lname,
            'address1'      =>      $request->address1,
            'address2'      =>      $request->address2,
            'pincode'       =>      $request->postcode,
            'email'         =>      $request->email,
            'country'       =>      $request->country,
            'city'          =>      $request->city,
            'state'         =>      $request->state,
            'mobile'        =>      $request->mobile,
            'created_at'    =>      Carbon::now(),
            'updated_at'    =>      Carbon::now()
        ]); 
            Session::flash('success', 'address added successfully');
        }

        return back();    
    }

    public function bill($id)
    {
        $cart = DB::table('checkout_carts')
            ->join('products','products.id','checkout_carts.product_id')
            ->join('productsizes','productsizes.id','checkout_carts.size')
            ->where('checkout_id',$id)
            ->get();

        $details=DB::table('checkout')->where('id',$id)->first();

        $total=DB::table('checkout')->where('checkout.id',$id)->first();
        // dd($cart);
        return view('bills',compact('cart','total','details'));

    }
// 
}
