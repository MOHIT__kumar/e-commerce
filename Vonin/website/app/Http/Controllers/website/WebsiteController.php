<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Carbon\Carbon;

class WebsiteController extends Controller
{
    public function index()
    {
    	$sub_categories = DB::table('sub__categories')->get(); 
        $products = DB::table('products')->get(); 
        $banners = Db::table('banners')->get();
        // dd($sub_categories);
        return view('home',compact('sub_categories','products','banners'));
    }
    
    public function contact()
    {   
        return view('contact');
    }
    
    public function contact_store(Request $request)
    {   
        // dd($request->all());
        $request->validate([
            'name'          =>      'required',
            'email'         =>      'required|email',
            'subject'       =>      'required',
            'number'        =>      'required|min:2|max:30',
            'message'       =>      'required'
        ]);

        DB::table('contact')->insert([
            'name'          =>      $request->name,
            'email'         =>      $request->email,
            'message'       =>      $request->message,
            'created_at'    =>      Carbon::now(),
            'updated_at'    =>      Carbon::now()
        ]); 

        Session::flash('success', 'address updated successfully');

        return back();
    }

    public function about()
    {
        return view('about');
    }

    public function term_and_condition()
    {
        return view('term_and_conditions');
    }

    public function return()
    {
        return view('return');
    }

    public function blog_detail()
    {
        return view('blog_detail');
    }
    
    public function policies()
    {
        return view('privacy_policy');
    }
}
