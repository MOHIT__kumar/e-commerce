<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SpecificationController extends Controller
{   
    public function single_product($id)
    {
        // for printing the amount of all different sizez 
        $size=DB::table('cloth_size')->get(); 
        // for printing the name of different sizez 
        $data = DB::table('products')->where('id',$id)->first();
        // for printing the related products
        $products = DB::table('products')
        ->where('sub_category_id',$data->sub_category_id)
        ->wherenotin('id',[$id])
        ->get(); 
        // for printing the amount of specific size
        $productsizes=DB::table('productsizes')->where('product_id',$id)->first();
        // for images 
        // $image=DB::table('product_file')->where('product_id',$id)->first(); 
        $images=DB::table('product_file')->where('product_id',$id)->get(); 
        //   dd($products);  

        return view('product_description',compact('data','products','size','productsizes','images'));
    }

    public function amount($id)
	{
		$amount=DB::table("productsizes")
            ->where('id',$id)->pluck('id','amount');
            /*dd($amount);*/
            return response()->json($amount);
    }
}
