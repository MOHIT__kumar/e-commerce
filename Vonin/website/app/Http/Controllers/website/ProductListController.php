<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProductListController extends Controller
{
    public function store($id = null, request $request)
    {  
        if ($request->search) {
            $products = DB::table('products')->Where('title', 'like','%'.$request->search.'%') 
            ->paginate(10); 
            // dd($products);
        }
        else{
            $products = DB::table('products')->where('sub_category_id',$id)
                                             ->get(); 
        }

        // dd($products);
        return view('product_list',compact('products'));
    }

     public function sub_categories($id)
    {
        $products = DB::table('products')->where('sub_category_id',$id)->get();
        // dd($products);
        return view('product_list',compact('products'));
    }
    
     public function categories($id)
    {
        $products = DB::table('products')->where('category_id',$id)->get();
        // dd($products);
        return view('product_list',compact('products'));
    }

    public function sub_cat($id)
    {
        $products = DB::table('products')->where('sub_category_id',$id)->get();
        // dd($products);
        return view('product_list',compact('products'));
    }

    public function child($id)
    {
        $products = DB::table('products')->where('sub_sub_category_id',$id)->get();
        // dd($products);
        return view('product_list',compact('products'));
    }

    public function image(request $request)
    {
        $response=DB::table("carts")->where("id", $request->id)->update([
                                'qty'  => $request->qty,
                                'price'=> $request->price * $request->qty
                            ]);
        // dd($response);
        return response()->json(['status'   => 1]);
    }

    public function products()
    {
        $products = DB::table('products')->paginate(9);
        // dd($products);
        return view('allproducts',compact('products'));
    }

    public function color(request $request)
    {
        $data=DB::table("product_file")->where("color_id", $request->color_id)
                                       ->pluck('files');

        return response()->json([
                        'status'   => 1,
                        'data'     => $data,
                        'color_id' => $request->color_id
                        ]);
        // return response()->json($data);
    }
}
