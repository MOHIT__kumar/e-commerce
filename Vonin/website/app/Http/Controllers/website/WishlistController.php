<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Session;

class WishlistController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function store($id)
    {
            // dd($id);
            $oldcart=DB::table('wishlists')->where('user_id',auth::user()->id)
                                       ->where('product_id',$id)
                                       ->first();
            // update quentity                                     
            if($oldcart) 
            {
                DB::table('wishlists')->where('product_id', $id)->delete();
                Session::flash('success', 'product Remove From Wishlist Successfully');
                return back();                            
            }
            // insert new product
            else
            {
                DB::table('wishlists')->insert([
                    'product_id'    => $id,
                    'user_id'  => auth::user()->id,
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);

            Session::flash('success', 'product added successfully');
            return back();
            
            }
                               
        return view('wishlist');
    }

    public function view()
    {
        $wish = DB::table('products')->join('wishlists','wishlists.product_id','=','products.id')
                                ->select('products.main_image','products.title','products.id','wishlists.id as wishlists_id')
                                ->where('wishlists.user_id',auth::user()->id)
							    ->get();
        // dd($wish);
		return view('wishlist',compact('wish'));
    }

    public function delete($id)
    {
        // dd($id);    
        DB::table('wishlists')->where('product_id',$id)->delete();
        Session::flash('success', 'product Remove From Wishlist Successfully');
        return back();
    }
}
