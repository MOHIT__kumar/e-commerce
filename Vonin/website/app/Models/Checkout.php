<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    Protected $fillable = ['auth_id','address','mobile','pincode','transaction_id','delivery_charges','total_price','discount_amount'];

    Protected $table='checkout';

    public $primaryKey='id';
}
