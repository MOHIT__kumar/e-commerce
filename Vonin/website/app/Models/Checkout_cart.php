<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkout_cart extends Model
{
    Protected $fillable = ['checkout_id','product_id','size','user_id','qty','price','breast_size','waist_size','height'];

    Protected $table='checkout_carts';

    public $primaryKey='id';

}
