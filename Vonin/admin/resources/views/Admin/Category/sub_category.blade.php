@extends('layouts.admin')
@section('title', 'Sub category')
@section('content')

<section class="content-header">
  <h1>
   View Sub Category
  </h1>
  <ol class="breadcrumb">
    <button class="btn btn-primary" data-toggle="modal" data-target="#text"><i class="glyphicon glyphicon-plus"></i> </button>
    <div class="modal fade" id="text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">ADD Sub Category</h4>
        </div>
          <div class="modal-body">
            <form role="form" action="{{route('sub_category.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('POST')

              <label>Category:</label>
              <Select class="form-control" name="category_id" required="required">
                @foreach($category as $row)
                <option value="{{$row->id}}">{{$row->category}}</option>
                @endforeach
              </Select>

              <label>Sub Category:</label>
              <input type="text" class="form-control" name="sub_category" required="required" placeholder="Enter Category Type">

              <label>Image:</label>
              <input type="file" class="form-control" name="image" >

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
              </div>
            </form>
          </div>
      </div>
    </div>
  </ol>
</section>
<br>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-xs-12">
      <div class="box" style="overflow-y: scroll;">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sno.</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Images</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($data as $data)

              <tr>
                <td>{{$i++}}</td>
                <td>{{$data->category_name}}</td>
                <td>{{$data->sub_category}}</td>
                <td><img src="{{URL::to('img/subcategory/'.$data->image)}}" height="80" width="100"></td>
                <td>{{$data->created_at}}</td>
                <td>{{$data->updated_at}}</td>
                <td>

                  <!-- Edit Button -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#{{$data->id}}"><i class="glyphicon glyphicon-edit"></i> </button>
              

                 <div class="modal fade" id="{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                   <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Edit Sub Category</h4>
                    </div>
                    <div class="modal-body">
                      <form role="form" action="{{route('sub_category.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <label>Category:</label>
                        <Select class="form-control" name="category_id" required="required">
                            @foreach($category as $row)
                                @if($data->category_id == $row->id)
                                  <option value="{{$row->id}}" selected>{{$row->category}}</option>
                                @else
                                  <option value="{{$row->id}}">{{$row->category}}</option>
                                @endif
                            @endforeach
                        </Select>

                        <label>Sub Category:</label>
                        <input type="text" class="form-control" name="sub_category" required="required" value="{{$data->sub_category}}"> 

                        <label>Image:</label>
                        <input type="file" class="form-control" name="image" >
                        <span>{{$data->image}}</span>

                        <label></label>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" value="submit">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            @endforeach  

          </tbody>

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

@endsection