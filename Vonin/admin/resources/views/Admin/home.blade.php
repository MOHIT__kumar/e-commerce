@extends('layouts.admin')
@section('title', 'Home')
@section('content')
<section class="content-header">
   <h1>
      Dashboard
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
   </ol>
</section>
<section class="content">
   <!-- Info boxes -->
   <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-th-large"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Category</span>
               <span class="info-box-number">{{$category}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-th"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Total Products</span>
               <span class="info-box-number">{{$products}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Sales</span>
               <span class="info-box-number">{{$total_order}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">New Members</span>
               <span class="info-box-number">{{$users}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->    
   <!-- Main row -->
   <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">Latest Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="table-responsive">
                  <table class="table no-margin">
                     <thead>
              <tr>
                <th>Sno.</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($checkout as $datas)

                <tr>
                  <td>
                    {{$i++}}
                  </td>
                  <td>
                    {{$datas->name}}
                  </td> 
                  <td>
                    {{$datas->mobile}}
                  </td> 
                   <td>
                    @if($datas->status == 1)
                      Delivered
                    @elseif($datas->status == 0)
                      Processing
                   @elseif($datas->status == -2)
                      Reject
                    @else
                      Pending
                    @endif  
                  </td>
                  <td>
                    {{$datas->total_price}}
                  </td>
                  <td>
                    <a href="{{route('orders.edit',$datas->id)}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-print"></i></a>
                           
                  </td>
                </tr>

              @endforeach  

          </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.box-footer -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4">
         <!-- PRODUCT LIST -->
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title">Recently Added Products</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <ul class="products-list product-list-in-box">

                  @foreach($product_limit as $product_limit)
                     <li class="item">
                        <div class="product-img">
                           <img src="{{URL::to('img/product/'.$product_limit->main_image)}}" alt="Product Image">
                        </div>
                        <div class="product-info">
                           <a href="{{route('product.edit',$product_limit->id)}}" class="product-title">{{$product_limit->title}}
                           <span class="product-description">
                           </span>
                        </div>
                     </li>
                  @endforeach
               </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
               <a href="{{URL::to('product')}}" class="btn btn-sm btn-primary btn-flat">View All Products</a>
            </div>
            <!-- /.box-footer -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->
</section>
@endsection