<title>Product List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
      <!-- <a href="#" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> </a> -->
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li><a href="{{route('product.edit',$id)}}">Edit Product</a></li>
            <li><a href="{{route('product_size.edit',$id)}}">Product Size</a></li>
            <!-- <li><a href="{{route('product_size.show',$id)}}">Product Atribute</a></li> -->
            <li class="active"><a href="{{route('stock.edit',$id)}}">Stock</a></li>
            <li><a href="{{route('product.show',$id)}}">File</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{route('stock.store')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{$id}}">
            <div class="box-body">

               <div class="col-md-4">
                  <div class="form-group" data-select2-id="13">
                     <label>Select Size</label>
                     <select class="form-control select2 select2-hidden-accessible" data-select2-id="7" tabindex="-1" aria-hidden="true" style="width: 100%" name="size">
                        @foreach($size as $size)
                          <option value="{{$size->id}}">{{$size->size}}</option>
                         @endforeach
                     </select>
                  </div>
               </div>

               <div class="col-md-4">
                  <div class="form-group">
                     <label>Credit</label>
                     <input type="number" name="credit" class="form-control" required placeholder="Enter Product Weight">
                  </div> 
               </div>


               <div class="col-md-4">
                  <div class="form-group">
                     <label>Debit</label>
                     <input type="number" name="debit" class="form-control" required placeholder="Enter Product Weight">
                  </div> 
               </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>

   <div class="col-xs-12">
      <div class="box box-primary">
         <div class="box-body" style="overflow-y: auto;">
            <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                      <th>Size</th>
                      <th>Credit</th>
                      <th>Debit</th>
                      <th>Created At</th>
                  </tr>
               </thead>
               <tbody>
                    
                    @foreach($data as $datas)

                      <tr>
                        <td>{{$datas->size}}</td>
                        <td>{{$datas->credit}}</td>
                        <td>{{$datas->debit}}</td>
                        <td>{{$datas->created_at}} </td>
                      </tr>

                    @endforeach
                   
                 
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection


<script type="text/javascript">
   function sizes(element){
    var sizes = $(element).val();    
    if(sizes){
        $.ajax({
           type:"GET",
           url:"{{URL::to('price')}}/"+sizes+'/',
           success:function(res){               
            if(res){
                if (res.length == 0) {
                   $("#default_size").empty();
                   $("#size_type").empty();
                   $("#size_type").append('<option>None</option>');
                   $("#default_size").append('<option>None</option>');
                }
                else
                {
                    $("#default_size").empty();
                    $("#default_size").append('<option>Select Default Size</option>');
                    $("#size_type").empty();
                    $.each(res,function(key,value){
                        $("#size_type").append('<option value="'+key+'">'+key+''+value+'</option>');
                    });
                    $.each(res,function(key,value){
                        $("#default_size").append('<option value="'+key+'">'+key+''+value+'</option>');
                    });
                }   
            }
           }
        });
    }     
    };
</script>
