<title>Product List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
      <!-- <a href="#" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> </a> -->
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li><a href="{{route('product.edit',$id)}}">Edit Product</a></li>
            <li class="active"><a href="{{route('product_size.edit',$id)}}">Product Size</a></li>
            <!-- <li><a href="{{route('product_size.show',$id)}}">Product Atribute</a></li> -->
            <li><a href="{{route('stock.edit',$id)}}">Stock</a></li>
            <li><a href="{{route('product.show',$id)}}">File</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{route('product_size.store')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{$id}}">
            <div class="box-body">
               <!-- <div class="col-md-4">
                  <div class="form-group">
                     <label>Size</label>
                     <input type="text" name="size" class="form-control" required placeholder="Enter Product Size">
                  </div>
               </div> -->
               <div class="col-md-4">
                  <div class="form-group">
                     <label>Size</label>
                     @php
                     $size=DB::table('cloth_size')->get();
                     @endphp
                     <select name="size" id="size" class="form-control" required>
                           @foreach($size as $size)
                           <option value="{{$size->id}}" name="{{$size->size}}">{{$size->size}}</option>
                           @endforeach
                           <!-- <option id="customize">customize</option> -->
                        </select>
                  </div> 
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label>Amount</label>
                     <input type="number" name="amount" class="form-control" required placeholder="Enter Product Amount">
                  </div>
               </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>

   <div class="col-xs-12">
      <div class="box box-primary">
         <div class="box-body" style="overflow-y: auto;">
            <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                      <!-- <th>Size</th> -->
                      <th>Size</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                    
                    @foreach($data as $datas)

                      <tr>
                        <!-- <td>{{$datas->size}}</td> -->
                        <td>{{$datas->size}}</td>
                        <td>{{$datas->amount}}</td>
                        <td>
                          @if($datas->status == 1)
                               Active
                          @else
                                DeActive
                          @endif
                        </td>
                        <td>
                          <button class="btn btn-primary" data-toggle="modal" data-target="#text{{$datas->id}}"><i class="glyphicon glyphicon-edit"></i> </button>
                                    <div class="modal fade" id="text{{$datas->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Edit Price</h4>
                                          </div>
                                             <div class="modal-body">
                                                <form role="form" action="{{route('product_size.update',$datas->id)}}" method="Post" >
                                                    @csrf
                                                    @method('PUT')
                                                       
                                                   <label>Size</label>
                                                    <input type="" class="form-control" name="size" value="{{$datas->size}}">

                                                  <label>Price</label>
                                                    <input type="number" class="form-control" name="amount" value="{{$datas->amount}}" step="0.01">

                                                   <label>Status</label>
                                                    <select class="form-control" name="status">
                                                        <option value="1">Active</option>
                                                        <option value="0">DeActive</option>
                                                    </select>
                                                 
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                                                  </div>
                                                  </form>
                                              </div>
                                          </div>
                                     </div>
                        </td>

                      </tr>

                    @endforeach
                   
                 
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection


<script type="text/javascript">
   function sizes(element){
    var sizes = $(element).val();    
    if(sizes){
        $.ajax({
           type:"GET",
           url:"{{URL::to('price')}}/"+sizes+'/',
           success:function(res){               
            if(res){
                if (res.length == 0) {
                   $("#default_size").empty();
                   $("#size_type").empty();
                   $("#size_type").append('<option>None</option>');
                   $("#default_size").append('<option>None</option>');
                }
                else
                {
                    $("#default_size").empty();
                    $("#default_size").append('<option>Select Default Size</option>');
                    $("#size_type").empty();
                    $.each(res,function(key,value){
                        $("#size_type").append('<option value="'+key+'">'+key+''+value+'</option>');
                    });
                    $.each(res,function(key,value){
                        $("#default_size").append('<option value="'+key+'">'+key+''+value+'</option>');
                    });
                }   
            }
           }
        });
    }     
    };
</script>
