<title>Product List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
      <a href="{{route('product.index')}}" class="btn btn-primary">View Product </a>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Create Product</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="box-body">
               <div class="col-md-6">
                  <div class="form-group">
                     <label for="exampleInputPassword1">Category</label>
                     <select class="form-control" id="category" name="category_id" onchange="categorys(this)">
                        <option>Select Category</option>
                        @foreach($category as $key)
                        <option value="{{$key->id}}">{{$key->category}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Choose Sub Category </label>
                     <select class="form-control" id="sub_service" name="subcategory_id" required onchange="subcategoryss(this)">
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label>Choose Sub Sub Category </label>
                     <select class="form-control" id="subsubcategory" name="subsubcategory_id" required>
                     </select>
                  </div>
               </div>
               
              <input type="hidden" id="meta_title" name="meta_title">
                  
              <input type="hidden" id="meta_keywords" name="meta_keywords">

               <div class="col-md-6">
                  <div class="form-group">
                     <label>Product Name</label>
                     <input type="text" class="form-control" id="title" name="title" placeholder="Enter Product Name" required="required" >
                  </div>
               </div>
               <!-- 
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Offer Amount</label>
                     <input type="number" class="form-control" name="offer_amount" placeholder="Enter Offer Amount" required="required" >
                  </div>
               </div> -->

               <div class="col-md-12">
                  <div class="form-group">
                     <label for="sponser_id">Description</label>
                     <textarea class="textarea t" id="textarea" required placeholder="Place some text here" name="description" style="width:100%"></textarea>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <label for="sponser_id">Featured Image</label>
                      <input type="file" name="image" class="form-control" required>
                  </div>
               </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection
<script type="text/javascript">
   function categorys(element){
    var category = $(element).val();    
    if(category){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_category')}}/"+category+'/edit',
           success:function(res){               
            if(res){
                $("#sub_service").empty();
                $("#sub_service").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#sub_service").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">
   function subcategoryss(element){
    var subcategorys = $(element).val();    
    if(subcategorys){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_sub_category')}}/"+subcategorys+'/edit',
           success:function(res){               
            if(res){
                $("#subsubcategory").empty();
                $("#subsubcategory").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#subsubcategory").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">

    $('#title').keyup(function(){
        var title = $('#title').val();   
        console.log(title); 
        $('#meta_title').val(title);
        $('#meta_keywords').val(title);
      });
</script>