<title>Product List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
     <a href="{{route('product.create')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Create Product</a>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-xs-12">
         <div class="box" style="overflow-y: scroll;">
            <!-- /.box-header -->
            <div class="box-body">
              <!-- /.box-body -->
               <table class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>Sno.</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Product Name</th>
                        <th>Created At</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                 
                     @php $i=1; @endphp
                     @foreach($data as $datas)
                        <tr>
                          <td>{{$datas->id}}</td>
                          <td>{{$datas->category}}</td>
                          <td>{{$datas->sub_category}}</td>
                          <td>{{$datas->title}}</td>
                          <td>{{$datas->created_at}}</td>
                          <td><a href="{{route('product.edit',$datas->id)}}" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a></td>
                        </tr>
                     @endforeach
                     
                  </tbody>
               </table>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  {{ $data->links() }}
              </div>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
@endsection


<script type="text/javascript">
   function categorys(element){
    var category = $(element).val();    
    if(category){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_category')}}/"+category+'/edit',
           success:function(res){               
            if(res){
                $("#sub_service").empty();
                $("#sub_service").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#sub_service").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">
   function subcategoryss(element){
    var subcategorys = $(element).val();    
    if(subcategorys){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_sub_category')}}/"+subcategorys+'/edit',
           success:function(res){               
            if(res){
                $("#subsubcategory").empty();
                $("#subsubcategory").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#subsubcategory").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>