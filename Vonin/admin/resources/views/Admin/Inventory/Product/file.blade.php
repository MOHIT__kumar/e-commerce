<title>Product List</title>
@extends('layouts.admin')
<style type="text/css">
  #internal {
    width: 200px;
    height: 100px;
    background: #999999;
    overflow: auto;
}
</style>
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
      <!-- <a href="#" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> </a> -->
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li><a href="{{route('product.edit',$id)}}">Edit Product</a></li>
            <li><a href="{{route('product_size.edit',$id)}}">Product Size</a></li>
            <!-- <li><a href="{{route('product_size.show',$id)}}">Product Atribute</a></li> -->
            <li><a href="{{route('stock.edit',$id)}}">Stock</a></li>
            <li  class="active"><a href="{{route('product.show',$id)}}">File</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{route('product_size.index')}}" method="post" enctype="multipart/form-data">
          @csrf
          @method('GET')
          <input type="hidden" name="id" value="{{$id}}">
            <div class="box-body">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Select Color</label>
                     <select class="form-control" name="file_type" onchange="files(this)">
                       <option>Select color</option>
                       <option value="red">Red</option>
                       <option value="blue">Blue</option>
                       <option value="green">Green</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Files</label>
                     <input type="file" name="file" id="file" class="form-control" required placeholder="Enter Video Embedded Code"> 
                  </div> 
               </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>

   <div class="col-xs-12">
      <div class="box box-primary">
         <div class="box-body" style="overflow-y: auto;">
            <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                      <th>Files</th>
                      <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                    
                    @foreach($data as $data)
                      <tr>
                        <td>
                          @if($data->file_type == 2)
                            <div id="internal">{!! html_entity_decode($data->files) !!}</div>
                          @else
                            <img src="{{URL::to('img/product/'.$data->files)}}" height="50">
                          @endif
                        </td>
                        <td>
                          <form action="{{route('product.destroy',$data->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i></button>
                          </form>
                        </td>
                      </tr>
                    @endforeach                   
                
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection


<script type="text/javascript">
   function files(element){
    var file_type = $(element).val();
      if (file_type == 2) {
        document.getElementById('file').type = 'text';
      }
      else{
         document.getElementById('file').type = 'file';
      }
     
    };
</script>
