   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{URL::To('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::To('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::To('bower_components/Ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{URL::To('dist/css/AdminLTE.min.css')}}" rel="stylesheet">
    <link href="{{URL::To('plugins/iCheck/square/blue.css')}}" rel="stylesheet"> 
    <link href="{{URL::To('dist/css/skins/_all-skins.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="{{URL::To('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" href="{{URL::To('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
     <link rel="stylesheet" href="{{URL::to('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{URL::to('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
   <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{URL::to('plugins/timepicker/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('bower_components/select2/dist/css/select2.min.css')}}">