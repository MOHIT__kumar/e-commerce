<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
})->name('welcome');*/

Auth::routes();
Route::get('/user_login', 'Login_RegistrationController@user_login');

Route::post('/user_register', 'Login_RegistrationController@register');
Route::get('/otp_send', 'Login_RegistrationController@login')->name('otp_veriftication');
Route::post('/otp_verification','Login_RegistrationController@otp_veriftication');
Route::get('auth/logout','Login_RegistrationController@logout');

Route::get('/my_account', 'My_AccountController@view')->name('my_account');
Route::get('/bill/{id}', 'My_AccountController@bill');

Route::get('/', 'WebsiteController@home')->name('home');
Route::get('/about', 'WebsiteController@about')->name('about');
Route::get('/contact', 'WebsiteController@contact')->name('contact');

Route::get('/wishlist/{id}', 'WishlistController@store')->name('wishlist');
Route::get('/wishlistview', 'WishlistController@view')->name('wishlistview');
Route::get('/deletewishlist/{id}','WishlistController@delete');

Route::get('/store/{id?}', 'Product_ListController@store')->name('store');
Route::get('/sub_cat_store/{id}', 'Product_ListController@sub_cat');
Route::get('/child_store/{id}', 'Product_ListController@child');
Route::get('/sub_category/{id}','Product_ListController@sub_sub_category');
// Route::get('/sub_sub_cat/{id}','Product_ListController@sub_sub_subcat');

Route::get('/terms', 'WebsiteController@terms')->name('terms');
Route::get('/privacy', 'WebsiteController@privacy')->name('privacy');

Route::get('single_product/{id}','single_productController@single_product')->name('single_product');
Route::get('amount/{id}','single_productController@amount')->name('amount');

Route::post('addtocart','CartController@store')->name('addtocart');
Route::post('update/cart', 'CartController@update_cart');
Route::get('deletecart/{id}','CartController@delete');
Route::get('addtocartview','CartController@view')->name('addtocartview');

Route::get('/checkoutview', 'CheckoutController@checkout')->name('checkoutview');
Route::post('/checkoutstore', 'CheckoutController@store');
	
Route::post('checkoutpayment','CheckoutController@checkoutpayment');
Route::post('razorpay','RazorPayController@start_payment');
Route::post('payment','RazorPayController@complete');

Route::get('quiz','QuizController@index');
Route::post('question/store','QuizController@answers')->name('quiz');
Route::get('result','QuizController@result');





