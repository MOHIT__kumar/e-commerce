<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\str;
use Razorpay\Api\Api;
use App\Model\Clc\Clcapplication;
use DB;
use Session;
use Auth;
use random;
use carbon\carbon;
use Mail;
use App\Model\Checkout;
use App\Model\Checkout_cart;

class RazorPayController extends Controller
{
    private $razorpayId = "rzp_test_NBKtYvP3YaLx0d";
    private $razorpayKey = "DGF3CExgBbTHOoh51wTvyScZ";

    public function start_payment(Request $request)
    { 
        $request->validate([
            'payment_method'  => 'required',
            'product_amount'  => 'required',
            'discount_amount' => 'required',
        ]);
    	
            // for cash on delivery
        if($request->payment_method==0)
        {
                $data = DB::table('address')->where('address.user_id',Auth::user()->id)->first();

                $checkouty_data=Checkout::create([
                'user_id'            => $data->user_id,
                'address'            => $data->id,
                'mobile'             => $data->mobile,
                'pincode'            => $data->pincode,
                'discount_amount'    => $request->discount_amount,
                'total_price'        => $request->product_amount
                ]);           
                
                // fething necessery data to insert into checkout_cart
                $carts_data = DB::table('carts')
                                ->where('carts.user_id',Auth::user()->id)
                                ->get();
                 // insert into checkout_cart
                foreach($carts_data as $data)
                {
                Checkout_cart::create([            
                    'checkout_id'        =>      $checkouty_data->id,
                    'product_id'         =>      $data->product_id,
                    'size'               =>      $data->size,
                    'user_id'            =>      $data->user_id,
                    'qty'                =>      $data->qty,
                    'price'              =>      $data->price
                    ]);
            }
            // delete cart data
            DB::table('carts')->where('carts.user_id',Auth::user()->id)->delete();

            Session::flash('success', 'Product Ordered successfully');

            return redirect()->route('addtocartview');
       }

        // for online payment
        else{
            $total_price=$request->product_amount;
            $data = DB::table('address')->where('address.user_id',Auth::user()->id)->first();

            $api = new Api($this->razorpayId,$this->razorpayKey);

            $order = $api->order->create(array(
              'amount'          => $total_price * 100,
              'currency'        => 'INR'
              )
            );

            $response = [
                'orderId'         => $order['id'],
                'razorpayId'      => $this->razorpayId,
                'amount'          => $order['amount'],
                'currency'        => 'INR',
                'name'            => $data->name,
                'mobile'          => $data->mobile,
                'email'           => $data->email,
                'address'         => $data->address1.' '.$data->address2.' '.$data->city.' '.$data->state.' '.$data->pincode,
                'massage'         => 'Books',
                'discount_amount' => $request->discount_amount,
                'total_amount'    => $request->product_amount,
            ];
            // dd($response);

            $address = DB::table('users')->where('users.id',Auth::user()->id)
                                         ->leftjoin('address','address.user_id','=','users.id')
                                         ->select('address.*')
                                         ->first();

            $subtotal = DB::table('carts')->where('user_id',auth::user()->id)
                                            ->get();

            $pincode = DB::table('pincode')->where('pincode',$address->pincode)
                                           ->first();

            $final_amount=0;
        
            foreach($subtotal as $total)
            {
                $final_amount+=$total->price;
            }

        $discount_amount = $request->discount_amount;
        return view('checkout',compact('response','final_amount','address','pincode','discount_amount'));
      
        }

    }  

    public function complete(Request $request)
    {
    	$SignatureStatus = $this->SignatureVerify(
    	  	$request->all()['rzp_payment_id'],
    	  	$request->all()['rzp_order_id'],
    	  	$request->all()['rzp_signature']
    	  );

    	if ($SignatureStatus == true) {
            
    	}
    	else{

            /*update payment details*/
    		$res = DB::table('payment_collection')->insert([
    			'user_id'	   => Auth::user()->id,
                'receipt_no'   => $this->generate_receipt_no(),
    			'name'         => $request->name,
    			'email'        => $request->email,
    			'address'      => $request->address,
    			'mobile'       => $request->mobile,
    			'amount'       => $request->amount / 100,
    			'payment_id'   => $request->rzp_payment_id,
    			'order_id'     => $request->rzp_order_id,
    			'signature_id' => $request->rzp_signature,
                'date' 		   => carbon::now(),
    		]);

            /*insert in checkout*/
            $data = DB::table('address')->where('address.user_id',Auth::user()->id)->first();

            $checkout_data=Checkout::create([
                'user_id'            =>      Auth::user()->id,
                'address'            =>      $data->id,
                'phone'              =>      $data->mobile,
                'pincode'            =>      $data->pincode,
                'transaction_id'     =>      $this->generate_receipt_no(),
                'discount_amount'    =>      $request->discount_amount,
                'total_price'        =>      $request->total_amount,
            ]);           
            
            // fething necessery data to insert into checkout_cart
            $carts_data = DB::table('carts')
                            ->where('carts.user_id',Auth::user()->id)
                            ->get();
             // insert into checkout_cart
            foreach($carts_data as $data)
            {
                Checkout_cart::create([            
                'checkout_id'        =>      $checkout_data->id,
                'product_id'         =>      $data->product_id,
                'size'               =>      $data->size,
                'user_id'            =>      $data->user_id,
                'qty'                =>      $data->qty,
                'price'              =>      $data->price
                ]);

    	   }
    		DB::table('carts')->where('user_id',Auth::user()->id)->delete();
    		return response()->json(['status' => 1]);
        }

    }

    private function SignatureVerify($_signature,$_payment_id,$_order_id)
    {
    	try
    	{
    		$api = new Api($this->razorpayId,$this->razorpayKey);
    		 $attributes  = array('razorpay_signature'  => $_signature,  'razorpay_payment_id'  => $_payment_id’ ,  'razorpay_order_id' => $_order_id’);
			 $order  = $api->utility->verifyPaymentSignature($attributes);
			 return true;
    	}
    	catch(\Exception $e)
    	{
    		return false;
    	}
    }

    public function generate_receipt_no()
    {
                // generate receipt no 
        $receipt_last = DB::table('payment_collection')->orderby('id', 'desc')->first();
        if($receipt_last){
            $receipt_no = $receipt_last->id+1;
        }
        else{
            $receipt_no = 1;
        }

        $receipt_no = 'Accurate'.'000'.$receipt_no;
          return $receipt_no;
        //dd($receipt_no);
    }

}
	