<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Config;

class My_AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {   	
        $address = DB::table('users')->where('users.id',Auth::user()->id)
                                     ->leftjoin('address','address.user_id','=','users.id')
                                     ->select('address.*')
                                     ->first();
                               // dd($orders);
                                             
        $data = DB::table('checkouts')->join('users','users.id','checkouts.user_id')
                                     ->where('user_id',Auth::user()->id)
                                     ->select('checkouts.*','users.name','users.email')
                                     ->paginate(10);
    
    	return view('my_account',compact('address','data'));
    }

    public function bill($id)
    {
        $cart = DB::table('checkout_carts')->where('checkout_id',$id)
                                           ->join('products','checkout_carts.product_id','products.id')
                                           ->select('checkout_carts.*','products.title','products.amount')
                                           ->get();
        // dd($cart);

        $total=DB::table('checkouts')->where('checkouts.id',$id)
                                     ->join('address','checkouts.address','address.id')
                                     ->select('checkouts.*','address.name','address.address1','address.address2','address.pincode','address.city','address.state','address.mobile')
                                     ->first();
             //dd($total);
        
        return view('bills',compact('cart','total'));

    }

}
