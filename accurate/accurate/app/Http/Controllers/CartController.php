<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cart;
use Auth;   
use Carbon\Carbon;
use Session;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {
        $is_product_exist=DB::table('products')->where('id',$request->product_id)->count();
        // dd($is_product_exist);
        // check product exist or not
        if($is_product_exist==1)
        {
            $oldcart=DB::table('carts')->where('user_id',auth::user()->id)
                                       ->where('product_id',$request->product_id)
                                       ->first();
                // dd($oldcart);                                     
            if($oldcart) 
            {
                Session::flash('error', 'product Already in Your Cart');
                return back();                        
            }
            // insert new product
            else
            {
                $product = DB::table('products')->where('products.id',$request->product_id)->first();
                    //  dd($product->title);

                DB::table('carts')->insert([
                    'product_id'    => $request->product_id,
                    'size'          => $request->size,
                    'qty'           => $request->qty,
                    'price'         => $product->amount * $request->qty,
                    'user_id'       => auth::user()->id,
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);

            Session::flash('success', 'product added successfully');
            return back();
            
            }
                               
        }
        else{
            Session::flash('error', 'Product id does not exist');
            return back();
        }

    }


    public function delete($id)

    {
        // dd($id);
        DB::table('carts')->where('id', $id)->delete();

        return redirect()->route('addtocartview');
    }

    public function view()
    {        
        $carts=DB::table('carts')->join('products','carts.product_id', 'products.id')
                                ->where('user_id',auth::user()->id)
                                ->select("carts.*", 'products.main_image','products.title as product_name')
                                ->get();
                    //dd($carts);
        return view('addtocart',compact('carts'));
    }

    public function update_cart(request $request)
    {
        $response=DB::table("carts")->where("id", $request->id)->update([
                                'qty'  => $request->qty,
                                'price'=> $request->price * $request->qty
                            ]);
        // dd($response);
        return response()->json(['status'   => 1]);
    }
}
