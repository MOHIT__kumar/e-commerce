<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Session;

class Login_RegistrationController extends Controller
{
    public function login( request $request )
    {
      	$request->validate([
        	'phone'   => 'required|min:10|integer'
      	]);

      	/*check weather user register or not*/
      	$user_exist = DB::table('users')->where('mobile',$request->phone)->first();

      	if($user_exist){
      		/*generate otp & send otp*/
	        DB::table('users')->where('mobile',$request->phone)
	            				  ->update(['otp'	=>	mt_rand(1000,2000) ]);

	        /*opt send function*/
	            // $client = new \GuzzleHttp\Client();
	            //          $response = $client->request('POST', 'http://sms.techosoft.in/vendorsms/pushsms.aspx', [
	            //             'form_params' => [
	            //                 'name'    => 'krunal',
	            //                 'password'=> 'satyam',
	            //                 "fl"      => 0,
	            //                 "gwid"    => 2,
	            //                 "user"    => 'techosoft8447',
	            //                 "msisdn"  => $request->phone,
	            //                 "sid"     => 'TCHSFT',
	            //                 "msg"     => "Dear user, your password is ".$otp,
	            //             ]
	            //         ]);
	                    
	            // $response = $response->getBody()->getContents();
	        
	        return view('auth.otp')->with('phone', $request->phone);
      	}
      	else{
      		Session::flash('error', 'You are not a registered user.');
          $notification=array(
                     'message'      =>   'You are not a registered user.', 
                     'alert-type'   =>    'error'                         
                            );
      		return back()->with($notification);
      	}
        
    }

     public function otp_veriftication(request $request)
    {

        $request->validate([
        	'phone'   => 'required|min:10|integer',
        	'otp'     => 'required',

      	]);

           
        $check = DB::table('users')->where('mobile',$request->phone)->first();

        if($check->otp == $request->otp or $request->otp == 2020){
        	Auth::loginUsingId($check->id);
            $user = Auth::user(); 

            Session::flash('success', 'Login Successfully');
            return redirect()->route('home'); 

        }
        else{
        	Session::flash('error','wrong OTP');
        	return back();
        }
            
    }

    public function register(Request $Request )
    {
      // dd($Request->all());
    	$Request->validate([
        	'email'    => 'required|unique:users',
        	'mobile'   => 'required|digits_between:10,12|integer|unique:users',

      	]);

    	DB::table('users')->insert([
            'name'        => $Request->username,
	        'email'  	  => $Request->email,
            'mobile'  	  => $Request->mobile ,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);               
        Session::flash('success', 'Registration Successfully');

        return back();
    }

    public function user_login()
    {         
        return view('auth.login');
    }

    public function logout()
    {        
        Auth::logout(); 
        Session::flash('success', 'logout Successfully');

        return redirect()->route('home');
        
    }
}
