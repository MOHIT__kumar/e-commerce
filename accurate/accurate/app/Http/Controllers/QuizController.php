<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;

class QuizController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	 $questions = DB::table('questions')->where('status',1)->paginate(1);
    	return view('quiz',compact('questions'));
    }

    public function answers(Request $request)
    {
    	//dd($request->all());
    	$question = DB::table('questions')->where('id',$request->question_id)->first();

    	 $answer = DB::table('answer')->where('user_id',Auth::user()->id)->where('question_id',$request->question_id)->first();
           // dd($answer);
    	  if ($answer) {
    	  	Session::flash('error','This answer is already submitted by you');
    	  }
    	  else
    	  {


    	 if ($question->answer == $request->answer)
    	  {
    	 	DB::table('answer')->insert([
    	 		'question_id'  => $request->question_id,
    	 		'answers'      => $request->answer,
    	 		'marks'        => 1,
    	 		'user_id'      => Auth::user()->id,
    	 	]);
    	  }
    	  else
    	  {
    	  	DB::table('answer')->insert([
    	 		'question_id'  => $request->question_id,
    	 		'answers'      => $request->answer,
    	 		'marks'        => 0,
    	 		'user_id'      => Auth::user()->id,
    	 	]);
    	  }	
    	  }
    	 	return back();
	
    }

    public function result()
    {
    	$questions = DB::table('answer')->where('user_id',Auth::user()->id)->count();
    	$right     = DB::table('answer')->where('user_id',Auth::user()->id)->where('marks',1)->count();
    	$wrong     = DB::table('answer')->where('user_id',Auth::user()->id)->where('marks',0)->count();
    	$result    = [ 'total_question' => $questions,
    				   'right_answer'  => $right,
    				   'wrong_answer'   => $wrong
    				   ];

    	 return response()->json($result);
    }
}
