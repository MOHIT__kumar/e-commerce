<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WebsiteController extends Controller
{
    public function home()
    {
        $categories = DB::table('categories')->get(); 
        $products   = DB::table('products')->get(); 
        $banners    = DB::table('banners')->get(); 
        //  dd($products);
        return view('home',compact('categories','products','banners'));
    } 

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function copy()
    {
        return view('copy');
    }
}
