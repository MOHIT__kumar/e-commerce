<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Session;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function checkout(Request $request)
    {
        $address = DB::table('users')->where('users.id',Auth::user()->id)
                                     ->leftjoin('address','address.user_id','=','users.id')
                                     ->select('address.*')
                                     ->first();

        $subtotal = DB::table('carts')->where('user_id',auth::user()->id)
                                        ->get();

        $pincode = DB::table('pincode')->where('pincode',$address->pincode)
                                       ->first();

        $offer=DB::table('vouchers')->where('label',$request->Voucher)
                                    ->first();
                                       
        $final_amount=0;
        $discount_amount=0;
        
        foreach($subtotal as $total){
           $final_amount+=$total->price;
        }


        if($offer)
        {
            $discount_amount=$offer->value;
        }

        return view('checkout',compact(
            'final_amount',
            'address',
            'pincode',
            'offer',
            'discount_amount'
        ));   
    }

    public function store(Request $request)
    {
        $is_address_exist=DB::table('address')->where('user_id',auth::user()->id)->count();
        // dd($is_address_exist);

         $request->validate([
            'fname'         =>      'required|min:2|max:30',
            'lname'         =>      'nullable',
            'country'       =>      'required',
            'address1'      =>      'required',
            'address2'      =>      'required',
            'city'          =>      'required',
            'state'         =>      'required',
            'postcode'      =>      'required',
            'mobile'        =>      'required|integer|digits_between:10,12',
            'email'         =>      'required|email',

        ]);
        if($is_address_exist==1)
        {
            DB::table("address")->where("user_id",auth::user()->id)
            ->update([             
            'user_id'       =>      auth::user()->id,
            'name'          =>      $request->fname.' '.$request->lname,
            'address1'      =>      $request->address1,
            'address2'      =>      $request->address2,
            'pincode'       =>      $request->postcode,
            'email'         =>      $request->email,
            'country'       =>      $request->country,
            'city'          =>      $request->city,
            'state'         =>      $request->state,
            'mobile'        =>      $request->mobile,
            'created_at'    =>      Carbon::now(),
            'updated_at'    =>      Carbon::now()

            ]);
            Session::flash('success', 'address updated successfully');
        }

        else{

        DB::table('address')->insert([
            'user_id'       =>      auth::user()->id,
            'name'          =>      $request->fname.' '.$request->lname,
            'address1'      =>      $request->address1,
            'address2'      =>      $request->address2,
            'pincode'       =>      $request->postcode,
            'email'         =>      $request->email,
            'country'       =>      $request->country,
            'city'          =>      $request->city,
            'state'         =>      $request->state,
            'mobile'        =>      $request->mobile,
            'created_at'    =>      Carbon::now(),
            'updated_at'    =>      Carbon::now()
        ]); 
            Session::flash('success', 'address added successfully');
        }

        return back();    
    }
}
	