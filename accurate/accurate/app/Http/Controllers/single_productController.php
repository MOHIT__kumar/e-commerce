<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cart;
use Auth;
use Carbon\Carbon;

class single_productController extends Controller
{
    public function single_product($id)
    {
        
        // for printing the amount of different sizez 
        $size=DB::table('productsizes')->where('product_id',$id)->get(); 
        // for printing the name of different sizez 
        $data = DB::table('products')->where('id',$id)->first();
        // for printing the related products
        $products = DB::table('products')
        ->where('sub_category_id',$data->sub_category_id)
        ->where('sub_sub_category_id',$data->sub_sub_category_id)
        ->get();
          $product = DB::table('products')->where('id',$id)->first();
        //  dd($products);
        // for printing the amount of different sizez 
        $productsizes=DB::table('productsizes')->where('product_id',$id)->first();
        // for images 
        $image=DB::table('product_file')->where('product_id',$id)->first(); 
        $images=DB::table('product_file')->where('product_id',$id)->get(); 
        //  dd($image);  

        return view('single_product',compact('data','products','size','productsizes','image','images','product'));
    }

    public function amount($id)
	{
		$amount=DB::table("productsizes")
            ->where('id',$id)->pluck('id','amount');
            /*dd($amount);*/
            return response()->json($amount);
    }
}
