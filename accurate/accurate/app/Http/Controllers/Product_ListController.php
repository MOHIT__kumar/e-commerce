<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Product_ListController extends Controller
{
    public function store($id = null, request $request)
    {  
        if ($request->search) {
            $products = DB::table('products')->Where('title', 'like','%'.$request->search.'%') 
                                            ->paginate(10); 
        }
        else{
            $products = DB::table('products')->where('category_id',$id)->paginate(10); 
        }

        // dd($products);
        return view('store',compact('products'));
    }
    public function category($id)
    {
       $products = DB::table('products')->where('category',$id)->get(); 
        // dd($products);
        return view('store',compact('products')); 
    }

    public function sub_cat($id)
    {
        $products = DB::table('products')->where('sub_category_id',$id)->get(); 
        // dd($products);
        return view('store',compact('products'));
    }

    public function child($id)
    {
        $products = DB::table('products')->where('sub_sub_category_id',$id)->get(); 
        // dd($products);
        return view('store',compact('products'));
    }

    public function sub_sub_category($id)
    {
        $products = DB::table('products')->where('sub_sub_sub_category_id',$id)->get(); 
        return view('store',compact('products'));
    }
}
