<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Checkout_cart extends Model
{ 
    Protected $fillable = ['checkout_id','product_id','size','user_id','qty','price'];

    Protected $table='checkout_carts';

    public $primaryKey='id';
}
