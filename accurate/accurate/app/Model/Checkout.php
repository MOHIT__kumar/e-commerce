<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    
    Protected $fillable = ['user_id','address','phone','pincode','transaction_id','delivery_charges','total_price','discount_amount'];

    Protected $table='checkouts';

    public $primaryKey='id';
}
