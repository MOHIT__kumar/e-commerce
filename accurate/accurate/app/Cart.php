<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends ldap_mod_del(link_identifier, dn, entry)
{
    
    Protected $fillable = ['product_id'];

    Protected $table='carts';

    public $primaryKey='id';
}
