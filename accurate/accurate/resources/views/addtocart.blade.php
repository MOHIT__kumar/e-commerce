@extends('layouts.app')

@section('content')

								@php
										$subtotal=0;
								@endphp
        <main class="main">
        	<div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg');margin-top: 100px;">
        		<div class="container">
        			<h1 class="page-title">Shopping Cart<span>Shop</span></h1>
        		</div><!-- End .container -->
        	</div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Shop</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
            	<div class="cart">
	                <div class="container">
	                	<div class="row">
	                		<div class="col-lg-9">
	                			<table class="table table-cart table-mobile">
									<thead>
										<tr>
											<th>Product</th>
											<th>Price</th>
											<th>Quantity</th>
											<th>Total</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
                        @foreach($carts as $data)

										<tr>
											<td class="product-col">
												<div class="product">
													<figure class="product-media">
															<a href="{{ route('single_product',$data->product_id) }}"><img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="Product image"></a>
													</figure>
													<h3 class="product-title">
														<a href="{{ route('single_product',$data->product_id) }}">{{$data->product_name}}</a>
													</h3><!-- End .product-title -->
												</div><!-- End .product -->
											</td>
											<td id="price{{$data->id}}" class="price-col">{{$data->price / $data->qty}}</td>
											<td class="quantity-col">
                                                <div class="cart-product-quantity">
                                                    <input id="qty{{$data->id}}"  type="number" class="form-control" value="{{$data->qty}}" min="1" max="100" step="1" data-decimals="0" required="" onchange="display('{{$data->id}}')">
                                                </div><!-- End .cart-product-quantity -->
                                            </td>
                                                  
										<td id="total{{$data->id}}" class="total-col">{{$data->price}}</td>
				
										<input type="hidden" id="subtotal" name="subtotal" value="{{$subtotal+=$data->price}}">
																				
										<td class="remove-col"><a href="{{URL::To('deletecart',$data->id)}}"><button class="btn-remove"><i class="icon-close"></i></button></a></td>
										</tr>
						@endforeach
									</tbody>
								</table><!-- End .table table-wishlist -->
	                		</div><!-- End .col-lg-9 -->
	                		<aside class="col-lg-3">
	                			<div class="summary summary-cart">
	                				<h3 class="summary-title">Cart Total</h3><!-- End .summary-title -->

	                				<table class="table table-summary">
	                					<tbody>
	                						<tr class="summary-subtotal">
	                							<td>Subtotal:</td>
	                							<td><div id="subtotalamount">{{$subtotal}}</div></td>
	                						</tr><!-- End .summary-subtotal -->
	                							                						
	                					</tbody>
	                				</table><!-- End .table table-summary -->

	                				<a href="{{URL::To('checkoutview')}}" class="btn btn-outline-primary-2 btn-order btn-block">PROCEED TO CHECKOUT</a>
	                			</div>
	                		</aside><!-- End .col-lg-3 -->
	                	</div><!-- End .row -->
	                </div><!-- End .container -->
                </div><!-- End .cart -->
            </div><!-- End .page-content -->
        </main>

@endsection

@section('addtocart-page-js')

<script>
	function display(id){
		var p = '#price'+id;
		var q   = '#qty'+id;
		var t  = "#total"+id;
		var price = $(p).text();
		var qty  = $(q).val();
		$(t).text(price * qty);

		var ids = [];
		@foreach($carts as $data)
			ids.push("{{$data->id}}");
		@endforeach

		var length=ids.length;
		var total_amount = 0;
		for(i=0; i<length; i++){
			var total = "#total"+ids[i];
			
			total_amount = total_amount + parseInt($(total).text());
		}
		$('#subtotalamount').text(total_amount);
		// console.log(total_amount );

		$.ajax({
                url: "{{URL::TO('update/cart')}}",
                type: 'post',
                dataType: 'json',
                data: { 
                        "_token": "{{ csrf_token() }}",
                        id          : id,
                        qty         : qty,
                        price       : price,
                },
        });
    }

</script>
@endsection
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
