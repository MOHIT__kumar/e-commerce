@extends('layouts.app')

@section('content')

        <main class="main" style="margin-top: -50px;">
            <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Login</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="login-page bg-image pt-8 pb-8 pt-md-12 pb-md-12 pt-lg-17 pb-lg-17" style="background-image: url('images/backgrounds/login-bg.jpg')">
                <div class="container">
                    <div class="form-box">
                    
                            <ul class="nav nav-pills nav-fill" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="signin-tab-2" aria-controls="signin-2" aria-selected="false"><b>Otp Verification<b></b></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="signin-2">
                                    <form action="{{URL::TO('otp_verification')}}" method="post">
                                        @csrf
                                        @method('post')

                                        <div class="form-group">
                                            <label for="singin-password-2">Phone</label>
                                            <input type="text" class="form-control" name="phone" value="{{$phone}}" readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="singin-password-2">Type Your Otp</label>
                                            <input type="text" class="form-control" name="otp" required="">
                                        </div>

                                        <div class="form-footer">
                                            <button type="submit" class="btn btn-outline-primary-2">
                                                <span>LOG IN</span>
                                                <i class="icon-long-arrow-right"></i>
                                            </button>


                                            <a href="#" class="forgot-link" style="margin-left: 90px;">Resend Otp</a>
                                        </div><!-- End .form-footer -->
                                    </form>
                                </div><!-- .End .tab-pane -->
                                </div>
                            
                        </div>
                    </div>
                </div>
            </main>
@endsection
