@extends('layouts.app')
@section('content')
        <main class="main">
            <div class="intro-slider-container mb-0">
                <div class="intro-slider owl-carousel owl-theme owl-nav-inside owl-light" data-toggle="owl" data-owl-options='{"nav": false, "dots": false}'>
                    @foreach($banners as $banner)
                    <div class="intro-slide" style="background-image: url('images/menu/banner-3.jpg');">
                        <div class="container intro-content text-center">
                            <h3 class="intro-subtitle text-white">Don’t Miss</h3>
                            <!-- End .h3 intro-subtitle -->
                            <h1 class="intro-title text-white">Mystery Deals</h1>
                            <!-- End .intro-title -->
                            <div class="intro-text text-white">Online Only</div>
                            <!-- End .intro-text -->
                        </div>
                        <!-- End .intro-content -->
                    </div>
                    @endforeach
                    <!-- End .intro-slide -->
                </div>  

                <span class="slider-loader text-white"></span>
                <!-- End .slider-loader -->
            </div><br><br>
            <!-- End .intro-slider-container -->
            <div class="container">
                <div class="banner-group">
                    <div class="row">
                 @foreach($categories as $categories)
                        
                        <div class="col-md-4">
                            <div class="banner banner-border-hover">
                                <a href="{{URL::To('store',$categories->id)}}">
                                    <img src="{{Config::get('app.admin_url')}}/img/category/{{$categories->image}}" alt="Banner">
                                </a>
                                <div class="banner-content">
                                    <h4 class="banner-subtitle"><a href="{{URL::To('store',$categories->id)}}">
                                        {{$categories->category}}</a></h4>
                                    <!-- End .banner-subtitle -->
                                    <h3 class="banner-title"><a href="{{URL::To('store',$categories->id)}}"><span>{{$categories->description}}</span></a></h3>
                                    <!-- End .banner-title -->
                                    <a href="{{URL::To('store',$categories->id)}}" class="btn btn-outline-primary-2 banner-link">Shop Now<i class="icon-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>

                    @endforeach
        
                
                </div>
                <!-- End .banner-group -->
            </div>
            <!-- End .container -->

            <div class="mb-4"></div>
            <!-- End .mb-6 -->

            <div class="container">
                <div class="heading heading-center mb-3">
                    <h2 class="title">Trending Products</h2>
                </div>
                <!-- End .heading -->

                <div class="tab-content tab-content-carousel">
                    <div class="tab-pane p-0 fade show active" id="trendy-all-tab" role="tabpanel" aria-labelledby="trendy-all-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                            
                             @foreach($products as $product)
                             @if($product->id <= 7)  
                            <div class="product product-2">
                                <figure class="product-media">
                                    <a href="{{ route('single_product',$product->id) }}">
                                        <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" class="product-image" style="height: 276px;">
                                    </a>

                                    <div class="product-action-vertical">
                                    <a href="{{route('wishlist',$product->id)}}" class=" btn-product-icon btn-wishlist btn-expandable" title="Add to wishlist"><span>add to wishlist</span></a>
                                    </div>
                                    <!-- End .product-action -->

                                    <div class="product-action product-action-transparent">
                                       <div class="product-action product-action-transparent">
                                        <a href="{{ URL::to('single_product',$product->id) }}" class="btn-product btn-cart"><span>View Details</span></a>
                                        </div>
                                    </div>
                                    <!-- End .product-action -->
                                </figure>
                                <!-- End .product-media -->

                                <div class="product-body">
                                    <div class="product-cat">
                                        
                                    </div>
                                    <!-- End .product-cat -->
                                    <h3 class="product-title"><a href="{{ route('single_product',$product->id) }}">{{$product->title}}</a></h3>
                                    <span><strong>Rs. {{$product->amount}}</strong></span>
                                </div>
                        <!-- End .owl-carousel -->  
                            </div>
                            @endif
                    @endforeach  
              
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-women-tab" role="tabpanel" aria-labelledby="trendy-women-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-men-tab" role="tabpanel" aria-labelledby="trendy-men-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-access-tab" role="tabpanel" aria-labelledby="trendy-access-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                </div>
                <!-- End .tab-content -->
            </div>
            <!-- End .container -->

            <div class="mb-5"></div>
            <!-- End .mb-5 -->

            <div class="video-banner video-banner-bg bg-image text-center" style="background-image: url(images/banners/banner-1.jpeg)">
                <div class="container">
                    <h3 class="video-banner-title h1 text-white"><span>New Collection</span><strong>Winter’19 <i>/</i> Spring’20</strong></h3>
                </div>
                <!-- End .container -->
            </div>
            <!-- End .video-banner bg-image -->

            <div class="container pt-6 new-arrivals">
                <div class="heading heading-center mb-3">
                    <h2 class="title">New Arrivals</h2>
                    <!-- End .title -->
                </div>
                <!-- End .heading -->

                <div class="tab-content">
                    <div class="tab-pane p-0 fade show active" id="new-all-tab" role="tabpanel" aria-labelledby="new-all-link">
                        <div class="products">
                            
                <div class="tab-content tab-content-carousel">
                    <div class="tab-pane p-0 fade show active" id="trendy-all-tab" role="tabpanel" aria-labelledby="trendy-all-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                            
                             @foreach($products as $product)
                              @php $product_date = \Carbon\Carbon::parse($product->created_at)->format('d-m-Y');
                                   $date = \Carbon\Carbon::today()->toDateTimeString();
                                   $today = Carbon\Carbon::parse($date)->format('d-m-Y');
                                   $diff = \Carbon\Carbon::parse( $product_date )->diffInDays( $today );
                              @endphp
                             @if($diff < 10)
                            <div class="product product-2">
                                <figure class="product-media">
                                    <a href="{{ route('single_product',$product->id) }}">
                                        <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" class="product-image">
                                        <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" alt="Product image" class="product-image-hover">
                                    </a>

                                    <div class="product-action-vertical">
                                        <a href="{{route('wishlist',$product->id)}}" class="btn-product-icon btn-wishlist btn-expandable" title="Add to wishlist"><span>add to wishlist</span></a>
                                    </div>
                                    <!-- End .product-action -->

                                    <div class="product-action product-action-transparent">
                                       <div class="product-action product-action-transparent">
                                        <a href="{{ URL::to('single_product',$product->id) }}" class="btn-product btn-cart"><span>View Details</span></a>
                                        </div>
                                    </div>
                                    <!-- End .product-action -->
                                </figure>
                                <!-- End .product-media -->

                                <div class="product-body">
                                    <div class="product-cat">
                                        
                                    </div>
                                    <!-- End .product-cat -->
                                    <h3 class="product-title"><a href="{{ route('single_product',$product->id) }}">{{$product->title}}</a></h3>
                            
                                </div>
                        <!-- End .owl-carousel -->  
                            </div>
                            @endif
                    @endforeach  
              
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-women-tab" role="tabpanel" aria-labelledby="trendy-women-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-men-tab" role="tabpanel" aria-labelledby="trendy-men-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                    <div class="tab-pane p-0 fade" id="trendy-access-tab" role="tabpanel" aria-labelledby="trendy-access-link">
                        <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "margin": 20,
                                "loop": false,
                                "responsive": {
                                    "0": {
                                        "items":2
                                    },
                                    "480": {
                                        "items":2
                                    },
                                    "768": {
                                        "items":3
                                    },
                                    "992": {
                                        "items":4
                                    },
                                    "1200": {
                                        "items":4,
                                        "nav": true
                                    }
                                }
                            }'>
                        </div>
                        <!-- End .owl-carousel -->
                    </div>
                    <!-- .End .tab-pane -->
                </div>
                            <!-- End .row -->
                        </div>
                        <!-- End .products -->
                    </div>
                </div>
                <!-- End .more-container -->
            </div>
            <!-- End .container -->

            <div class="mb-2"></div>
            <!-- End .mb-2 -->
        </main>
        <br>
        <br>
        <!-- End .main -->
@endsection