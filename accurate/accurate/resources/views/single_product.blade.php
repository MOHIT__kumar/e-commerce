@extends('layouts.app')
@section('content')

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .carousel-control.left,
        .carousel-control.right {
            background-image: none;
        }

        .carousel-indicators li {
            display: none;
        }
    </style>
    <style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

</head>
<div class="container">
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0" style="margin-top: 100px;">
            <div class="container d-flex align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item">Products</li>
                </ol>
            </div>
            <!-- End .container -->
        </nav>
        <!-- End .breadcrumb-nav -->
        <!-- ================================================= -->
        <!-- ================================================= -->
        <!-- ================================================= -->
        <div id="myCarousel" class="carousel slide show-hide" data-interval="false">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active" style="background: white">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="Los Angeles" style="width: 100%;" />
                        </div>
                        <div class="col-md-4 text-center">
                        </div>
                         @if( $image ?? "")
                          <div class="col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$image->files}}" alt="Los Angeles" style="width: 100%" />
                        </div>
                          @endif
                    </div>
                </div>
                <div class="item" style="background: white;">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="Los Angeles" style="width: 100%" />
                        </div>
                        <div class="col-md-4 text-center"></div>
                       @if( $image ?? "")
                         <div class="col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$image->files}}" alt="Los Angeles" style="width: 100%" />
                        </div>
                        @endif
                    </div>
                </div>
                <div class="item" style="background: white;">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="Los Angeles" style="width: 100%" />
                        </div>
                        <div class="col-md-4 text-center"></div>
                        @if( $image ?? "")
                          <div class="col-md-4 text-center">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$image->files}}" alt="Los Angeles" style="width: 100%" />
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- mobile view -->
      <div class="show-hide-m">
            <div class="slideshow-container">
            <div class="mySlides">
              <img src="{{Config::get('app.admin_url')}}/img/product/{{$data->main_image}}" alt="Los Angeles" style="width: 100%" />
              <!-- <div class="text">Caption Text</div> -->
            </div>
            
            @foreach($images as $img)
            <div class="mySlides">
              <img src="{{Config::get('app.admin_url')}}/img/product/{{$img->files}}" alt="Los Angeles" style="width: 100%" />
              <div class="text">Caption Two</div>
            </div>
            @endforeach

            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            </div>
            <br>

            <div style="text-align:center">
              <span class="dot" onclick="currentSlide(1)"></span> 
              <span class="dot" onclick="currentSlide(2)"></span> 
              <span class="dot" onclick="currentSlide(3)"></span> 
        </div>
      </div>

        <!-- mends -->
        <script>
            $('.carousel').carousel({
                interval: false,
            });
        </script>
        <!-- ================================================= -->
        <!-- ================================================= -->
        <!-- ================================================= -->
        <div class="mt-3 specs">
            <form action="{{URL::to('addtocart')}}" method="post">
                @csrf
                <div class="product-details">
                    <h1 class="product-title text-center" style="width: 90%;">{{$data->title}}</h1>
                    <h2 class="product-title text-center" style="color: #cc6666;margin-left: -50px;" id="amount">₹{{$product->amount}}</h2>
                    <!-- End .product-price -->
                </div>
                <!-- End .details-filter-row -->
                <div>
                    <div class="">
                        <div class="details-action-col">
                            <label for="qty">Qty:</label>
                            <div class="product-details-quantity">
                                <input type="number" id="qty" class="form-control" value="1" min="1" max="10" step="1" data-decimals="0" name="qty" required="">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="product_id" value="{{$data->id}}">
                    <input type="hidden" name="name" value="{{$data->title}}">
                    <div class="product-price">
                        <input type="hidden" name="amount" id="amt" value="{{$product->amount}}">
                    </div>
                    <div class="product-details-action">
                        <!-- <div class="details-action-col">
               <a href="#" class="btn-product btn-cart" style="border: -1px;height: 40px;width: 160px; margin-left: 100px;">Add to Cart</a>
               </div> -->
                        <div class="product-details-action">
                            @guest
                            <div class="details-action-col">
                                <a href="{{URL::To('login')}}" class="btn-product btn-cart" style="margin-left: -90px;">Add to Cart</a>
                            </div>
                            @else
                            <div class="details-action-col">
                                <button type="submit" class="btn-product btn-cart" style="border: -1px;height: 40px;width: 160px; margin-left: -90px;">Add to Cart</button>
                            </div>
                            @endguest
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- =============================== -->
        <div class="product-details-tab mt-3">
            <ul class="nav nav-pills justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="product-desc-link" data-toggle="tab" href="#product-desc-tab" role="tab" aria-controls="product-desc-tab" aria-selected="true">Description</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="product-desc-tab" role="tabpanel" aria-labelledby="product-desc-link">
                    <div class="product-desc-content">
                        <div class="container-fluid">
                            <h3>Product Information</h3>
                            <p>{!!html_entity_decode ($data->description)!!}</p>
                        </div>
                        <!-- End .product-desc-content -->
                    </div>
                    <!-- .End .tab-pane -->
                </div>
                <!-- End .tab-content -->
            </div>
            <!-- End .product-details-tab -->
            <h2 class="title text-center mb-4">You May Also Like</h2>
            <!-- End .title text-center -->
            <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" data-owl-options='{
      "nav": false, 
      "dots": true,
      "margin": 20,
      "loop": false,
      "responsive": {
      "0": {
      "items":1
      },
      "480": {
      "items":2
      },
      "768": {
      "items":3
      },
      "992": {
      "items":4
      },
      "1200": {
      "items":4,
      "nav": true,
      "dots": false
      }
      }
      }'>
                @foreach($products as $product)
                <div class="product product-7 text-center">
                    <figure class="product-media">
                        <a href="{{ route('single_product',$product->id) }}">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" class="product-image">
                            <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" alt="Product image" class="product-image-hover">
                        </a>
                        <div class="product-action-vertical">
                            <a href="{{route('wishlist',$product->id)}}" class="btn-product-icon btn-wishlist btn-expandable"><span>add to wishlist</span></a>
                        </div>
                        <!-- End .product-action-vertical -->
                        <div class="product-action">
                            <a href="{{ URL::to('single_product',$product->id) }}" class="btn-product btn-cart"><span>View Details</span></a>
                        </div>
                        <!-- End .product-action -->
                    </figure>
                    <!-- End .product-media -->
                    <div class="product-body">
                        <h3 class="product-title">{{$product->title}}</h3>
                        <!-- End .product-title -->
                        <div class="ratings-container">
                        </div>
                        <!-- End .rating-container -->
                    </div>
                    <!-- End .product-body -->
                </div>
                <!-- End .product -->
                @endforeach
            </div>
            <!-- End .row -->
        </div>
        <!-- End .container -->
        @endsection
        @section('page-js')
        <script type="text/javascript">
            $(document).ready(function() {
                // alert('working');
                $("#size").change(function() {
                    var id = $("#size").val();
                    $.ajax({
                        type: "GET",
                        url: "{{URL::To('amount')}}" + '/' + id,
                        success: function(result) {
                            $("#amount").empty();
                            $.each(result, function(value) {
                                $("#amount").append('₹' + value);
                                $("#amt").empty();
                                $("#amt").append('<input type="hidden" id="amount" name="amount" value="' + value + '">');

                                //alert(amount);
                            });
                        }
                    });
                });
            });
        </script>
        <script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

        @endsection