<footer class="footer footer-2" style="background-color: #525252">
            <div class="footer-middle border-0">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="widget widget-about">
                                 <a href="{{route('home')}}" class="logo">
                                    <img src="{{URL::to('images/menu/accurate.png')}}" alt="Accurate" width="105" height="25">
                                 </a>
                                <p class="text-white">Praesent dapibus, neque id cursus ucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. </p>   
                            </div>
                            <!-- End .widget about-widget -->
                        </div>
                        <!-- End .col-sm-12 col-lg-3 -->

                        <div class="col-sm-4 col-lg-2 col-6">
                            <div class="widget">
                                <h4 class="widget-title">Information</h4>
                                <!-- End .widget-title -->

                                <ul class="widget-list">

                                    <li><a href="{{route('about')}}">About Us</a></li>
                                    <li><a href="{{route('contact')}}">Contact Us</a></li>
                                    <li><a href="{{URL::to('user_login')}}">Log in</a></li>
                                </ul>
                                <!-- End .widget-list -->
                            </div>
                            <!-- End .widget -->
                        </div>
                        <!-- End .col-sm-4 col-lg-3 -->

                        <div class="col-sm-4 col-lg-2 col-6">
                            <div class="widget">
                                <h4 class="widget-title">Customer Service</h4>
                                <!-- End .widget-title -->

                                <ul class="widget-list">
                                    <li><a href="{{route('terms')}}">Terms and conditions</a></li>
                                    <li><a href="{{route('privacy')}}">Privacy Policy</a></li>
                                </ul>
                                <!-- End .widget-list -->
                            </div>
                            <!-- End .widget -->
                        </div>
                        <!-- End .col-sm-4 col-lg-3 -->

                        <div class="col-sm-4 col-lg-2 col-6">
                            <div class="widget">
                                <h4 class="widget-title">My Account</h4>
                                <!-- End .widget-title -->

                                <ul class="widget-list">
                                    <li><a href="{{URL::to('user_login')}}">Sign In</a></li>
                                    <li><a href="{{route('addtocartview')}}">View Cart</a></li>
                                    <li><a href="{{route('wishlistview')}}">My Wishlist</a></li>
                                </ul>
                                <!-- End .widget-list -->
                            </div>
                            <!-- End .widget -->
                        </div>
                        <!-- End .col-sm-64 col-lg-3 -->
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .footer-middle -->

            <div class="footer-bottom">
                <div class="container">
                    <p class="footer-copyright" style="color: #fff">Copyright © 2020 Designed and Developed by <a href="{{URL::to('https://techosoft.in')}}" target="_blank">Techosoft Technologies</a></p>
                    <!-- End .footer-menu -->
                    <div class="social-icons social-icons-color">
                        <span class="social-label">Social Media</span>
                        <a href="#" class="social-icon social-facebook" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                        <a href="#" class="social-icon social-twitter" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                        <a href="#" class="social-icon social-instagram" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                        <a href="#" class="social-icon social-youtube" title="Youtube" target="_blank"><i class="icon-youtube"></i></a>
                        <a href="#" class="social-icon social-pinterest" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
                    </div>
                    <!-- End .soial-icons -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .footer-bottom -->
        </footer>