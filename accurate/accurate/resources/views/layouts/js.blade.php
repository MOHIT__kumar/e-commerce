
<!-- Plugins JS File -->
    <script src="{{URL::to('js\jquery.min.js')}}"></script>
    <script src="{{URL::to('js\bootstrap.bundle.min.js')}}"></script>
    <script src="{{URL::to('js\jquery.hoverIntent.min.js')}}"></script>
    <script src="{{URL::to('js\jquery.waypoints.min.js')}}"></script>
    <script src="{{URL::to('js\superfish.min.js')}}"></script>
    <script src="{{URL::to('js\owl.carousel.min.js')}}"></script>
    <script src="{{URL::to('js\jquery.magnific-popup.min.js')}}"></script>
    <script src="{{URL::to('js\jquery.plugin.min.js')}}"></script>
    <script src="{{URL::to('js\jquery.countdown.min.js')}}"></script>
    <script src="js\bootstrap-input-spinner.js"></script>

    <!-- Main JS File -->
    <script src="{{URL::to('js\main.js')}}"></script>
    <script src="{{URL::to('js\demos\demo-5.js')}}"></script>

    
    <!-- Google Map -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDc3LRykbLB-y8MuomRUIY0qH5S6xgBLX4"></script>

    
    <!-- toster cdn -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
        
    @yield('subtotal')
    
   @yield('page-js')

   @yield('checkoutpage-js')
   
    
    
    