<!DOCTYPE html>
<html>
<head>
      @include('layouts.css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
</head>
<body>
     @include('layouts.header')
      <div class="page-container">
         @yield('content')
      </div>
   @include('layouts.js')
   @yield('addtocart-page-js')
   @include('layouts.footer')



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            toastr.error("{{ $error }}");
        @endforeach
    @endif
</script>
<script>
  @if(Session::has('success'))
      toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
      toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
      toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
      toastr.error("{{ Session::get('error') }}");
  @endif
</script>
</body>
</html>