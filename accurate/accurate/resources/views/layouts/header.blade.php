<div class="page-wrapper">
<header class="header header-5">
   <div class="header-top">
      <div class="container">
         <div class="header-right" style="color: black;">
            <li style="margin-right: 2rem;list-style:"><a href="tel:#"><i class="icon-phone"></i>Call: +0123 456 789</a>
            </li>
            <!-- <li style="margin-right: 2rem;list-style:none"><a href="{{route('about')}}">About Us</a></li>
               <li style="margin-right: 2rem;list-style:none"><a href="{{route('contact')}}">Contact</a></li> -->
            <li style="margin-right: 2rem;list-style:none"><a href="{{URL::to('user_login')}}">Sign In/Sign Up</a>
            </li>
            <!-- End .top-menu -->
         </div>
         <!-- End .header-right -->
      </div>
      <!-- End .container -->
   </div>
   <!-- End .header-top -->
   <div class="header-middle sticky-header">
   <div class="container-fluid">
      <div class="header-left">
         <button class="mobile-menu-toggler"> <span class="sr-only">Toggle mobile menu</span>  <i class="icon-bars" style="    color: black;"></i> 
         </button>
         <a href="{{route('home')}}" class="logo">
         <img src="{{URL::to('images/menu/accurate.png')}}" alt="Accurate" width="105" height="25">
         </a>
         <nav class="main-nav">
            <ul class="menu sf-arrows">
               <li class="megamenu-container"> <a href="{{route('home')}}" style="color: black">Home</a> 
               </li>
               <!-- primary -->
               <li>
                  <a href="#" class="sf-with-ul" style="color: black;">Services</a>
                  <div class="megamenu megamenu-md">
                     <div class="row no-gutters">
                        <div class="col-md-12">
                           <div class="menu-col">
                              <div class="row">
                                 @php $categories=DB::table('categories')->get(); @endphp 
                                 @foreach($categories as $cat)
                                 <!-- parent -->
                                 <div class="col-md-4">
                                    <div class="menu-title"> 
                                       <a href="{{route('store',$cat->id)}}">
                                        {{$cat->category}}
                                       </a> 
                                    </div>
                                    @php $parent = DB::table('sub__categories')->where('category_id',$cat->id)->get(); @endphp
                                     @foreach($parent as $sub_sub)
                                    <!-- child -->
                                    <ul>
                                       <li>
                                          <a href="{{URL::To('sub_cat_store',$sub_sub->id)}}">                             
                                          {{$sub_sub->sub_category}}
                                          </a>
                                          <!-- sub category -->
                                          <ul>
                                             @php $child = DB::table('sub__sub__categories')->where('sub_category_id',$sub_sub->id)->get(); @endphp
                                              @foreach($child as $sub_3_category)
                                               <li style="padding-left: 1.5rem;padding-top: .5rem;">
                                                <a href="{{URL::to('child_store',$sub_3_category->id)}}">
                                                {{$sub_3_category->sub_sub_category}}
                                                </a>
                                                 @php $sub_sub_sub_cat = DB::table('sub_sub_sub_category')->where('sub_sub_sub_id',$sub_3_category->id)->get(); 
                                                @endphp
                                                <ul>
                                                   <li style="padding-left: 1.5rem;padding-top: .5rem;">
                                                 @foreach( $sub_sub_sub_cat as $subsub_category)
                                                      <a href="{{URL::to('sub_category',$subsub_category->id)}}">{{$subsub_category->subsubsubcategory}}</a>
                                                @endforeach
                                                   </li>
                                                </ul>
                                             </li>
                                              @endforeach
                                          </ul>
                                       </li>
                                    </ul>
                                    <!-- end child -->@endforeach
                                 </div>
                                 <!-- end parent -->@endforeach
                              </div>
                              <!-- End .row -->
                           </div>
                           <!-- End .menu-col -->
                        </div>
                     </div>
                     <!-- End .row -->
                  </div>
               </li>
               <!--end primary  -->
               <li class="megamenu-container"> <a href="{{URL::to('quiz')}}" style="color: black">Quiz</a> 
               </li>
            </ul>
            <!-- End .menu -->
         </nav>
         <!-- End .main-nav -->
      </div>
      <!-- End .header-left -->
      <div class="header-right">
         <div class="header-search header-search-extended header-search-visible">
            <a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
            <form action="{{URL::to('store')}}" method="get">
               <div class="header-search-wrapper">
                  <label for="q" class="sr-only">Search</label>
                  <input type="search" class="form-control" name="search" id="q" placeholder="Search product ..." required="">
                  <button class="btn btn-primary" type="submit"><i class="icon-search"></i>
                  </button>
               </div>
               <!-- End .header-search-wrapper -->
            </form>
         </div>
         <!-- End .header-search -->@guest
         <a href="{{route('wishlistview')}}" class="wishlist-link"> <i class="icon-heart-o">
         </i> 
         </a>
         <a href="{{route('addtocartview')}}" class="wishlist-link"> <i class="icon-shopping-cart"></i> 
         </a>@else @php $count = DB::table('wishlists')->where('user_id',Auth::user()->id)->count(); $cart = DB::table('carts')->where('user_id',Auth::user()->id)->count(); @endphp
         <a href="{{route('wishlistview')}}" class="wishlist-link"> <i class="icon-heart-o">
         </i> @if($count>0) <span style="font-size: 12px;
            color: white;
            background: red;
            padding: 3px 5px;
            border-radius: 50px;
            top: -9px;
            left: -9px;
            position: relative;">{{$count}}</span> @else <span style="font-size: 12px;
            color: red;"></span> @endif</a>
         <a href="{{route('addtocartview')}}" class="wishlist-link"> <i class="icon-shopping-cart"></i> @if($cart>0) <span style="font-size: 12px;
            color: white;
            background: red;
            padding: 3px 5px;
            border-radius: 50px;
            top: -9px;
            left: -9px;
            position: relative;">{{$cart}}</span> @else <span style="font-size: 12px;
            color: red;"></span> @endif</a>@endguest
         <div class="header-middle sticky-header">
            <!-- <div class="container-fluid"> -->
            <nav class="main-nav">
               <ul class="menu sf-arrows">
                  <li class="megamenu-container">
                     <a href="{{URL::To('my_account')}}" style="color: black;width: 177px;"> <i class="icon-user "></i> My Account</a>
                  </li>
               </ul>
            </nav>
            <!-- </div> -->
         </div>
         <!-- End .header-right -->
         <!-- End .page-wrapper -->
         <button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i>
         </button>
         <!-- Mobile Menu -->
         <div class="mobile-menu-overlay"></div>
         <!-- End .mobil-menu-overlay -->
         <div class="mobile-menu-container mobile-menu-light">
            <div class="mobile-menu-wrapper">
               <span class="mobile-menu-close"><i class="icon-close"></i></span>
               <form action="#" method="get" class="mobile-search">
                  <label for="mobile-search" class="sr-only">Search</label>
                  <input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Search in..." required="">
                  <button class="btn btn-primary" type="submit"><i class="icon-search"></i>
                  </button>
               </form>
               <nav class="mobile-nav">
                  <ul class="mobile-menu">
                     <li class="megamenu-container"> <a href="{{route('home')}}">Home</a> 
                     </li>
                     @php $categories=DB::table('categories')->get(); @endphp @foreach($categories as $cat)
                     <li>
                        <a href="{{route('store',$cat->id)}}" class="sf-with-ul" style="color: black;">{{$cat->category}}</a>
                        <div class="megamenu megamenu-md">
                           <div class="row no-gutters">
                              <div class="col-md-12">
                                 <div class="menu-col">
                                    <div class="row">
                                       @php $parent = DB::table('sub__categories')->where('category_id',$cat->id)->get(); @endphp @foreach($parent as $sub)
                                       <!-- parent -->
                                       <div class="col-md-4">
                                          <div class="menu-title"><a href="{{URL::To('sub_cat_store',$sub->id)}}">{{$sub->sub_category}}</a>
                                          </div>
                                          @php $child = DB::table('sub__sub__categories')->where('sub_category_id',$sub->id)->get(); @endphp @foreach($child as $sub_sub)
                                          <!-- child -->
                                          <ul>
                                             <li><a href="{{URL::To('child_store',$sub_sub->id)}}">{{$sub_sub->sub_sub_category}}</a>
                                             </li>
                                          </ul>
                                          @php $sub_sub_sub_category = DB::table('sub_sub_sub_category')->where('sub_sub_sub_id',$sub_sub->id)->get(); @endphp @foreach( $sub_sub_sub_category as $sub_3_category)
                                          <ul>
                                             <li>
                                                <a href=""></a>{{$sub_3_category->subsubsubcategory}}
                                             </li>
                                          </ul>
                                          @endforeach
                                          <!-- end child -->
                                          @endforeach
                                       </div>
                                       <!-- end parent -->
                                       @endforeach
                                    </div>
                                    <!-- End .row -->
                                 </div>
                                 <!-- End .menu-col -->
                              </div>
                           </div>
                           <!-- End .row -->
                        </div>
                     </li>
                     @endforeach
                  </ul>
               </nav>
               <!-- End .mobile-nav -->
               <div class="social-icons"> <a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>  <a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>  <a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>  <a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a> 
               </div>
               <!-- End .social-icons -->
            </div>
            <!-- End .mobile-menu-wrapper -->
         </div>
         <!-- End .mobile-menu-container -->
      </div>
      <!-- End .container-fluid -->
   </div>
   <!-- End .header-middle -->
</header>
<!-- End .header -->