@extends('layouts.app')
@section('page-css')
@stop
@section('content')
<main class="main" style="margin-top:100px;">
   <div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg')">
      <div class="container">
         <h1 class="page-title">My Account<span>Information</span></h1>
      </div>
      <!-- End .container -->
   </div>
   <!-- End .page-header -->
   <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
      <div class="container d-flex align-items-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">My Account</li>
         </ol>
      </div>
      <!-- End .container -->
   </nav>
   <!-- End .breadcrumb-nav -->
   <div class="page-content">
      <div class="dashboard">
         <div class="container">
            <div class="row">
               <aside class="col-md-4 col-lg-3">
                  <ul class="nav nav-dashboard flex-column mb-3 mb-md-0" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active" id="tab-dashboard-link" data-toggle="tab" href="#tab-dashboard" role="tab" aria-controls="tab-dashboard" aria-selected="true">Dashboard</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="tab-orders-link" data-toggle="tab" href="#tab-orders" role="tab" aria-controls="tab-orders" aria-selected="false">Orders</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="tab-address-link" data-toggle="tab" href="#tab-address" role="tab" aria-controls="tab-address" aria-selected="false">Address</a>
                     </li>
                     <li class="no"></li>
                     <li class="nav-item">
                        <a class="nav-link" href="{{URL::To('auth/logout')}}">Sign Out</a>
                     </li>
                  </ul>
               </aside>
               <!-- End .col-lg-3 -->
               <div class="col-md-8 col-lg-9">
                  <div class="tab-content">
                     <div class="tab-pane fade show active" id="tab-dashboard" role="tabpanel" aria-labelledby="tab-dashboard-link">
                        <p>Hello <span class="font-weight-normal text-dark">{{auth::user()->name}}</span> 
                           <br>
                           From your account dashboard you can view your <a href="#tab-orders" class="tab-trigger-link link-underline">recent orders</a>, <br>
                           manage your <a href="#tab-address" class="tab-trigger-link">shipping and billing addresses</a>,<br> and <a href="#tab-account" class="tab-trigger-link">edit your password and account details</a>.
                        </p>
                        <br>
                        (not <span class="font-weight-normal text-dark">{{auth::user()->name}}</span>? <a href="{{URL::To('auth/logout')}}">Log out</a>)
                     </div>
                     <!-- .End .tab-pane -->
                     <div class="tab-pane fade" id="tab-orders" role="tabpanel" aria-labelledby="tab-orders-link">
                        <div class="row">
                           <table class="table table-bordered table-striped" style="    text-align-last: center;">
                              <thead>
                                 <tr>
                                    <th>Sno.</th>
                                    <th>Order Id</th>
                                    <th>Order Date</th>
                                    <th>Transaction_id</th>
                                    <th>Status</th>
                                    <th>Total Amount</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @php $i=1; @endphp
                                 @foreach ($data as $datas)
                                 <tr>
                                    <td>
                                       {{$i++}}
                                    </td>
                                    <td>
                                       LT{{$datas->id}}
                                    </td>
                                    <td>
                                       {{$datas->created_at}}
                                    </td>
                                    <td>
                                       {{$datas->transaction_id}}
                                    </td>
                                    <td>
                                       @if($datas->status == 1)
                                       Delivered
                                       @elseif($datas->status == 0)
                                       Processing
                                       @elseif($datas->status == -2)
                                       Reject
                                       @else
                                       New
                                       @endif  
                                    </td>
                                    <td>
                                       {{$datas->total_price}}
                                    </td>
                                    <td>
                                       <a href="{{URL::To('bill',$datas->id)}}" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>
                                       Print</a>
                                       <a href="{{URL::To('my_account',$datas->id)}}" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal{{$datas->id}}"><i class="fa fa-download" aria-hidden="true"></i>
                                       View Carts</a>
                                                      <!-- Modal -->
                           <div class="modal fade" id="myModal{{$datas->id}}" role="dialog">
                              <div class="modal-dialog">
                                 <!-- Modal content-->
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title">Order Details</h4>
                                    </div>
                                    <div class=" text-left">
                                       <table class="col-md-12">
                                          <thead>
                                             <tr class="text-center">
                                                <th>Sr</th>
                                                <th>Product</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @php $orders = DB::table('checkout_carts')->where('checkout_id',$datas->id)
                                             ->join('products','checkout_carts.product_id','products.id')
                                             ->select('checkout_carts.*','products.title','products.main_image','products.id as product_id')
                                             ->get();
                                             @endphp
                                             @foreach( $orders as $order)
                                             <tr>
                                                <td class="text-center">{{$order->id}}</td>
                                                <td class="text-center">{{$order->title}}</td>
                                                <td class="text-center"> 
                                                   @php $count = DB::table('downloads')->where('checkout_carts_id',$order->id)->where('product_id',$order->product_id)->count();
                                                   @endphp
                                                   @if($count >= 3)
                                                   @else
                                                   <form action="{{Config::get('app.admin_url')}}/file_download/{{$order->id}}" method="get">
                                                      <button type="submit" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true" cl></i>Download</button>
                                                   </form>
                                                   @endif
                                                </td>
                                             </tr>
                                             @endforeach
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                    </td>
                                 </tr>

                                 @endforeach  
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <!-- .End .tab-pane -->
                     <div class="tab-pane fade" id="tab-address" role="tabpanel" aria-labelledby="tab-address-link">
                        <p>The addresses will be used on the checkout page by default.</p>
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="card card-dashboard">
                                 <div class="card-body">
                                    <h3 class="card-title">Billing Address</h3>
                                    <!-- End .card-title -->
                                    <form action="{{URL::To('checkoutstore')}}" method="post">
                                       @csrf
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>First Name *</label>
                                             <input type="text" value="{{$address->name}}" class="form-control" name="fname" required="" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                          <div class="col-sm-6">
                                             <label>Last Name *</label>
                                             <input type="text" class="form-control" name="lname" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                       </div>
                                       <!-- End .row -->
                                       <label>Country *</label>
                                       <input type="text" value="{{$address->country}}" class="form-control" name="country" required="" />
                                       <label>Street address *</label>
                                       <input type="text" value="{{$address->address1}}" class="form-control" placeholder="House number and Street name" required="" name="address1" />
                                       <input type="text" value="{{$address->address2}}" class="form-control" placeholder="Appartments, suite, unit etc ..." required="" name="address2" />
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Town / City *</label>
                                             <input type="text" value="{{$address->city}}" class="form-control" name="city" required="" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                          <div class="col-sm-6">
                                             <label>State*</label>
                                             <input type="text" value="{{$address->state}}" class="form-control" name="state" required="" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                       </div>
                                       <!-- End .row -->
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Postcode / ZIP *</label>
                                             <input type="text" value="{{$address->pincode}}" class="form-control" name="postcode" required="" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                          <div class="col-sm-6">
                                             <label>Mobile *</label>
                                             <input type="tel" value="{{$address->mobile}}" name="mobile" class="form-control" required="" />
                                          </div>
                                          <!-- End .col-sm-6 -->
                                       </div>
                                       <!-- End .row -->
                                       <label>Email address *</label>
                                       <input type="email" value="{{$address->email}}" class="form-control" name="email" required="" />
                                       <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="checkout-diff-address" />
                                       </div>
                                       <button class="btn btn-success" style="margin-left: 640px;">Save Address</button>
                                    </form>
                                 </div>
                                 <!-- End .card-body -->
                              </div>
                              <!-- End .card-dashboard -->
                           </div>
                           <!-- End .col-lg-6 -->
                        </div>
                        <!-- End .row -->
                     </div>
                     <!-- .End .tab-pane -->
                  </div>
               </div>
               <!-- End .col-lg-9 -->
            </div>
            <!-- End .row -->
         </div>
         <!-- End .container -->
      </div>
      <!-- End .dashboard -->
   </div>
   <!-- End .page-content -->
</main>
@endsection