@extends('layouts.app')
@section('content')
    <div class="page-wrapper" style="margin-top:100px;">
         <main class="main">
        	<div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg')">
        		<div class="container">
        			<h1 class="page-title">Wishlist<span>Shop</span></h1>
        		</div><!-- End .container -->
        	</div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item">Store</li>
                        <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
            	<div class="container">
					<table class="table table-wishlist table-mobile">
						<thead>
							<tr>
								<th>Product</th>
								<th>Stock Status</th>
								<th></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							@foreach($wish as $product)
							<tr>
								<td class="product-col">
									<div class="product">
										<figure class="product-media">
											<a href="{{ route('single_product',$product->id) }}">
												<img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}">
											</a>
										</figure>

										<h3 class="product-title">
											<a href="{{ route('single_product',$product->id) }}">{{$product->title}}</a>
										</h3><!-- End .product-title -->
									</div><!-- End .product -->
								</td>
								<td class="action-col">
                                    <div class="product-details-action">     
                                        <div class="details-action-col">
                                        	<a href="{{ route('single_product',$product->id) }}">
                               <button type="submit" class="btn-product btn-cart" style="border: -1px;height: 40px;width: 160px;margin-left: -350px;margin-top: 57px;" >View Details</button></a>
                                        </div>
                                    </div>
								</td>
								<td class="remove-col"><a href="{{URL::To('deletewishlist',$product->wishlists_id)}}"><button class="btn-remove"><i class="icon-close" style="margin-top: 12px;margin-left: -442px;"></i>
								</button></a></td>
							</tr>
							@endforeach
						</tbody>
					</table><!-- End .table table-wishlist -->
	            	<div class="wishlist-share">
	            		<div class="social-icons social-icons-sm mb-2">
	            			<label class="social-label">Share on:</label>
	    					<a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
	    					<a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
	    					<a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
	    					<a href="#" class="social-icon" title="Youtube" target="_blank"><i class="icon-youtube"></i></a>
	    					<a href="#" class="social-icon" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
	    				</div><!-- End .soial-icons -->
	            	</div><!-- End .wishlist-share -->
            	</div><!-- End .container -->
            </div><!-- End .page-content -->
        </main><!-- End .main -->

        
    </div><!-- End .page-wrapper -->
    
@endsection
