@extends('layouts.app')
@section('content')

        <main class="main">
            <div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg');margin-top:120px;">
                    <div class="container">
                        <span>Luxary Store</span>
                </div>
            </div> 
            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item">Store</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div class="container">
                    <!-- <div class="toolbox">
                        <div class="toolbox-left">
                            <a href="#" class="sidebar-toggler"><i class="icon-bars"></i>Filters</a>
                        </div>

                        <div class="toolbox-center">
                            <div class="toolbox-info">
                                 <span></span> 
                            </div>
                        </div>

                        <div class="toolbox-right">
                            <div class="toolbox-sort">
                                <label for="sortby">Sort by:</label>
                                <div class="select-custom">
                                    <select name="sortby" id="sortby" class="form-control">
                                        <option value="popularity" selected="selected">Most Popular</option>
                                        <option value="rating">Most Rated</option>
                                        <option value="date">Date</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="products">
                        <div class="row">
                        @foreach($products as $product)
                        

                            <div class="col-6 col-md-4 col-lg-4 col-xl-3">
                                <div class="product product-2">
                                    <figure class="product-media">
                                        <a href="{{ URL::to('single_product',$product->id) }}">
                                        <img src="{{Config::get('app.admin_url')}}/img/product/{{$product->main_image}}" class="product-image">
                                        </a>

                                        <div class="product-action-vertical">
                                            <a href="{{route('wishlist',$product->id)}}" class="btn-product-icon btn-wishlist btn-expandable"><span>add to wishlist</span></a>
                                        </div><!-- End .product-action -->

                                    <div class="product-action product-action-transparent">
                                       <div class="product-action product-action-transparent">
                                        <a href="{{ URL::to('single_product',$product->id) }}" class="btn-product btn-cart"><span>View Details</span></a>
                                        </div>
                                    </div>
                                    </figure><!-- End .product-media -->

                                    <div class="product-body">
                                        <h3 class="product-title"><a href="product.html">{{$product->title}}</a></h3>
                                        <span><strong>Rs. {{$product->amount}}</strong></span>
                                        <!-- End .product-title -->
                                     
                                        <div class="ratings-container">
                                           <!--  <div class="ratings">
                                                <div class="ratings-val" style="width: 0%;"></div>
                                            </div>
                                            <span class="ratings-text">( 0 Reviews )</span> -->
                                        </div><!-- End .rating-container -->

                                    </div><!-- End .product-body -->
                                </div><!-- End .product -->
                            </div><!-- End .col-sm-6 col-lg-4 col-xl-3 -->
                            @endforeach 
                        </div><!-- End .row -->

                        <div class="load-more-container text-center">
                        </div><!-- End .load-more-container -->
                    </div><!-- End .products -->

                    <div class="sidebar-filter-overlay"></div><!-- End .sidebar-filter-overlay -->
                    
                </div><!-- End .container -->
            </div><!-- End .page-content -->
        </main><!-- End .main -->
@endsection