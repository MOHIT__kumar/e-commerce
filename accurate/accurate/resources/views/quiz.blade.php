@extends('layouts.app')
@section('content')
  <div class="page-container">
         <main class="main">
            <div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg');margin-top: 100px;">
               <div class="container">
                  <h1 class="page-title">Quiz </h1>
               </div>
               <!-- End .container -->
            </div>
            <!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
               <div class="container">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="http://localhost:8000">Home</a></li>
                     <li class="breadcrumb-item"><a href="#">Quiz</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Questions</li>
                  </ol>
               </div>
               <!-- End .container -->
            </nav>
            <!-- End .breadcrumb-nav -->
            <div class="page-content">
               <div class="cart">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-9">
                            <form method="post" action="{{URL::to('question/store')}}">
                                @csrf
                                @php $i = 1; @endphp
                            @foreach( $questions as $question)
                             <h4>{{$i++}}. {{$question->question}}</h4>
                             <input type="hidden" name="question_id" id="question_id" value="{{$question->id}}">
                             <input type="hidden" name="page_on" value="{{$questions->currentPage() }}">
                              &nbsp;<strong>A.</strong>&nbsp;  &nbsp;<input type="radio" id="opt_1" name="answer" value="opt_1"> {{$question->option1}}
                              &nbsp;<strong>B.</strong>&nbsp;  &nbsp;<input type="radio" id="opt_2" name="answer" value="opt_2"> {{$question->option2}}
                              &nbsp;<strong>C.</strong>&nbsp;  &nbsp;<input type="radio" id="opt_3" name="answer" value="opt_3"> {{$question->option3}}
                              &nbsp;<strong>D.</strong>&nbsp;  &nbsp;<input type="radio" id="opt_4" name="answer" value="opt_4"> {{$question->option4}}<br><br><br>
                              @endforeach
                              {{$questions->links()}}
                              <button type="submit" class="btn btn-info">Submit</button>
                               @foreach ($questions as $app)
                                  @if($app->id == $questions->lastPage())
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="result">
                                        Show Result
                                     </button>
                                     <!-- The Modal -->
                                          <div class="modal" id="myModal">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                              
                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                  <h4 class="modal-title">Result</h4>
                                                </div>
                                                
                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                  <ul>
                                                      <li>Total Questions</li>
                                                        <li id="total"></li>
                                                      <li>Correct Answer</li>
                                                          <li id="right"></li>
                                                      <li>Wrong Answers</li>
                                                          <li id="wrong"></li>
                                                  </ul>
                                                </div>
                                                
                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                                
                                              </div>
                                            </div>
                                          </div>
                                   @endif
                                @endforeach
                            </form>
                        </div>
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End .cart -->
            </div>
            <!-- End .page-content -->
         </main>
      </div>

@endsection

@section('page-js')
  <script>
     $(document).ready(function(){
        $('#result').click(function(){
            $.ajax({
                url:"{{URL::to('result')}}",
                type:'GET',
                success: function (result){
                   $('#total').text(result['total_question']);
                   $('#right').text(result['right_answer']);
                   $('#wrong').text(result['wrong_answer']);
                }
            })
        });

     });
  </script>
 @stop