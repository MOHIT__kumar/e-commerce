 @include('layouts.css')

<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container" style="margin-top:100px;">
    <div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg')">
                <div class="container">
                    <h1 class="page-title">Invoice</h1>
                </div><!-- End .container -->
            </div><!-- End .page-header -->
    <div class="row">
        <div class="col-md-12">
    		<div class="invoice-title">
    			<h3 class=""># Order {{$total->transaction_id}}</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-md-6">
    				<address>
                    <strong>Billed To:</strong><br>
                        {{$total->name}}<br>
                        {{substr($total->address1,0,35)}}<br>
                        {{substr($total->address1,35)}}<br>
                        {{$total->address2}}<br>
                        {{$total->city}}<br>
                        {{$total->state}}, {{$total->pincode}}
                    </address>
    			</div>
    			<div class="col-md-6 text-right">
    				<address>
                    <strong>Billed To:</strong><br>
                        {{$total->name}}<br>
                        {{substr($total->address1,0,35)}}<br>
                        {{substr($total->address1,35)}}<br>
                        {{$total->address2}}<br>
                        {{$total->city}}<br>
                        {{$total->state}}, {{$total->pincode}}
                    </address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Payment Method:</strong><br>
    					 @if($total->transaction_id)
                             Online Payment
                            @else
                          Cash On Delivery
                              @endif
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					{{$total->created_at}}<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
                                    <td><strong>Sr.no.</strong></td>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
                                  @php $i=1; $subtotal=0;@endphp
                                  @foreach ($cart as $data)
    							<tr>
                                    <td>{{$i++}}</td>
    								<td>{{$data->title}}</td>
    								<td class="text-center">{{$data->amount}}</td>
    								<td class="text-center">{{$data->qty}}</td>
    								<td class="text-right">{{$data->amount * $data->qty}}</td>
    							</tr>
                                @endforeach
                                
    							<tr >
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
                                    <td class="thick-line"></td>
    								<td class="thick-line text-center"><strong><br>Subtotal</strong></td>
    								<td class="thick-line text-right"><br>{{$total->total_price}}</td>
    							</tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="no-line text-center"><strong>Discount Amount</strong></td>
                                    <td class="no-line text-right">{{$total->discount_amount}}</td>
                                </tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
                                    <td class="thick-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">{{$total->total_price}}</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
@include('layouts.js')