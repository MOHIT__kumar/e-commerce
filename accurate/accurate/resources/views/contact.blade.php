@extends('layouts.app')
@section('content')
    <main class="main" style="margin-top:100px;">
            <div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg')">
                <div class="container">
                    <h1 class="page-title">Contact us <span>Pages</span></h1>
                </div><!-- End .container -->
            </div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav border-0 mb-0">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact us </li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div id="map" class="mb-5"></div><!-- End #map -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="contact-box text-center">
                                <h3>Office</h3>

                                <address>1 New York Plaza, New York, <br>NY 10004, USA</address>
                            </div><!-- End .contact-box -->
                        </div><!-- End .col-md-4 -->

                        <div class="col-md-4">
                            <div class="contact-box text-center">
                                <h3>Start a Conversation</h3>

                                <div><a href="mailto:#">info@Molla.com</a></div>
                                <div><a href="tel:#">+1 987-876-6543</a>, <a href="tel:#">+1 987-976-1234</a></div>
                            </div><!-- End .contact-box -->
                        </div><!-- End .col-md-4 -->

                        <div class="col-md-4">
                            <div class="contact-box text-center">
                                <h3>Social</h3>

                                <div class="social-icons social-icons-color justify-content-center">
                                    <a href="#" class="social-icon social-facebook" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                                    <a href="#" class="social-icon social-twitter" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                    <a href="#" class="social-icon social-instagram" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                    <a href="#" class="social-icon social-youtube" title="Youtube" target="_blank"><i class="icon-youtube"></i></a>
                                    <a href="#" class="social-icon social-pinterest" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
                                </div><!-- End .soial-icons -->
                            </div><!-- End .contact-box -->
                        </div><!-- End .col-md-4 -->
                    </div><!-- End .row -->

                    <hr class="mt-3 mb-5 mt-md-1">  
                </div><!-- End .container -->
            </div><!-- End .page-content -->
        </main><!-- End .main -->

@endsection
