@extends('layouts.app')
@section('content')
        <main class="main">
        	<div class="page-header text-center" style="background-image: url('images/page-header-bg.jpg'); margin-top: 100px;">
        		<div class="container">
        			<h1 class="page-title">Checkout<span>Shop</span></h1>
        		</div><!-- End .container -->
        	</div><!-- End .page-header -->
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item">Store</li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
        <div class="checkout">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <form action="{{URL::To('checkoutstore')}}" method="post">
                        @csrf
                        <h2 class="checkout-title">Billing Details</h2>
                        <!-- End .checkout-title -->
                        <div class="row">
                            <div class="col-sm-6">
                                <label>First Name *</label>
                                <input type="text" value="{{$address->name}}" class="form-control" name="fname" required="" />
                            </div>
                            <!-- End .col-sm-6 -->

                            <div class="col-sm-6">
                                <label>Last Name *</label>
                                <input type="text" class="form-control" name="lname" />
                            </div>
                            <!-- End .col-sm-6 -->
                        </div>
                        <!-- End .row -->

                        <label>Country *</label>
                        <input type="text" value="{{$address->country}}" class="form-control" name="country" required="" />

                        <label>Street address *</label>
                        <input type="text" value="{{$address->address1}}" class="form-control" placeholder="House number and Street name" required="" name="address1" />
                        <input type="text" value="{{$address->address2}}" class="form-control" placeholder="Appartments, Colony, unit etc ..." required="" name="address2" />

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Town / City *</label>
                                <input type="text" value="{{$address->city}}" class="form-control" name="city" required="" />
                            </div>
                            <!-- End .col-sm-6 -->

                            <div class="col-sm-6">
                                <label>State*</label>
                                <input type="text" value="{{$address->state}}" class="form-control" name="state" required="" />
                            </div>
                            <!-- End .col-sm-6 -->
                        </div>
                        <!-- End .row -->

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Postcode / ZIP *</label>
                                <input type="text" value="{{$address->pincode}}" class="form-control" name="postcode" required="" />
                            </div>
                            <!-- End .col-sm-6 -->

                            <div class="col-sm-6">
                                <label>Mobile *</label>
                                <input type="tel" value="{{$address->mobile}}" name="mobile" class="form-control" required="" />
                            </div>
                            <!-- End .col-sm-6 -->
                        </div>
                        <!-- End .row -->

                        <label>Email address *</label>
                        <input type="email" value="{{$address->email}}" class="form-control" name="email" required="" />

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="checkout-diff-address" />
                        </div>
                        <button class="btn btn-success">Save Address</button>
                    </form>
                    
                </div>

                <div class="col-lg-3">
                    <div class="summary">
                        <h3 class="summary-title">Your Order</h3>
                        <div class="checkout-discount">
                            <form action="{{route('checkoutview')}}">
                                <input type="text" class="form-control" required="" id="checkout-discount-input" name="Voucher" placeholder="Enter Voucher Code">
                                <p>
                                     @if(isset($_GET['Voucher']))
                                        <a href="{{URL::to('checkoutview')}}">You applied this coupan  {{$_GET['Voucher']}}</a>
                                    @endif
                                </p>
                                <br>
                                    <button class="btn btn-outline-primary-2 btn-order btn-block">Submit Coupon</button>
                            </form>
                            <hr>
                        </div>
                        <!-- End .summary-title -->
                            <table class="table table-summary">
                            <tbody>
                                <tr class="summary-subtotal">
                                    <td>Subtotal:</td>
                                    <td>{{$final_amount}}</td>
                                </tr>
                                <!-- End .summary-subtotal -->
                                <tr class="summary-subtotal">
                                    <td>Discount:</td>
                                    <td>{{$discount_amount}}</td>
                                </tr>

                                 <tr>
                                    <td>Total:</td>
                                    <td>
                                        {{($final_amount-$discount_amount)}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        

                    <form action="{{URL::To('razorpay')}}" method="post">
                        @csrf
                        <div class="accordion-summary" id="accordion-payment">
                            <div class="card">
                                <div class="card-header" id="heading-5">
                                    <h2 class="card-title">
                                        <input type="radio" name="payment_method" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" id="online" aria-controls="collapse-5" onclick="click()" value="1">
                                            Online Payments
                                    </h2><br>
                                            <img src="{{URL::To('images\payments-summary.png')}}" alt="payments cards" />      
                                        <br>     
                                    <h2 class="card-title">
                                        <input type="radio" name="payment_method" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" id="cod" aria-controls="collapse-3" onclick="click()" value="0">
                                            Cash on delivery   
                                    </h2>
                                </div>
                            </div>    
                        </div>
                        <input type="hidden" name="product_amount" value="{{($final_amount-$discount_amount)}}">
                        <input type="hidden" name="discount_amount" value="{{$discount_amount}}">
                        <button class="btn btn-outline-primary-2 btn-order btn-block">PROCEED TO PAYMENT</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End .container -->
    </div>
</div>

        </main>

@endsection
@section('checkoutpage-js')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<button id="rzp-button1" hidden="">Pay</button>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
@if($response ?? "")
<script>
    var options = {
        "key": "{{$response['razorpayId']}}", // Enter the Key ID generated from the Dashboard
        "amount": "{{$response['amount']}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "{{$response['currency']}}",
        "name": "{{$response['name']}}",
        "description": "{{$response['massage']}}",
        "image": "{{URL::TO('img/logo1.png')}}",
        "order_id": "{{$response['orderId']}}", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
        "handler": function (response){

            $.ajax({
                url: "{{URL::TO('/payment')}}",
                type: 'post',
                dataType: 'json',
                data: { 
                        "_token": "{{ csrf_token() }}",
                        rzp_payment_id      : response.razorpay_payment_id, 
                        rzp_order_id        : response.razorpay_order_id,
                        rzp_signature       : response.razorpay_signature,
                        name                : "{{$response['name']}}",
                        email               : "{{$response['email']}}",
                        mobile              : "{{$response['mobile']}}",
                        amount              : "{{$response['amount']}}",
                        address             : "{{$response['address']}}",
                        massage             : "{{$response['massage']}}",
                        discount_amount     : "{{$response['discount_amount']}}",
                        total_amount        : "{{$response['total_amount']}}",
                    }, 

                success: function (msg) {
                    // sweet alert
                    swal({
                        title: "Thanks For Orders!",
                        text: "Message!",
                        type: "success"
                    }).then(function() {
                        window.location = "{{URL::to('/')}}";
                    });
                }
            });


        },
        "prefill": {
            "name": "{{$response['name']}}",
            "email": "{{$response['email']}}",
            "contact": "{{$response['mobile']}}"
        },
        "notes": {
            "address": "{{$response['address']}}"
        },
        "theme": {
            "color": "#F37254"
        }
    };
    var rzp1 = new Razorpay(options);
    window.onload = function(){
        document.getElementById('rzp-button1').click();
    }
    document.getElementById('rzp-button1').onclick = function(e){
        rzp1.open();
        e.preventDefault();
    }
</script>
@endif

@endsection

