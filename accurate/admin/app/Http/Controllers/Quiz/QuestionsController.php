<?php

namespace App\Http\Controllers\Quiz;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Quiz\quizcategory;
use App\Model\Quiz\questions;
use Session;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = quizcategory::where('status',1)->get();
        $questions  = questions::all();
        return view('Admin.Quiz.question.index',compact('categories','questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id'  => 'required',
            'question'     => 'required|unique:questions',
            'opt_1'        => 'required',
            'opt_2'        => 'required',
            'opt_3'        => 'required',
            'opt_4'        => 'required',
            'answer'       => 'required',
        ]);

        questions::create([
            'category_id'   => $request->category_id,
            'question'      => $request->question,
            'option1'       => $request->opt_1,
            'option2'       => $request->opt_2,
            'option3'       => $request->opt_3,
            'option4'       => $request->opt_4,
            'answer'        => $request->answer,
            ]);

        Session::flash('success','Question created successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = questions::where('id',$id)->first();
         if ($question->status == 1) 
         {
             $question->status = 0;
             $question->save();
             Session::flash('error','Question deactivated successfully');
         }
         else{
            $question->status = 1;
            $question->save();
            Session::flash('success','Question activated successfully');
         }
         return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id'   => 'required',
            'question'      => 'required|unique:questions',
            'answer'        => 'required',
            'opt_1'         => 'required',
            'opt_2'         => 'required',
            'opt_3'         => 'required',
            'opt_4'         => 'required'
        ]);

        questions::where('id',$id)->update([
            'category_id'    => $request->category_id,
            'question'       => $request->question,
            'answer'         => $request->answer,
            'option1'        => $request->opt_1,
            'option2'        => $request->opt_2,
            'option3'        => $request->opt_3,
            'option4'        => $request->opt_4
        ]);

        Session::flash('success','Question updated successfully');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
