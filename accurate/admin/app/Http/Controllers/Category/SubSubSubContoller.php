<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Category;
use App\Model\Category\Sub_Category;
use App\Model\Category\Sub_Sub_Category;
use App\Model\Category\SubSubSubCateogory;
use DB;
use Auth;
use Session;    
use Carbon\Carbon;


class SubSubSubContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = DB::table('sub_sub_sub_category')
                    ->leftjoin('sub__categories', 'sub__categories.id', '=', 'sub_sub_sub_category.sub_category_id')
                    ->leftjoin('sub__sub__categories', 'sub__sub__categories.id', '=', 'sub_sub_sub_category.sub_sub_sub_id')
                    ->join('categories', 'categories.id', '=', 'sub_sub_sub_category.category_id')
                    ->select('sub_sub_sub_category.*', 'sub__categories.sub_category', 'categories.category','sub__sub__categories.sub_sub_category')
                    ->get();

        $category = Category::all();

        return view('Admin.Category.sub_sub_sub',compact('data', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'category_id'      => 'required',
            'sub_category_id'  => 'required',
            'sub_sub_sub_id'   => 'required',
            'subsubsubcategory'=> 'required',
        ]);

        SubSubSubCateogory::create([
            'category_id'       => $request->category_id,
            'sub_category_id'   => $request->sub_category_id,
            'sub_sub_sub_id'    => $request->sub_sub_sub_id,
            'subsubsubcategory' => $request->subsubsubcategory,
        ]);   

        Session::flash('success','Data Insert Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           /*ajax response*/
        $subcategory =DB::table('sub_sub_sub_category')
                            ->where('sub_sub_sub_id',$id)
                            ->pluck('subsubsubcategory','id');
        return response()->json($subcategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         /*ajax response*/
        $subcategory =DB::table('sub__sub__categories')
                            ->where('sub_category_id',$id)
                            ->pluck('sub_sub_category','id');
        return response()->json($subcategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
