<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use App\Model\Inventory\Productsize;
use App\Model\Inventory\Product;
use DB;
use Response;
use Carbon\Carbon;
use Session;


class WebsiteController extends Controller
{
    /*Index Page*/

    public function index($id)
    {
       
         $data = DB::table('checkout_carts')->where('checkout_carts.id',$id)
                                            ->join('products','checkout_carts.product_id','products.id')
                                            ->join('product_file','products.id','product_file.product_id')
                                            ->select('checkout_carts.*','products.title','products.id as product_id','product_file.files')
                                            ->first();
          
         $respnse = DB::table('downloads')->insert([
            'checkout_carts_id'  => $data->id,
            'product_id'         => $data->product_id,
            'product_name'       => $data->title,
         ]);

          if($respnse) 
          {
            $count = DB::table('downloads')->where('checkout_carts_id',$data->id)->where('product_id',$data->product_id)->count();

            if ($count >= 3) {
              Session::flash('Data not found');
              return back();
            }
            else
            {
              $file= public_path(). "/img/product/$data->files";
              $headers = array(

                  'Content-Type: application/pdf',

                );

              return Response::download($file,$data->title, $headers);
            }
          }
          else
          {
              Session::flash('Data not found');
              return back();
          }         
    
    }

}
