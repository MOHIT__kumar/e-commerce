<?php

namespace App\Model\Quiz;

use Illuminate\Database\Eloquent\Model;

class quizcategory extends Model
{
    protected $fillable = ['category','status'];

    protected $table = 'quizcategories';

    public $primaryKey = 'id';
}
