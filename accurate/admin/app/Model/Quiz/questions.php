<?php

namespace App\Model\Quiz;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
    protected $fillable = ['category_id','question','answer','option1','option2','option3','option4','status'];

    protected $table = 'questions';

    public $primaryKey = 'id';
}
