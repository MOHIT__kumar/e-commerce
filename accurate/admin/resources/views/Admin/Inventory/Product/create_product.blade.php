<title>Product List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Product List
   </h1>
   <ol class="breadcrumb">
      <a href="{{route('product.index')}}" class="btn btn-primary">View Product </a>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
   <div class="col-xs-12">
      <div class="box box-primary">
         <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Create Product</a></li>
         </ul>
         <!-- /.box-header -->
         <!-- form start -->
         <form role="form" action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="box-body">
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="exampleInputPassword1">Category</label>
                     <select class="form-control" id="category" name="category_id" onchange="categorys(this)">
                        <option>Select Category</option>
                        @foreach($category as $key)
                        <option value="{{$key->id}}">{{$key->category}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label>Choose Sub Category </label>
                     <select class="form-control" id="sub_service" name="subcategory_id" required onchange="subcategoryss(this)">
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label>Choose Sub Sub Category </label>
                     <select class="form-control" id="subsubcategory" name="subsubcategory_id" required onchange="get_sub_sub_category(this)">
                     </select>
                  </div>
               </div>

                <div class="col-md-4">
                  <div class="form-group">
                     <label>Choose Sub Sub Sub Category </label>
                     <select class="form-control" id="subsubcategorys" name="sub_sub_sub_category_id" required>
                     </select>
                  </div>
               </div>
               
              <div class="col-md-4">
                  <div class="form-group">
                     <label>Meta Title</label>
                     <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" >
                  </div>
               </div>


               <div class="col-md-4">
                  <div class="form-group">
                     <label>Meta Keywords</label>
                     <input type="text" class="form-control" name="meta_keywords" placeholder="Enter Meta Keywords" >
                  </div>
               </div>


                <div class="col-md-6">
                  <div class="form-group">
                     <label>Product Name</label>
                     <input type="text" class="form-control" name="title" placeholder="Enter Product Name" required="required" value="">
                  </div>
               </div>

                <div class="col-md-6">
                  <div class="form-group">
                     <label>Product Amount</label>
                     <input type="number" class="form-control" name="amount" placeholder="Enter Product Amount" required="required">
                  </div>
               </div>

               <div class="col-md-12">
                  <div class="form-group">
                     <label for="sponser_id">Description</label>
                     <textarea class="textarea t" id="textarea" required placeholder="Place some text here" name="description" style="width:100%"></textarea>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <label for="sponser_id">Featured Image</label>
                      <input type="file" name="image" class="form-control" required>
                  </div>
               </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </div>
   </div>
   <!-- /.row -->
</section>
@endsection
<script type="text/javascript">
   function categorys(element){
    var category = $(element).val();    
    if(category){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_category')}}/"+category+'/edit',
           success:function(res){               
            if(res){
                $("#sub_service").empty();
                $("#sub_service").append('<option>Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#sub_service").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">
   function subcategoryss(element){
    var subcategorys = $(element).val();    
    if(subcategorys){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_sub_category')}}/"+subcategorys+'/edit',
           success:function(res){               
            if(res){
                $("#subsubcategory").empty();
                $("#subsubcategory").append('<option value="">Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#subsubcategory").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>

<script type="text/javascript">
   function get_sub_sub_category(element){
    var subcategorys = $(element).val();    
    if(subcategorys){
        $.ajax({
           type:"GET",
           url:"{{URL::to('sub_sub_sub_category')}}/"+subcategorys,
           success:function(res){               
            if(res){
                $("#subsubcategorys").empty();
                $("#subsubcategorys").append('<option value="">Select Sub Category</option>');
                $.each(res,function(key,value){
                    $("#subsubcategorys").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }
           }
        });
    }     
    };
</script>