@extends('layouts.admin')
@section('title', 'Banner')
@section('content')

<section class="content-header">
  <h1>
   View Questions
  </h1>
  <ol class="breadcrumb">
    <button class="btn btn-primary" data-toggle="modal" data-target="#text"><i class="glyphicon glyphicon-plus"></i> </button>
    <div class="modal fade" id="text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">ADD Questions</h4>
        </div> 
          <div class="modal-body">
            <form role="form" action="{{route('question.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('POST')

              <label>Category:</label>
              <select class="form-control" name="category_id" required="required">
                <option value="">Select Category</option>
                 @foreach( $categories as $cat)
                  <option value="{{$cat->id}}">{{$cat->category}}</option>
                 @endforeach
              </select>

              <label>Question</label>
              <input type="text" name="question" class="form-control">


              <label>Option 1</label>
              <input type="text" name="opt_1" class="form-control">

              <label>Option 2</label>
              <input type="text" name="opt_2" class="form-control">

              <label>Option 3</label>
              <input type="text" name="opt_3" class="form-control">

              <label>Option 4</label>
              <input type="text" name="opt_4" class="form-control">

              <label>Answer</label>
              <select class="form-control" name="answer">
                <option value="opt_1">Option 1</option>
                <option value="opt_2">Option 2</option>
                <option value="opt_3">Option 3</option>
                <option value="opt_4">Option 4</option>
              </select>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
              </div>
            </form>
          </div>
      </div>
    </div>
  </ol>
</section>
<br>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-xs-12">
      <div class="box" style="overflow-y: scroll;">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sno.</th>
                <th>Category</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Option 1</th>
                <th>Option 2</th>
                <th>Option 3</th>
                <th>Option 4</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($questions as $question)

              <tr>
                <td>{{$i++}}</td>
                <td>{{$question->category_id}}</td>
                <td>{{$question->question}}</td>
                <td>{{$question->answer}}</td>
                <td>{{$question->option1}}</td>
                <td>{{$question->option2}}</td>
                <td>{{$question->option3}}</td>
                <td>{{$question->option4}}</td>
                <td>
                    @php
                      if($question->status == 1)
                      {
                        echo 'Active';
                      }

                      else
                      {

                        echo 'Inactive';
                      }
                    @endphp
                </td>
                <td>{{$question->created_at}}</td>
                <td>{{$question->updated_at}}</td>
                <td>

                  <!-- Edit Button -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#{{$question->id}}"><i class="glyphicon glyphicon-edit"></i> </button>

                <a href="{{route('question.edit',$question->id)}}" class="btn btn-danger"><i class="glyphicon glyphicon-off"></i></a>
                 <div class="modal fade" id="{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                   <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Edit Quiz Category</h4>
                    </div>
                    <div class="modal-body">
                      <form role="form" action="{{route('question.update',$question->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                       <label>Category:</label>
                          <select class="form-control" name="category_id" required="required">
                            <option value="">Select Category</option>
                             @foreach( $categories as $cat)
                              <option value="{{$cat->id}}">{{$cat->category}}</option>
                             @endforeach
                          </select>

                          <label>Question</label>
                          <input type="text" name="question" value="{{$question->question}}" class="form-control">


                          <label>Option 1</label>
                          <input type="text" name="opt_1" value="{{$question->option1}}" class="form-control">

                          <label>Option 2</label>
                          <input type="text" name="opt_2"  value="{{$question->option2}}" class="form-control">

                          <label>Option 3</label>
                          <input type="text" name="opt_3" value="{{$question->option3}}" class="form-control">

                          <label>Option 4</label>
                          <input type="text" name="opt_4" value="{{$question->option4}}" class="form-control">

                          <label>Answer</label>
                          <select class="form-control" name="answer">
                             @if($question->opt_1 == 'opt_1')
                              <option value="" selected="">{{$question->answer}}</option>
                               @else
                                <option value="opt_1">Option 1</option>
                                <option value="opt_2">Option 2</option>
                                <option value="opt_3">Option 3</option>
                                <option value="opt_4">Option 4</option>
                                 @endif
                          </select>

                        <label></label>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" value="submit">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            @endforeach  

          </tbody>

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

@endsection