@extends('layouts.admin')
@section('title', 'Banner')
@section('content')

<section class="content-header">
  <h1>
   View Quiz Categories
  </h1>
  <ol class="breadcrumb">
    <button class="btn btn-primary" data-toggle="modal" data-target="#text"><i class="glyphicon glyphicon-plus"></i> </button>
    <div class="modal fade" id="text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">ADD Quiz Category</h4>
        </div> 
          <div class="modal-body">
            <form role="form" action="{{route('quiz.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('POST')

              <label>Category:</label>
              <input type="text" class="form-control" name="category" required="required" placeholder="Enter Category ">

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
              </div>
            </form>
          </div>
      </div>
    </div>
  </ol>
</section>
<br>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-xs-12">
      <div class="box" style="overflow-y: scroll;">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sno.</th>
                <th>Category</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($categories as $cat)

              <tr>
                <td>{{$i++}}</td>
                <td>{{$cat->category}}</td>
                <td>
                    @php
                      if($cat->status == 1)
                      {
                        echo 'Active';
                      }

                      else
                      {

                        echo 'Inactive';
                      }
                    @endphp
                </td>
                <td>{{$cat->created_at}}</td>
                <td>{{$cat->updated_at}}</td>
                <td>

                  <!-- Edit Button -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#{{$cat->id}}"><i class="glyphicon glyphicon-edit"></i> </button>

                <a href="{{route('quiz.edit',$cat->id)}}" class="btn btn-danger"><i class="glyphicon glyphicon-off"></i></a>
                 <div class="modal fade" id="{{$cat->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                   <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Edit Quiz Category</h4>
                    </div>
                    <div class="modal-body">
                      <form role="form" action="{{route('quiz.update',$cat->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <label>Category:</label>
                        <input type="text" class="form-control" name="category" required="required" placeholder="Enter Category " value="{{$cat->title}}">

                        <label></label>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" value="submit">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            @endforeach  

          </tbody>

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

@endsection