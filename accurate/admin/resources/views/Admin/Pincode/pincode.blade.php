@extends('layouts.admin')
@section('title', 'Pincode')
@section('content')

<section class="content-header">
  <h1>
   View Pincode
  </h1>
  <ol class="breadcrumb">
    <button class="btn btn-primary" data-toggle="modal" data-target="#text"><i class="glyphicon glyphicon-plus"></i> </button>
    <div class="modal fade" id="text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">ADD Pincode</h4>
        </div>
          <div class="modal-body">
            <form role="form" action="{{route('pincode.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('POST')

              <label>Pincode:</label>
              <input type="text" class="form-control" name="pincode" required="required"  maxlength='6' placeholder="Enter Pincode">

              <label>Delivery Days:</label>
              <input type="text" class="form-control" name="delivery_time" required="required"  maxlength='6' placeholder="Enter Delivery Days">

              <label>Max Amount:</label>
              <input type="text" class="form-control" name="max_delivery_charge" required="required"  maxlength='6' placeholder="Enter Max Amount">

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
              </div>
            </form>
          </div>
      </div>
    </div>
  </ol>
</section>
<br>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-xs-12">
      <div class="box" >
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sno.</th>
                <th>Pincode</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($data as $data)

              <tr>
                <td>{{$i++}}</td>
                <td>{{$data->pincode}}</td>
                <td>{{$data->created_at}}</td>
                <td>{{$data->updated_at}}</td>
                <td>

                  <!-- Edit Button -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#{{$data->id}}"><i class="glyphicon glyphicon-edit"></i> </button>
                <a href="{{route('pincode.delete',$data->id)}}" class="btn btn-danger"><i class="fa fa-trash fa-3"></i></a>

                 <div class="modal fade" id="{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                   <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Edit Pincode</h4>
                    </div>
                    <div class="modal-body">
                      <form role="form" action="{{route('pincode.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        
                        <label>Pincode:</label>
                        <input type="text" class="form-control" name="pincode" maxlength="6" required="required" value="{{$data->pincode}}"> 

                        <label>Delivery Days:</label>
                        <input type="text" class="form-control" name="delivery_time" required="required"  maxlength='6' placeholder="Enter Delivery Days" value="{{$data->delivery_time}}">

                        <label>Max Amount:</label>
                        <input type="text" class="form-control" name="max_delivery_charge" required="required"  maxlength='6' placeholder="Enter Max Amount" value="{{$data->max_delivery_charge}}">

                        <label></label>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" value="submit">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            @endforeach  

          </tbody>

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

@endsection