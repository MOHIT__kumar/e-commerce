<title>Users List</title>
@extends('layouts.admin')
@section('content')
<section class="content-header">
   <h1>
      Users List
   </h1>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-xs-12">
         <div class="box" style="overflow-y: scroll;">
            <!-- /.box-header -->
            <div class="box-body">
              <!-- /.box-body -->
              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>Sno.</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Mobile</th>
                     </tr>
                  </thead>
                  <tbody>
                 
                     @php $i=1; @endphp
                     @foreach($data as $datas)
                        <tr>
                          <td>{{$datas->id}}</td>
                          <td>{{$datas->name}}</td>
                          <td>{{$datas->email}}</td>
                          <td>{{$datas->mobile}}</td>
                          <td>{{$datas->created_at}}</td>
                        </tr>
                     @endforeach
                     
                  </tbody>
               </table>
               </div>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
@endsection