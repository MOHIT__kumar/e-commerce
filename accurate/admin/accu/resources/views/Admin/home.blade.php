@extends('layouts.admin')
@section('title', 'Home')
@section('content')
<section class="content-header">
   <h1>
      Dashboard
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
   </ol>
</section>
<section class="content">
   <!-- Info boxes -->
   <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-th-large"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Category</span>
               <span class="info-box-number">{{$category}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-th"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Total Products</span>
               <span class="info-box-number">{{$products}}</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">Sales</span>
               <span class="info-box-number">760</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
               <span class="info-box-text">New Members</span>
               <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->    
   <!-- Main row -->
   <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
         <!-- TABLE: LATEST ORDERS -->
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">Latest Orders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="table-responsive">
                  <table class="table no-margin">
                     <thead>
                        <tr>
                           <th>Order ID</th>
                           <th>Item</th>
                           <th>Status</th>
                           <th>Popularity</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR9842</a></td>
                           <td>Call of Duty IV</td>
                           <td><span class="label label-success">Shipped</span></td>
                           <td>
                              <div class="sparkbar" data-color="#00a65a" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR1848</a></td>
                           <td>Samsung Smart TV</td>
                           <td><span class="label label-warning">Pending</span></td>
                           <td>
                              <div class="sparkbar" data-color="#f39c12" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR7429</a></td>
                           <td>iPhone 6 Plus</td>
                           <td><span class="label label-danger">Delivered</span></td>
                           <td>
                              <div class="sparkbar" data-color="#f56954" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR7429</a></td>
                           <td>Samsung Smart TV</td>
                           <td><span class="label label-info">Processing</span></td>
                           <td>
                              <div class="sparkbar" data-color="#00c0ef" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR1848</a></td>
                           <td>Samsung Smart TV</td>
                           <td><span class="label label-warning">Pending</span></td>
                           <td>
                              <div class="sparkbar" data-color="#f39c12" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR7429</a></td>
                           <td>iPhone 6 Plus</td>
                           <td><span class="label label-danger">Delivered</span></td>
                           <td>
                              <div class="sparkbar" data-color="#f56954" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><a href="pages/examples/invoice.html">OR9842</a></td>
                           <td>Call of Duty IV</td>
                           <td><span class="label label-success">Shipped</span></td>
                           <td>
                              <div class="sparkbar" data-color="#00a65a" data-height="20">
                                 <canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas>
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
               <a href="javascript:void(0)" class="btn btn-sm btn-primary btn-flat pull-left">Place New Order</a>
               <a href="javascript:void(0)" class="btn btn-sm btn-success btn-flat pull-right">View All Orders</a>
            </div>
            <!-- /.box-footer -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4">
         <!-- PRODUCT LIST -->
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title">Recently Added Products</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <ul class="products-list product-list-in-box">

                  @foreach($product_limit as $product_limit)
                     <li class="item">
                        <div class="product-img">
                           <img src="{{URL::to('img/product/'.$product_limit->main_image)}}" alt="Product Image">
                        </div>
                        <div class="product-info">
                           <a href="{{route('product.edit',$product_limit->id)}}" class="product-title">{{$product_limit->title}}
                           <span class="product-description">
                           </span>
                        </div>
                     </li>
                  @endforeach
               </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
               <a href="{{URL::to('product')}}" class="btn btn-sm btn-primary btn-flat">View All Products</a>
            </div>
            <!-- /.box-footer -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->
</section>
@endsection