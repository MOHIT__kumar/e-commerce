@extends('layouts.admin')
@section('title', 'View Stocks')
@section('content')
<section class="content-header">
  <h1>
   View Stocks
  </h1>
  <ol class="breadcrumb">
  </ol>
</section>
<br>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-xs-12">
      <div class="box" style="overflow-y: scroll;">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sno.</th>
                <th>Product Name</th>
                <th>Size</th>
                <th>Credit</th>
                <th>Debit</th>
                <th>Remaining Stocks</th>
              </tr>
            </thead>

            <tbody>
              @php $i=1; @endphp
              @foreach ($data as $data)
                  <tr>
                     <td>{{$i++}}</td>
                      <td>{{$data->title}}</td>
                      <td>{{$data->size}}</td>
                      <td>{{$data->credit}}</td>
                      <td>{{$data->debit}}</td>
                      <td>{{$data->credit-$data->debit}}</td>
                  </tr>
              @endforeach  

          </tbody>

        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

@endsection