<ul class="sidebar-menu" data-widget="tree">

        <li class="header">MAIN NAVIGATION</li>
        <li><a href="{{URL::to('/')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
       
        <!-- Category Management -->
         <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-th"></i>
            <span>Category Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{route('category.index')}}"><i class="fa fa-circle-o"></i>Category</a></li>
              <li><a href="{{route('sub_category.index')}}"><i class="fa fa-circle-o"></i>SubCategory</a></li>
              <li><a href="{{route('sub_sub_category.index')}}"><i class="fa fa-circle-o"></i>SubSubCategory</a></li>
              <li><a href="{{route('sub_sub_sub_category.index')}}"><i class="fa fa-circle-o"></i>SubSubSubCategory</a></li>
          </ul>
        </li>

        <!-- Inventory Management -->
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-folder-close"></i>
            <span>Inventory Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{route('product.create')}}"><i class="fa fa-circle-o"></i>Create Product</a></li>
              <li><a href="{{route('product.index')}}"><i class="fa fa-circle-o"></i>View Product</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-percent"></i>
            <span>Voucher</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{route('voucher.index')}}"><i class="fa fa-circle-o"></i>Voucher List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-th"></i>
            <span>CMS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{route('banner.index')}}"><i class="fa fa-circle-o"></i>Banner</a></li>
             <li><a href="{{route('pages.index')}}"><i class="fa fa-circle-o"></i>Pages</a></li>
          </ul>
        </li> 

           <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-shopping-cart"></i>
            <span>Order Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"><!-- 
             <li><a href="{{route('orders.show',0)}}"><i class="fa fa-circle-o"></i>New Orders</a></li> -->
          </ul>
        </li> 

        <!-- user List -->
         <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{route('user.index')}}"><i class="fa fa-circle-o"></i>User List</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-cog"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{route('roles.index')}}"><i class="fa fa-circle-o"></i>Roles</a></li>
             <li><a href="{{route('users.index')}}"><i class="fa fa-circle-o"></i>Users</a></li>
          </ul>
        </li>

          

      </ul>