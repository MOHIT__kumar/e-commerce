<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
   @include('layouts.css')
</head>
<body class="skin-blue fixed sidebar-mini sidebar-mini-expand-feature">
<div class="wrapper">
  <!-- Sidebar -->
   @include('layouts.header')
      <div class="content-wrapper">
          @yield('content')
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="container">
          <div class="pull-right hidden-xs">
          </div>
          <strong>Copyright &copy; 2019-2020 <a href="https://techosoft.in/" target="_blank">TechoSoft Technologies</a></strong>
        </div>
      </footer>
    </div>
@include('layouts.notification')
@include('layouts.js')
</body>
</html>


