<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class SubSubSubCateogory extends Model
{
    protected $fillable = ['category_id', 'sub_category_id', 'sub_sub_sub_id', 'subsubsubcategory', 'created_at', 'updated_at',];
    protected $table    = 'sub_sub_sub_category';
    public $primaryKey  = 'id';
}
