<?php

use Illuminate\Support\Facades\Route;


Route::get('/','HomeController@index')->name('index');
Auth::routes();
/* Website Routes */
// Single Product view
Route::get('single_product/{id}','Website\StoreController@single_product');
// About Us
Route::get('about','Website\DashboardController@about');
//Contact Us 
Route::get('contact','Website\DashboardController@contact');
Route::get('contact_submit','Website\DashboardController@contact_submit');
// Wishlist
Route::get('wishlist/{wish}','Website\StoreController@wishlist');
Route::get('wish','Website\StoreController@wish')->name('wishlist');
// CheckOut
Route::get('checkout','Website\CheckoutController@checkout');
Route::get('proceed_checkout','Website\CheckoutController@proceed_checkout');
// Order Placed
Route::get('check_add/{add_id}','Website\CheckoutController@check_add');//Checkout address id wise
Route::get('order_placed','Website\CheckoutController@order_placed');
// Store
Route::get('store','Website\StoreController@store');
// Cart
Route::get('del_item/{id}','Website\CheckoutController@delete_cart_item');
Route::get('carts','Website\CheckoutController@cart_details');
Route::post('cart/{id}','Website\StoreController@cart');
Route::get('remove_cart/{id}','Website\StoreController@remove_item');
Route::get('cart_single','Website\StoreController@cart_single');
// My Dashboard
Route::get('dashboard','Website\DashboardController@dashboard');
Route::get('orders','Website\DashboardController@orders');
Route::get('user_address','Website\DashboardController@address');
// User Address
Route::get('address_update','Website\DashboardController@add_update');
Route::post('useradd','Website\DashboardController@useraddress');

/* Admin Dashboard */
Route::get('home','Website\AuthController@home')->name('home');
/* Login route */
Route::post('login','Website\AuthController@login');
Route::get('otp','Website\AuthController@otp')->name('otp');
/* Registeration */
Route::get('register','Website\AuthController@register');
Route::post('register_details','Website\AuthController@register_details');
/* OTP Verification */
Route::post('enter_otp','Website\AuthController@otp');

/* Admin Porfile Routes */
Route::get('profile','Admin\ProfileController@profile');
Route::put('profile_update','Admin\ProfileController@profile_update');


/* Admin Address Routes */
//Address Index
Route::get('address','Admin\AdminController@address')->name('address');
// Address Create
Route::get('address_create','Admin\AdminController@address_create');
/* Address Details */
Route::post('address_details','Admin\AdminController@address_details');
/* Address Edit */
Route::get('address_edit/{id}','Admin\AdminController@address_edit');
/* Address Update */
Route::put('address_update','Admin\AdminController@address_update');

/* Wishlist*/
Route::get('wishlist','Admin\AdminController@wishlist');