 @extends('layouts.app')
@section('content')
  <!-- Body Start -->
	<div class="wrapper">
		<div class="gambo-Breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">User Dashboard</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="dashboard-group">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="user-dt">
							<div class="user-img">
								<img src="images/avatar/img-5.jpg" alt="">
								<div class="img-add">													
									<input type="file" id="file">
									<label for="file"><i class="uil uil-camera-plus"></i></label>
								</div>
							</div>
							<h4>{{Auth::user()->name}}</h4>
							<p>{{Auth::user()->mobile}}<a href="#"><i class="uil uil-edit"></i></a></p>
							<div class="earn-points"><img src="images/Dollar.svg" alt="">Points : <span>20</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4">
						<div class="left-side-tabs">
							<div class="dashboard-left-links">
								<a href="{{URL::to('dashboard')}}" class="user-item active"><i class="uil uil-apps"></i>Overview</a>
								<a href="{{URL::to('orders')}}" class="user-item"><i class="uil uil-box"></i>My Orders</a>
								<a href="{{URL::to('wish')}}" class="user-item"><i class="uil uil-heart"></i>Shopping Wishlist</a>
								<a href="{{URL::to('user_address')}}" class="user-item"><i class="uil uil-location-point"></i>My Address</a>
							</div>
						</div>
					</div>
					<div class="col-lg-9 col-md-8">
						<div class="dashboard-right">
							<div class="row">
								<div class="col-md-12">
									<div class="main-title-tab">
										<h4><i class="uil uil-apps"></i>Overview</h4>
									</div>
									<div class="welcome-text">
										<h2>Hi! {{Auth::user()->name}}</h2>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="pdpt-bg">
										<div class="pdpt-title">
											<h4>My Orders</h4>
										</div>
										<div class="ddsh-body">
											<h2>{{$recent}} Recently Purchases</h2>
											<ul class="order-list-145">
												<li>
													<div class="smll-history">
														<div class="order-title">2 Items <span data-inverted="" data-tooltip="2kg broccoli, 1kg Apple" data-position="top center">?</span></div>
														<div class="order-status">On the way</div>
														<p>$22</p>
													</div>
												</li>
											</ul>
										</div>
										<a href="{{URL::to('orders')}}" class="more-link14">All Orders <i class="uil uil-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
	</div>
	<!-- Body End -->
@stop