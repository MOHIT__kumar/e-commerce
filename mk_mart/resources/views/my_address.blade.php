 @extends('layouts.app')

 @section('content')
   <!-- Body Start -->
	<div class="wrapper">
		<div class="gambo-Breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page"> Address</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="dashboard-group">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="user-dt">
							<div class="user-img">
								<img src="images/avatar/img-5.jpg" alt="">
								<div class="img-add">													
									<input type="file" id="file">
									<label for="file"><i class="uil uil-camera-plus"></i></label>
								</div>
							</div>
							<h4>{{Auth::user()->name}}</h4>
							<p>{{Auth::user()->mobile}}<a href="#"><i class="uil uil-edit"></i></a></p>
							<div class="earn-points"><img src="images/Dollar.svg" alt="">Points : <span>20</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4">
						<div class="left-side-tabs">
							<div class="dashboard-left-links">
								<a href="{{URL::to('dashboard')}}" class="user-item"><i class="uil uil-apps"></i>Overview</a>
								<a href="{{URL::to('orders')}}" class="user-item"><i class="uil uil-box"></i>My Orders</a>
								<a href="{{URL::to('wish')}}" class="user-item"><i class="uil uil-heart"></i>Shopping Wishlist</a>
								<a href="{{URL::to('user_address')}}" class="user-item active"><i class="uil uil-location-point"></i>My Address</a>
							</div>
						</div>
					</div>
					<div class="col-lg-9 col-md-8">
						<div class="dashboard-right">
							<div class="row">
								<div class="col-md-12">
									<div class="main-title-tab">
										<h4><i class="uil uil-location-point"></i>My Address</h4>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="pdpt-bg">
										<div class="pdpt-title">
											<h4>My Address</h4>
										</div>
										<div class="address-body">
											<a href="#" class="add-address hover-btn" data-toggle="modal" data-target="#address_model">Add New Address</a>
											@foreach($addresses as $address)
											<div class="address-item">
												<div class="address-icon1">
													<i class="uil uil-home-alt"></i>
												</div>
												<div class="address-dt-all">
													<h4>
														@if($address->address_type == 1)
														 Home
														 @elseif($address->address_type == 2)
														 Office
														 @else
														 Others
														 @endif
													</h4>
													<p>{{$address->address1}} {{$address->city}}, {{$address->pincode}}</p>	
													<ul class="action-btns">
														<li><a href="" data-toggle="modal" data-target="#address_edit/{{$address->id}}" class="action-btn"><i class="uil uil-edit"></i></a></li>
														<li><a href="#" class="action-btn"><i class="uil uil-trash-alt"></i></a></li>
													</ul>
												</div>
											</div>
          									@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
	</div>
	<!-- Body End -->

	<!-- Add Address Model Start-->
	<div id="address_model" class="header-cate-model main-gambo-model modal fade" tabindex="-1" role="dialog" aria-modal="false">
        <div class="modal-dialog category-area" role="document">
            <div class="category-area-inner">
                <div class="modal-header">
                    <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
						<i class="uil uil-multiply"></i>
                    </button>
                </div>
                <div class="category-model-content modal-content"> 
					<div class="cate-header">
						<h4>Add New Address</h4>
					</div>
					<div class="add-address-form">
						<div class="checout-address-step">
							<div class="row">
								<div class="col-lg-12">												
									<form class="" action="{{URL::to('useradd')}}" method="post">
										@csrf
										<!-- Multiple Radios (inline) -->
										<div class="form-group">
											<div class="product-radio">
												<ul class="product-now">
													<li>
														<input type="radio" id="ad1" name="address_type" checked value="1">
														<label for="ad1">Home</label>
													</li>
													<li>
														<input type="radio" id="ad2" name="address_type" value="2">
														<label for="ad2" >Office</label>
													</li>
													<li>
														<input type="radio" id="ad3" name="address_type" value="3">
														<label for="ad3">Other</label>
													</li>
												</ul>
											</div>
										</div>
										<div class="address-fieldset">
											<div class="row">
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Name*</label>
														<input id="" name="name" type="text" placeholder="Enter Name" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Mobile*</label>
														<input id="" name="mobile" type="text" placeholder="Enter Mobile" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Flat / House / Office No.*</label>
														<input id="flat" name="address1" type="text" placeholder="Address" class="form-control input-md" required="">
													</div>
												</div>

												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Street / Society / Office Name*</label>
														<input id="street" name="address2" type="text" placeholder="Street Address" class="form-control input-md">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">Pincode*</label>
														<input id="pincode" name="pincode" type="text" placeholder="Pincode" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">City*</label>
														<input id="Locality" name="city" type="text" placeholder="Enter City" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">State*</label>
														<input id="Locality" name="state" type="text" placeholder="Enter State" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group mb-0">
														<div class="address-btns">
															<button class="save-btn14 hover-btn">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Address -->
    <div id="address_edit/{{$address->id}}" class="header-cate-model main-gambo-model modal fade" tabindex="-1" role="dialog" aria-modal="false">
        <div class="modal-dialog category-area" role="document">
            <div class="category-area-inner">
                <div class="modal-header">
                    <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
						<i class="uil uil-multiply"></i>
                    </button>
                </div>
                <div class="category-model-content modal-content"> 
					<div class="cate-header">
						<h4>Add New Address</h4>
					</div>
					<div class="add-address-form">
						<div class="checout-address-step">
							<div class="row">
								<div class="col-lg-12">												
									<form class="" action="{{URL::to('useredit')}}" method="post">
										@csrf
										@method('put')
										<!-- Multiple Radios (inline) -->
										<div class="form-group">
											<div class="product-radio">
												<ul class="product-now">
													<li>
														<input type="radio" id="ad1" name="address_type" checked value="1">
														<label for="ad1">Home</label>
													</li>
													<li>
														<input type="radio" id="ad2" name="address_type" value="2">
														<label for="ad2" >Office</label>
													</li>
													<li>
														<input type="radio" id="ad3" name="address_type" value="3">
														<label for="ad3">Other</label>
													</li>
												</ul>
											</div>
										</div>
										<div class="address-fieldset">
											<div class="row">
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Name*</label>
														<input id="" name="name" type="text" value="{{$address->name}}" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Mobile*</label>
														<input id="" name="mobile" type="text" value="{{$address->mobile}}" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Flat / House / Office No.*</label>
														<input id="flat" name="address1" type="text" value="{{$address->address1}}" class="form-control input-md" required="">
													</div>
												</div>

												<div class="col-lg-12 col-md-12">
													<div class="form-group">
														<label class="control-label">Street / Society / Office Name*</label>
														<input id="street" name="address2" type="text" value="{{$address->address2}}" class="form-control input-md">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">Pincode*</label>
														<input id="pincode" name="pincode" type="text" value="{{$address->pincode}}" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">City*</label>
														<input id="Locality" name="city" type="text" value="{{$address->city}}" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-6 col-md-12">
													<div class="form-group">
														<label class="control-label">State*</label>
														<input id="Locality" name="state" type="text" value="{{$address->state}}" class="form-control input-md" required="">
													</div>
												</div>
												<div class="col-lg-12 col-md-12">
													<div class="form-group mb-0">
														<div class="address-btns">
															<button class="save-btn14 hover-btn">Update</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Edit Address -->
 @stop