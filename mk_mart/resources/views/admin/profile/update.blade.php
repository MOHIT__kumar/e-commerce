
    @extends('layouts_admin.app')

    @section('page-css')
    
    @stop

  @section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profile Management</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Update Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{URL::to('profile_update')}}">
            	@csrf	
              @method('put')
              <input type="hidden" name="id" value="{{$profile->id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="user_name" class="form-control" id="" value="{{$profile->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Address 1</label>
                  <div class="col-sm-10">
                    <input type="text" name="address1" class="form-control" id="" value="{{$profile->address}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-left">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @stop

   @section('page-js')


@stop