
    @extends('layouts_admin.app')

    @section('page-css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{URL::to('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    @stop

  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Wishlist Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Wishlist Management</a></li>
        <li class="active">Wishlist</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Wishlist Details </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Product</th>
                </tr>
                </thead>
                @php $i = 1; @endphp
                @foreach( $wishs as $wishlist)
                <tbody>
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$wishlist->user_name}}</td>
                  <td>{{$wishlist->product_name}}</td>
                </tr>
              </tbody>
              @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @stop

   @section('page-js')

<!-- DataTables -->
<script src="{{URL::to('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@stop