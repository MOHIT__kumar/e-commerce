
    @extends('layouts_admin.app')

    @section('page-css')
    <!-- DataTables -->
  <link rel="stylesheet" href="{{URL::to('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    @stop

  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Address Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Address Management</a></li>
        <li class="active">Address</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Address Details </h3>
              <a href="{{URL::to('address_create')}}" class=" btn btn-sm btn-info pull-right">Create Address</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Pincode</th>
                  <th>City</th>
                  <th>Mobile </th>
                  <th>Action</th>
                </tr>
                </thead>
                @php $i = 1; @endphp
                @foreach( $addresses as $address)
                <tbody>
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$address->name}}</td>
                  <td>{{$address->address1}}</td>
                  <td>{{$address->pincode}}</td>
                  <td>{{$address->city}}</td>
                  <td> {{$address->mobile}}</td>
                  <td><a href="{{URL::To('address_edit',$address->id)}}" class="btn btn-success btn-sm">Edit</a></td>
                </tr>
              </tbody>
              @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @stop

   @section('page-js')

<!-- DataTables -->
<script src="{{URL::to('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@stop