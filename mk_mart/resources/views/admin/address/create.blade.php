
    @extends('layouts_admin.app')

    @section('page-css')
    
    @stop

  @section('content')
  !-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Address Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Address Management</a></li>
        <li class="active">Address</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Create Address</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{URL::to('address_details')}}">
            	@csrf	
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="user_name" class="form-control" id="" placeholder="Enter Name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Mobile</label>
                  <div class="col-sm-10">
                    <input type="number" name="mobile" class="form-control" id="" placeholder="Enter Mobile">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Address 1</label>
                  <div class="col-sm-10">
                    <input type="text" name="address1" class="form-control" id="" placeholder="Enter Address">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Address 2</label>
                  <div class="col-sm-10">
                    <input type="text" name="address2" class="form-control" id="" placeholder="Enter Address">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Pincode</label>
                  <div class="col-sm-10">
                    <input type="number" name="pincode" class="form-control" id="" placeholder="Enter Pincode">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-1 control-label">City</label>
                  <div class="col-sm-10">
                    <input type="text" name="city" class="form-control" id="" placeholder="Enter City">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-1 control-label">State</label>
                  <div class="col-sm-10">
                    <input type="text" name="state" class="form-control" id="" placeholder="Enter State">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-left">Sign in</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @stop

   @section('page-js')


@stop