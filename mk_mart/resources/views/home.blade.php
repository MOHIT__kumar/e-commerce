@extends('layouts.app')
@section('content')
<!-- Body Start -->
    <div class="wrapper">
        <!-- Offers Start -->
        <div class="main-banner-slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel offers-banner owl-theme">
                            <div class="item">
                                <div class="offer-item">                                
                                    <div class="offer-item-img">
                                        <div class="gambo-overlay"></div>
                                        <img src="images/banners/offer-1.jpg" alt="">
                                    </div>
                                    <div class="offer-text-dt">
                                        <div class="offer-top-text-banner">
                                            <p>6% Off</p>
                                            <div class="top-text-1">Buy More & Save More</div>
                                            <span>Fresh Vegetables</span>
                                        </div>
                                        <a href="#" class="Offer-shop-btn hover-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="offer-item">                                
                                    <div class="offer-item-img">
                                        <div class="gambo-overlay"></div>
                                        <img src="images/banners/offer-2.jpg" alt="">
                                    </div>
                                    <div class="offer-text-dt">
                                        <div class="offer-top-text-banner">
                                            <p>5% Off</p>
                                            <div class="top-text-1">Buy More & Save More</div>
                                            <span>Fresh Fruits</span>
                                        </div>
                                        <a href="#" class="Offer-shop-btn hover-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="offer-item">                                
                                    <div class="offer-item-img">
                                        <div class="gambo-overlay"></div>
                                        <img src="images/banners/offer-3.jpg" alt="">
                                    </div>
                                    <div class="offer-text-dt">
                                        <div class="offer-top-text-banner">
                                            <p>3% Off</p>
                                            <div class="top-text-1">Hot Deals on New Items</div>
                                            <span>Daily Essentials Eggs & Dairy</span>
                                        </div>
                                        <a href="#" class="Offer-shop-btn hover-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="offer-item">                                
                                    <div class="offer-item-img">    
                                        <div class="gambo-overlay"></div>
                                        <img src="images/banners/offer-4.jpg" alt="">
                                    </div>
                                    <div class="offer-text-dt">
                                        <div class="offer-top-text-banner">
                                            <p>2% Off</p>
                                            <div class="top-text-1">Buy More & Save More</div>
                                            <span>Beverages</span>
                                        </div>
                                        <a href="#" class="Offer-shop-btn hover-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="offer-item">                                
                                    <div class="offer-item-img">
                                        <div class="gambo-overlay"></div>
                                        <img src="images/banners/offer-5.jpg" alt="">
                                    </div>
                                    <div class="offer-text-dt">
                                        <div class="offer-top-text-banner">
                                            <p>3% Off</p>
                                            <div class="top-text-1">Buy More & Save More</div>
                                            <span>Nuts & Snacks</span>
                                        </div>
                                        <a href="#" class="Offer-shop-btn hover-btn">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Offers End -->
        <!-- Categories Start -->
        <div class="section145">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title-tt">
                            <div class="main-title-left">
                                <span>Shop By</span>
                                <h2>Categories</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="owl-carousel cate-slider owl-theme">
                        
                         @foreach($category as $categorys)
                            <div class="item">
                                <a href="#" class="category-item">
                                    <div class="cate-img">
                                        <img src="{{$url}}/img/category/{{$categorys->image}}" alt="">
                                    </div>
                                    <h4>{{$categorys->category}}</h4>
                                </a>
                            </div>
                          @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Categories End -->
        <!-- Featured Products Start -->
        <div class="section145">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title-tt">
                            <div class="main-title-left">
                                <span>For You</span>
                                <h2>Top Featured Products</h2>
                            </div>
                            <a href="#" class="see-more-btn">See All</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="owl-carousel featured-slider owl-theme">
                            @foreach( $datas as $product )
                            <div class="item">
                                <div class="product-item">
                                    <a href="{{URL::to('single_product',$product['url'])}}" class="product-img">
                                        <img src="images/product/img-1.jpg" alt="">
                                        <div class="product-absolute-options">
                                            <span class="offer-badge-1">6% off</span>
                                          @if($product['wish_status'] == 0)
                                          <button id="wish" value="{{$product['url']}}"><span class="like-icon" title="wishlist"></span></button>
                                          @else
                                          <button id="wish" value="{{$product['url']}}"><span class="like-icon liked" title="wishlist"></span></button>
                                          @endif
                                        </div>
                                    </a>
                                    <div class="product-text-dt">
                                        <p>Available<span>(In Stock)</span></p>
                                        <h4>{{$product['title']}}</h4>
                                        <div class="product-price">{{$product['price']}} Rs<span>15 Rs.</span></div>
                                        <div class="qty-cart">
                                        <form method="post" action="{{URL::to('cart',$product['url'])}}">
                                            @csrf
                                            <input type="hidden" name="weight" value="{{$product['weight']}}">
                                            <input type="hidden" name="product_id" value="{{$product['url']}}">
                                            <input type="hidden" name="price" value="{{$product['price']}}">
                                            <div class="quantity buttons_added">
                                            <input type="button" value="-" class="minus minus-btn">
                                            <input type="number" step="1" name="quantity" value="1" class="input-text qty text">
                                            <input type="button" value="+" class="plus plus-btn">
                                        </div>
                                        <button class="btn">
                                            <span class="cart-icon" style="margin-left:46px;" id="submit"><i class="uil uil-shopping-cart-alt"></i></span>
                                        </button>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                           @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Featured Products End -->

        <!-- Best Values Offers Start -->
        <div class="section145">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title-tt">
                            <div class="main-title-left">
                                <span>Offers</span>
                                <h2>Best Values</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <a href="#" class="best-offer-item">
                            <img src="images/best-offers/offer-1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <a href="#" class="best-offer-item">
                            <img src="images/best-offers/offer-2.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <a href="#" class="best-offer-item offr-none">
                            <img src="images/best-offers/offer-3.jpg" alt="">
                            <div class="cmtk_dt">
                                <div class="product_countdown-timer offer-counter-text" data-countdown="2021/01/06"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Best Values Offers End -->

        <!-- New Products Start -->
        <div class="section145">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title-tt">
                            <div class="main-title-left">
                                <span>For You</span>
                                <h2>Added New Products</h2>
                            </div>
                            <a href="#" class="see-more-btn">See All</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="owl-carousel featured-slider owl-theme">
                           @foreach($datas as $products)
                            <div class="item">
                                <div class="product-item">
                                    <a href="{{URL::to('single_product',$product['url'])}}" class="product-img">
                                        <img src="images/product/img-10.jpg" alt="">
                                        <div class="product-absolute-options">
                                            <span class="offer-badge-1">New</span>
                                            <span class="like-icon" title="wishlist"></span>
                                        </div>
                                    </a>
                                    <div class="product-text-dt">
                                        <p>Available<span>(In Stock)</span></p>
                                        <h4>{{$products['title']}}</h4>
                                        <div class="product-price">Rs {{$products['price']}}<span>0</span></div>
                                        <div class="qty-cart">
                                        <form method="post" action="{{URL::to('cart',$products['url'])}}">
                                            @csrf
                                            <input type="hidden" name="weight" value="{{$products['weight']}}">
                                            <input type="hidden" name="product_id" value="{{$products['url']}}">
                                            <input type="hidden" name="price" value="{{$products['price']}}">
                                            <div class="quantity buttons_added">
                                            <input type="button" value="-" class="minus minus-btn">
                                            <input type="number" step="1" name="quantity" value="1" class="input-text qty text">
                                            <input type="button" value="+" class="plus plus-btn">
                                        </div>
                                        <button class="btn">
                                            <span class="cart-icon" style="margin-left:46px;" id="submit"><i class="uil uil-shopping-cart-alt"></i></span>
                                        </button>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- New Products End -->
    </div>
    <!-- Body End -->
@endsection
 @section('page-js')
   <script>
     $(document).ready(function(){
        $('#wish').click(function(){
            var wish = $('#wish').val();
               $.ajax({
                type:"GET",
                url:"{{URL::To('wishlist')}}"+'/'+wish,
                success:function(result){
                    $.each(result, function(key, value){
                       alert(value);
                       window.location = "{{URL::to('wish')}}";
                    });
                }
              });
        });
     });
   </script>
  @stop
