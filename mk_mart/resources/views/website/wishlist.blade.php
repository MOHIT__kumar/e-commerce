 @extends('layouts.app')

 @section('content')
  <div class="wrapper">
		<div class="gambo-Breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Wishlist</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="dashboard-group">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="user-dt">
							<div class="user-img">
								<img src="images/avatar/img-5.jpg" alt="">
								<div class="img-add">													
									<input type="file" id="file">
									<label for="file"><i class="uil uil-camera-plus"></i></label>
								</div>
							</div>
							<h4>{{Auth::user()->name}}</h4>
							<p>{{Auth::user()->mobile}}<a href="#"><i class="uil uil-edit"></i></a></p>
							<div class="earn-points"><img src="images/Dollar.svg" alt="">Points : <span>20</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4">
						<div class="left-side-tabs">
							<div class="dashboard-left-links">
								<a href="{{URL::to('dashboard')}}" class="user-item"><i class="uil uil-apps"></i>Overview</a>
								<a href="{{URL::to('orders')}}" class="user-item"><i class="uil uil-box"></i>My Orders</a>
								<a href="{{URL::to('wish')}}" class="user-item active"><i class="uil uil-heart"></i>Shopping Wishlist</a>
								<a href="{{URL::to('user_address')}}" class="user-item"><i class="uil uil-location-point"></i>My Address</a>
							</div>
						</div>
					</div>
					<div class="col-lg-9 col-md-8">
						<div class="dashboard-right">
							<div class="row">
								<div class="col-md-12">
									<div class="main-title-tab">
										<h4><i class="uil uil-heart"></i>Shopping Wishlist</h4>
									</div>
								</div>								
								<div class="col-lg-12 col-md-12">
									<div class="pdpt-bg">
										<div class="wishlist-body-dtt">
										   @foreach( $wishlists as $wishlist )
											<div class="cart-item">
												<div class="cart-product-img">
													<img src="images/product/img-14.jpg" alt="">
												</div>
												<div class="cart-text">
													<h4>{{$wishlist->product_name}}</h4>
													<div class="cart-item-price">{{$wishlist->amount}}</div>
													<button type="button" class="cart-close-btn">
														<a href="{{URL::to('wishlist',$wishlist->id)}}"><i class="uil uil-trash-alt"></i></a>
													</button>
												</div>		
											</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
	</div>
	@stop