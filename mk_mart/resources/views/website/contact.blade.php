 @extends('layouts.app')

  @section('content')
   <!-- Body Start -->
	<div class="wrapper">
		<div class="gambo-Breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="all-product-grid">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="panel-group accordion" id="accordion0">
							
							<div class="panel panel-default">
								<div class="panel-heading" id="headingThree">
									 <div class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapseThree" href="#" aria-expanded="false" aria-controls="collapseThree">
											<i class="uil uil-location-point chck_icon"></i>New Delhi
										</a>
									</div>
								</div>
								<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion0">
									<div class="panel-body">
										New Delhi Branch :<br>
										#0000, St No. 0, Lorem ipsum dolor sit amet, Main road, New Delhi<br>
										New Delhi- 141001<br>
										<div class="color-pink">Tel: 0000-000-000</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							
					<div class="col-lg-6 col-md-6">
						<div class="contact-title">
							<h2>Submit customer service request</h2>
							<p>If you have a question about our service or have an issue to report, please send a request and we will get back to you as soon as possible.</p>
						</div>
						<div class="contact-form">
							<form action="{{URL::to('contact_submit')}}">
								@csrf
								<div class="form-group mt-1">
									<label class="control-label">Full Name*</label>
									<div class="ui search focus">
										<div class="ui left icon input swdh11 swdh19">
											<input class="prompt srch_explore" type="text" name="name" id="" required="" placeholder="Your Full">															
										</div>
									</div>
								</div>
								<div class="form-group mt-1">
									<label class="control-label">Email Address*</label>
									<div class="ui search focus">
										<div class="ui left icon input swdh11 swdh19">
											<input class="prompt srch_explore" type="email" name="email" id="" required="" placeholder="Your Email Address">															
										</div>
									</div>
								</div>
								<div class="form-group mt-1">
									<label class="control-label">Subject*</label>
									<div class="ui search focus">
										<div class="ui left icon input swdh11 swdh19">
											<input class="prompt srch_explore" type="text" name="subject" id="sendersubject" required="" placeholder="Subject">															
										</div>
									</div>
								</div>
								<div class="form-group mt-1">	
									<div class="field">
										<label class="control-label">Message*</label>
										<textarea rows="2" class="form-control" id="message" name="message" required="" placeholder="Write Message"></textarea>
									</div>
								</div>
								<button class="next-btn16 hover-btn mt-3" type="submit" data-btntext-sending="Sending...">Submit Request</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<!-- Body End -->	
  @endsection