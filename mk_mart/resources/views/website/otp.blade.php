@include('layouts.css')

<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from gambolthemes.net/html-items/gambo_supermarket_demo/sign_in.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 Jul 2020 08:50:33 GMT -->
<head>
       @include('layouts.css')
    </head>

<body>
    <div class="sign-inup">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="sign-form">
                        <div class="sign-inner">
                            <div class="sign-logo" id="logo">
                                <a href="index.html"><img src="images/logo.svg" alt=""></a>
                                <a href="index.html"><img class="logo-inverse" src="images/dark-logo.svg" alt=""></a>
                            </div>
                            <div class="form-dt">
                                <div class="form-inpts checout-address-step">
                                    <form action="{{URL::to('enter_otp')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="mobile_no" value="{{$phone}}">
                                        <div class="form-title"><h6>Enter OTP</h6></div>
                                        <div class="form-group pos_rel">
                                            <input id="" name="otp" type="number" placeholder="Enter OTP" class="form-control lgn_input" required="">
                                            <i class="uil uil-mobile-android-alt lgn_icon"></i>
                                        </div>
                                        <button class="login-btn hover-btn" type="submit">Verify</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @include('layouts.js')
    
    
</body>
</html>