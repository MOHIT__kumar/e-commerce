 @extends('layouts.app')

 @section('content')
  <div class="wrapper">
		<div class="gambo-Breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Vegetables & Fruits</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="all-product-grid">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-top-dt">
							<div class="product-left-title">
								<h2>Vegetables & Fruits</h2>
							</div>
							<a href="#" class="filter-btn pull-bs-canvas-right">Filters</a>
							<div class="product-sort">
								<div class="ui selection dropdown vchrt-dropdown">
									<input name="gender" type="hidden" value="default">
									<i class="dropdown icon d-icon"></i>
									<div class="text">Popularity</div>
									<div class="menu">
										<div class="item" data-value="0">Popularity</div>
										<div class="item" data-value="1">Price - Low to High</div>
										<div class="item" data-value="2">Price - High to Low</div>
										<div class="item" data-value="3">Alphabetical</div>
										<div class="item" data-value="4">Saving - High to Low</div>
										<div class="item" data-value="5">Saving - Low to High</div>
										<div class="item" data-value="6">% Off - High to Low</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="product-list-view">
					<div class="row">
						@foreach( $products  as $product)
						  <div class="col-lg-3 col-md-6">
							<div class="product-item mb-30">
								<a href="{{URL::to('single_product',$product['url'])}}" class="product-img">
									<img src="images/product/img-2.jpg" alt="">
									<div class="product-absolute-options">
										<span class="offer-badge-1">2% off</span>
										 @if($product['wish_status'] == 0)
										  <button id="wish" value="{{$product['url']}}"><span class="like-icon" title="wishlist"></span></button>
										  @else
										  <button id="wish" value="{{$product['url']}}"><span class="like-icon liked" title="wishlist"></span></button>
										  @endif
									</div>
								</a>
								<div class="product-text-dt">
									<p>Available<span>(In Stock)</span></p>
									<h4>{{$product['title']}}</h4>
									<div class="product-price">{{$product['price']}} Rs <span>13 Rs</span></div>
									<div class="qty-cart">
										<form method="post" action="{{URL::to('cart',$product['url'])}}">
											@csrf
											<input type="hidden" name="weight" value="{{$product['weight']}}">
											<input type="hidden" name="product_id" value="{{$product['url']}}">
											<input type="hidden" name="price" value="{{$product['price']}}">
										<div class="quantity buttons_added">
											<input type="button" value="-" class="minus minus-btn">
											<input type="number" step="1" name="quantity" value="1" class="input-text qty text">
											<input type="button" value="+" class="plus plus-btn">
										</div>
										<button class="btn">
											<span class="cart-icon" style="margin-left:90px;" id="submit"><i class="uil uil-shopping-cart-alt"></i></span>
										</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						<div class="col-md-12">
							<div class="more-product-btn">
								<button class="show-more-btn hover-btn" onclick="window.location.href = '#';">Show More</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 @stop
  @section('page-js')
   <script>
   	 $(document).ready(function(){
        $('#wish').click(function(){
        	var wish = $('#wish').val();
        	   $.ajax({
		        type:"GET",
		        url:"{{URL::To('wishlist')}}"+'/'+wish,
		        success:function(result){
		        	$.each(result, function(key, value){
                       alert(value);
                       window.location = "{{URL::to('wish')}}";
		        	});
		        }
		      });
        });
   	 });
   </script>
  @stop
   