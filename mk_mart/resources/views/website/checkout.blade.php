@extends('layouts.app') 
@section('page-css')
<link href="css/step-wizard.css" rel="stylesheet">
@stop
@section('content')
<!-- Body Start -->
<div class="wrapper">
   <div class="gambo-Breadcrumb">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                  </ol>
               </nav>
            </div>
         </div>
      </div>
   </div>
   <div class="all-product-grid">
      <div class="container">
         <form class="" action="{{URL::to('proceed_checkout')}}">
            @csrf
         <div class="row">

            <div class="col-lg-8 col-md-7">
               <div id="checkout_wizard" class="checkout accordion left-chck145">
                  <div class="checkout-step">
                     <div class="checkout-card" id="headingTwo">
                        <span class="checkout-step-number">1</span>
                        <h4 class="checkout-step-title">
                           <button class="wizard-btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Delivery Address</button>
                        </h4>
                     </div>
                     <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#checkout_wizard">
                        <div class="checkout-step-body">
                           <div class="checout-address-step">
                              <div class="row">
                                 <div class="col-lg-12">
                                    
                                       <div id="address_id"></div>
                                       <!-- Multiple Radios (inline) -->
                                       <div class="form-group">
                                          <div class="product-radio">
                                             <ul class="product-now">
                                                @php $i = 1; @endphp
                                                @foreach( $address as $add)
                                                <li>
                                                   <input type="radio" id="add" name="address" value="" checked >
                                                   <label for="" onclick="address({{$add->id}})">Address {{$i++}}</label>
                                                </li>
                                                @endforeach
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="address-fieldset" id="" class="collapse">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label">Name*</label>
                                                   <div id="name"></div>
                                                </div>
                                             </div>
                                              <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label"> Mobile No.*</label>
                                                   <div id="mobile"></div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label"> Address *</label>
                                                   <div id="address1"></div>
                                                </div>
                                             </div>
                                             <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label">Flat / House / Office No.*</label>
                                                   <div id="address2"></div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label">Pincode*</label>
                                                   <div id="pincode"></div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label">City*</label>
                                                   <div id="city"></div>
                                                </div>
                                             </div>
                                             <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="control-label">State*</label>
                                                   <div id="state"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="checkout-step">
                     <div class="checkout-card" id="headingFour">
                        <span class="checkout-step-number">2</span>
                        <h4 class="checkout-step-title"> 
                           <button class="wizard-btn collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Payment</button>
                        </h4>
                     </div>
                     <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#checkout_wizard">
                        <div class="checkout-step-body">
                           <div class="payment_method-checkout">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="rpt100">
                                       <ul class="radio--group-inline-container_1">
                                          <li>
                                             <div class="radio-item_1">
                                                <input id="cashondelivery1" value="cashondelivery" name="paymentmethod" type="radio" data-minimum="50.0">
                                                <label for="cashondelivery1" class="radio-label_1">Cash on Delivery</label>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="form-group return-departure-dts" data-method="cashondelivery">
                                       <div class="row">
                                          <div class="col-lg-12">
                                             <div class="pymnt_title">
                                                <h4>Cash on Delivery</h4>
                                                <p>Cash on Delivery will not be available if your order value exceeds $10.</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-5">
               <div class="pdpt-bg mt-0">
                  <div class="pdpt-title">
                     <h4>Order Summary</h4>
                  </div>

                   @foreach( $details as $detail)
                  <div class="right-cart-dt-body">
                     <div class="cart-item border_radius">
                        <div class="cart-product-img">
                           <img src="images/product/img-11.jpg" alt="">
                           <div class="offer-badge">4% OFF</div>
                        </div>
                           <div class="cart-text">
                           <h4>{{$detail['title']}}</h4>
                           <div class="cart-item-price">{{$detail['price']}} <span>$18</span></div>
                           <button type="button" class="cart-close-btn"><i class="uil uil-multiply"></i></button>
                        </div>
                        
                     </div>
                  </div>
                   @endforeach
                   <div class="total-checkout-group">
                     <div class="cart-total-dil">
                     <div class="cart-total-dil pt-3">
                        <h4>Delivery Charges</h4>
                        <input type="hidden" name="delivery_charge" value="{{$detail['delivery_charge']}}">
                        <span>{{$detail['delivery_charge']}}</span>
                     </div>
                  </div>
                  <div class="main-total-cart">
                     <h2>Total</h2>
                     <input type="hidden" name="price" value="{{$price}}">
                     <span>{{$price}}</span>
                  </div>
                  <div class="payment-secure">
                  <button type="submit" class="next-btn16 hover-btn">Place Order</button>
                     <i class="uil uil-padlock"></i>Secure checkout
                  </div>
               </div>
            </div>
         </div>
      </form>
      </div>
   </div>
</div>
<!-- Body End -->
@stop
@section('page-js')
<script>
   function address(add_id){
      $.ajax({
          type:"GET",
          url:"{{URL::To('check_add')}}"+'/'+add_id,
          success:function(result){
            $("#address_id").empty();
            $("#name").empty();
            $("#mobile").empty();
            $("#address1").empty();
            $("#address2").empty();
            $("#pincode").empty();
            $("#city").empty();
            $("#state").empty();
            var id = result['id']
            var name   = result['name']
            var mobile = result['mobile']
            var add1   = result['address1']+' '+result['address2']
            var pin    = result['pincode']
            var city   = result['city']
            var state  = result['state']
            $('#address_id').append(' <input name="id" value="'+id+'" type="hidden" class="form-control input-md" required="">')
            $('#name').append(' <input name="name" value="'+name+'" type="text" class="form-control input-md" required="">')
             $('#mobile').append(' <input name="mobile" value="'+mobile+'" type="text" class="form-control input-md" required="">')
             $('#address1').append(' <input name="address1" value="'+add1+'" type="text" class="form-control input-md" required="">')
               $('#pincode').append(' <input name="pincode" value="'+pin+'" type="text" class="form-control input-md" required="">')
                $('#city').append(' <input name="city" value="'+city+'" type="text" class="form-control input-md" required="">')
                 $('#state').append(' <input name="state" value="'+state+'" type="text" class="form-control input-md" required="">')
          }
        });
   }
</script>
@stop