<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   @include('layouts_admin.css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('layouts_admin.topbar')
		 @include('layouts_admin.sidebar')
		  @yield('content')
		   <footer class="main-footer">
		    <div class="pull-right hidden-xs">
		      <b>Version</b> 2.4.0
		    </div>
		    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
		    reserved.
		  </footer>
	
@include('layouts_admin.js')
</body>
</html>