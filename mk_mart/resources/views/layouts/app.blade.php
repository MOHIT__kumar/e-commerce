<!DOCTYPE html>
<html lang="en">
    @include('layouts.css')
<body>
    @include('layouts.top')
    
      @guest
      @include('layouts.header2')
        @else
         @include('layouts.header')
          @endif
          
        @yield('content')
    @include('layouts.footer')
    @include('layouts.js')
      @include('website.cart')

</body>
</html>