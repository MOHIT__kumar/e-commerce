	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, shrink-to-fit=9">
		<meta name="description" content="Gambolthemes">
		<meta name="author" content="Gambolthemes">		
		<title>MK - Mart</title>
		
		<!-- Favicon Icon -->
		<link rel="icon" type="image/png" href="images/fav.png">
		
		<!-- Stylesheets -->
		<link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet">
		<link href='{{URL::to("vendor/unicons-2.0.1/css/unicons.css")}}' rel='stylesheet'>
		<link href="{{URL::to('css/style.css')}}" rel="stylesheet">
		<link href="{{URL::to('css/responsive.css')}}" rel="stylesheet">
		<link href="{{URL::to('css/night-mode.css')}}" rel="stylesheet">
		
		<!-- Vendor Stylesheets -->
		<link href="{{URL::to('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
		<link href="{{URL::to('vendor/OwlCarousel/assets/owl.carousel.css')}}" rel="stylesheet">
		<link href="{{URL::To('vendor/OwlCarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet">
		<link href="{{URL::to('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{URL::to('vendor/semantic/semantic.min.css')}}">	
		
        @yield('page-css')

	</head>
