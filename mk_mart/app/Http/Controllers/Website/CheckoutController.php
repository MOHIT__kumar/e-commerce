<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;  
use Auth;

class CheckoutController extends Controller
{
    public function cart_details()
    {
         $carts = DB::table('checkout_carts')->where('user_id',Auth::user()->id)
                                   ->join('products','checkout_carts.product_id','products.id')
                                   ->where('products.status',1)
                                   ->select('checkout_carts.size','checkout_carts.id','checkout_carts.qty','checkout_carts.price','products.title')
                                   ->get();
         $cart_details = DB::table('checkout_carts')->where('user_id',Auth::user()->id)->get();
          //dd($carts);
         return view('website.cart',compact('carts'));
    }

    public function delete_cart_item($id)
    {
      DB::table('checkout_carts')->where('id',$id)->delete();
      Session::flash('success','Your cart item has been deleted succefully');
      return back();
    }

    public function order_placed()
    {
        return view('website.order_placed');
    }

    public function checkout()
    {
         $carts = DB::table('checkout_carts')->where('user_id',Auth::user()->id)
                                   ->join('products','checkout_carts.product_id','products.id')
                                   ->join('pincode','products.id','pincode.checkout_id')
                                   ->where('products.status',1)
                                   ->select('checkout_carts.size','checkout_carts.id','checkout_carts.qty','checkout_carts.price','products.title','pincode.max_delivery_charge as delivery_charge')
                                   ->get();
                  $price = 0;
                foreach ($carts as $key) {
                  $price += $key->price;
                  $details[] = array(
                    'title'   => $key->title,
                    'price'   => $key->price,
                    'delivery_charge' => $key->delivery_charge, 
                  );
                }
               // dd($details);

    	$address = DB::table('address')->where('user_id',Auth::user()->id)->get();
    	  
        return view('website.checkout',compact('address','carts','price','details'));
    }

    public function proceed_checkout(Request $request)
    {
      //dd($request->all());
    	  DB::table('checkout')->insert([
          'auth_id'           => Auth::user()->id,
          'address'           => $request->address1,
          'pincode'           => $request->pincode,
          'transaction_id'    => mt_rand(1000,20000),
          'delivery_charges'  => $request->delivery_charge,
          'total_price'       => $request->price,
          'payment_type'      => $request->paymentmethod
        ]);
        Session::flash('success','Your order has been placed succefully');
        return back();
    }

    public function check_add($id)
    {
        $address = DB::table('address')->where('id',$id)->first();
        $array = array();
        return response()->json([
          'id'        => $address->id,
          'name'      => $address->name,
          'mobile'    => $address->mobile,
          'address1'  => $address->address1,
          'address2'  => $address->address2,
          'pincode'   => $address->pincode,
          'city'      => $address->city,
          'state'     => $address->state
           ]);         
    }
}
