<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use Carbon\Carbon;

class StoreController extends Controller
{
    public function store()
    {
    	$product  = DB::table('products')->limit('9')->orderby('products.id','DESC')
    	                                 ->join('productsizes','products.id','productsizes.product_id')
    	                                 ->Leftjoin('wishlists','products.id','wishlists.product_id')
    	                                 ->select('products.*','productsizes.weight','wishlists.status as wish_status')
    	                                 ->get();

         //dd($product);
        $datas = [];
        foreach ($product as $key) {
            $products[] = array(
                    'url'        => $key->id,
                    'title'      => $key->title, 
                    'wish_status'=> $key->wish_status,
                    'main_image' => $key->main_image,
                    'weight'     => $key->weight,
                    'price'      => $this->asset($key->id)
                );
        }
    	return view('website.store',compact('products'));
    }

     /*assets*/
    public function asset($id){

        $Productsize = DB::table('productsizes')->Where('product_id',$id)->first();

        if ($Productsize) {
            return $Productsize->amount;
        }
        else
        {
            return 0;
        }
        
    }

    public function wishlist($id)
    {
    	$wish = DB::table('wishlists')->where('id',$id)->first();
    	 if ($wish->status == 1) {
    	 	DB::table('wishlists')->where('id',$id)->update([
    	 		'status'  => '0']);
    	 }else{
    	 	DB::table('wishlists')->where('id',$id)->update([
    	 		'status'  => '1']);
    	 }
    	Session::flash('success','Product removed from wishlist');
    	return back();
    }

    public function wish()
    {
    	$wishlists = DB::table('wishlists')->where('user_id',Auth::user()->id)
    	 								   ->where('wishlists.status',1)
    	                             	   ->join('products','wishlists.product_id','products.id')
    	                             	   ->join('productsizes','products.id','productsizes.product_id')
    	                             	   ->select('wishlists.*','products.title as product_name','productsizes.amount')
    	                             	   ->get();
    	     // dd($wishlists);
    	return view('website.wishlist',compact('wishlists'));
    }

    public function cart( Request $request)
    {
        //dd($request->all());

    	 $price = $request->quantity * $request->price;

    	 $cart_details = DB::table('checkout_carts')->insert([
    	 	'user_id'        => Auth::user()->id,
    	 	'product_id'     => $request->id,
    	 	'size'           => $request->weight,
    	 	'price'          => $price,
    	 	'qty'            => $request->quantity,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
    	 ]);
          
    	 Session::flash('success','Product added in your cart');
         return back();
    }

    public function single_product($id)
    { 
        $product = DB::table('products')->where('products.status',1)
                                      ->join('productsizes','products.id','productsizes.product_id')
                                      ->select('products.*','productsizes.weight','productsizes.amount')
                                      ->first();
            //dd($product);
      return view('website.single_product',compact('product'));
    }

    public function cart_single(Request $request)
    {
        $price = $request->quantity * $request->price;

         $cart_details = DB::table('checkout_carts')->insert([
            'user_id'        => Auth::user()->id,
            'product_id'     => $request->id,
            'size'           => $request->weight,
            'price'          => $price,
            'qty'            => $request->quantity,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now(),
         ]);
          
         Session::flash('success','Product added in your cart');
         return back();
    }

    public function remove_item($id)
    {
       DB::table('checkout_carts')->where('id',$id)->delete();
       Session::flash('success','Item Removed successfully');
       return back();
    }

}
