<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $dash = DB::table('checkout')->where('auth_id',Auth::user()->id)->latest()->first();
        $recent = DB::table('checkout_carts')->where('checkout_id',$dash->id)->count();
           
    	return view('dashboard',compact('recent'));
    }

    public function orders()
    {
    	$orders = DB::table('checkout')->where('auth_id',Auth::user()->id)
    	 							 ->join('checkout_carts','checkout.id','checkout_carts.checkout_id')
    	 							 ->join('products','checkout_carts.product_id','products.id')
    	 							 ->select('checkout.*','checkout_carts.*','products.title as product')
    	 							   ->get();
    	//dd($orders);
    	return view('orders',compact('orders'));
    }

    public function address()
    { 
    	$addresses = DB::table('address')->where('user_id',Auth::user()->id)->get();
    	return view('my_address',compact('addresses'));
    }

    public function useraddress( Request $request)
    { 
        $check = DB::table('address')->where('user_id',Auth::user()->id)->count();
         //dd($check);

    	if ($check == 3) {
    	   Session::flash('error','You can not add more than three addresses');
    	}
    	else{
    		DB::table('address')->insert([
    	    'user_id'      => Auth::user()->id,
    	    'name'		   => $request->name,
    	    'mobile'       => $request->mobile,
    		'address1'     => $request->address1,
    		'address2'     => $request->address2,
    		'pincode'      => $request->pincode,
    		'city'         => $request->city,
    		'state'        => $request->state,
    		'address_type'  => $request->address_type
    	]);
    	Session::flash('success','Your address added succesfully');
    	}
    	return back();
    }

    public function add_update(Request $request)
    {
         $request->validate([
            'name'     => 'required',
            'mobile'   => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'city'     =>  'required',
            'state'    => 'required'
        ]);

        DB::table('address')->where('id',$request->id)->update([
            'name'     => $request->name,
            'mobile'   => $request->mobile,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'pincode'  => $request->pincode,
            'city'     => $request->city,
            'state'    => $request->state
        ]);

        session::flash('success','Your address saved successfully');
        return back();
    }

    public function contact()
    {
        return view('website.contact');
    }

    public function contact_submit(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'name'    => 'required',
            'email'   => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

      DB::table('contacts')->insert([
        'name'     => $request->name,
        'email'    => $request->email,
        'subject'  => $request->subject,
        'massage'  => $request->message
    ]);

      Session::flash('success','Your massage has been sent successfully');
      return back();
    }

    public function about()
    {
        return view('website.about');
    }
}
