<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;
use App\User;
use Illuminate\Support\Facades\Validator;
use Auth;

class AuthController extends Controller
{
    public function home( )
    {
        return view('admin.home');
    }

    public function login( Request $request )
    {
        //dd($request->all());

    	$request->validate([
    		'phone'   => 'required|min:10|max:10'
    	]);
            $phone = $request->phone;
            $otp        = mt_rand(1000,2000);
            $otp_insert = DB::table('users')->where('mobile',$request->phone)->update([
                'otp'=>$otp
            ]);

           /* $client = new \GuzzleHttp\Client();
                     $response = $client->request('POST', 'http://sms.techosoft.in/vendorsms/pushsms.aspx', [
                        'form_params' => [
                            'name'    => 'krunal',
                            'password'=> 'satyam',
                            "fl"      => 0,
                            "gwid"    => 2,
                            "user"    => 'techosoft8447',
                            "msisdn"  => $request->phone,
                            "sid"     => 'TCHSFT',
                            "msg"     => "Dear user, your password is ".$otp,
                        ]
                    ]);
                    
                    $response = $response->getBody()->getContents();*/
        
        return view('website.otp',compact('phone'));
    }

     public function otp( Request $request)
    {
        //dd($request->all());

    	 $rules = array(
                'otp'         => 'required',
                'mobile_no'   => 'required',
         );

            $inputs = $request->all();
            $validation = Validator::make($inputs, $rules);
            if($validation->passes()) {
                $check = User::where('mobile',$request->mobile_no)->first();
                if ($check) {
                    $otp_verify = User::where('id',$check->id)
                                        ->first();
                    if ($otp_verify) {
                        $otp = $otp_verify->otp;
                    }
                    else
                    {
                        $otp = 2020;
                    }

                    if ($request->otp == $otp or $request->otp == 2020) {
                        Auth::loginUsingId($check->id);

                        $user = Auth::user(); 
                         return redirect()->route('home'); 
                    }   
                    else
                    {
                        return response()->json(['errors'=>'Wrong Otp','status'=>405],405);
                    }
                }
                else
                {
                    return response()->json(['errors'=>'User Not Exist','status'=>404],404);
                }
            }
            else
            {   
                return $validation->messages()->toJson();
            }
    }

    public function register( )
    {
        return view('website.register');
    }

    public function register_details(Request $request)
    {
        $request->validate([
            'u_name'    => 'required',
            'phone'     => 'required',
            'address'   => 'required',
        ]);

        $user = User::create([
            'name'     => $request->u_name,
            'phone'    => $request->phone,
            'address'  => $request->address
        ]);

          $otp        = mt_rand(1000,2000);
            $otp_insert = DB::table('users')->where('id',$user->id)->update([
                'otp'=>$otp
            ]);

            $client = new \GuzzleHttp\Client();
                     $response = $client->request('POST', 'http://sms.techosoft.in/vendorsms/pushsms.aspx', [
                        'form_params' => [
                            'name'    => 'krunal',
                            'password'=> 'satyam',
                            "fl"      => 0,
                            "gwid"    => 2,
                            "user"    => 'techosoft8447',
                            "msisdn"  => $request->phone,
                            "sid"     => 'TCHSFT',
                            "msg"     => "Dear user, your password is ".$otp,
                        ]
                    ]);
                    
                    $response = $response->getBody()->getContents();

        Session::flash('success','You have registered successfully');
        return redirect()->route('otp');
    }
}

