<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Config;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $url = config::get('app.admin_url');

        $category = DB::table('categories')->get();
        $product = DB::table('products')->limit('9')->orderby('products.id','DESC')
                                         ->join('productsizes','products.id','productsizes.product_id')
                                         ->Leftjoin('wishlists','products.id','wishlists.product_id')
                                         ->select('products.*','productsizes.weight','wishlists.status as wish_status')
                                         ->get();
        if (Auth::check()) {
            $carts = DB::table('checkout_carts')->where('user_id',Auth::user()->id)
                                   ->join('products','checkout_carts.product_id','products.id')
                                   ->where('products.status',1)
                                   ->select('checkout_carts.size','checkout_carts.id','checkout_carts.qty','checkout_carts.price','products.title')
                                   ->get();
       // dd($carts);

        $datas = [];
        foreach ($product as $key) {
            $datas[] = array(
                    'url'        => $key->id,
                    'title'      => $key->title, 
                    'wish_status'=> $key->wish_status,
                    'main_image' => $key->main_image,
                    'weight'     => $key->weight,
                    'price'      => $this->asset($key->id)
                );
        }
        return view('home',compact('category','url','datas','carts'));
        }
        else{
           $datas = [];
        foreach ($product as $key) {
            $datas[] = array(
                    'url'        => $key->id,
                    'title'      => $key->title, 
                    'wish_status'=> $key->wish_status,
                    'main_image' => $key->main_image,
                    'weight'     => $key->weight,
                    'price'      => $this->asset($key->id)
                );
        }
        return view('home',compact('category','url','datas')); 
        }
    }

    /*assets*/
    public function asset($id){

        $Productsize = DB::table('productsizes')->Where('product_id',$id)->first();

        if ($Productsize) {
            return $Productsize->amount;
        }
        else
        {
            return 0;
        }
        
    }
}
