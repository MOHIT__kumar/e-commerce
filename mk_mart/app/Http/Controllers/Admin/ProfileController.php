<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class ProfileController extends Controller
{
    public function profile()
    {
    	$profile = DB::table('users')->where('id',Auth::user()->id)->first();
    	return view('admin.profile.update',compact('profile'));
    }

    public function profile_update( Request $request)
    {
    	$request->validate([
    		'user_name'  => 'required',
    		'address1'   => 'required'
    	]);

    	DB::table('users')->where('id',$request->id)->update([
    	    'name'  => $request->user_name,
    		'address'   => $request->address1
    	]);
    	Session::flash('success','Your profile has been updated succesfully');
    	return back();
    }
}
