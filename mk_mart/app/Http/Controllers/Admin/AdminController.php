<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use Session;
use Auth;

class AdminController extends Controller
{
    public function address()
    {
    	$addresses = DB::table('address')->where('user_id',Auth::user()->id)->get();
    	//dd($addresses);
    	return view('admin.address.index',compact('addresses'));
    }

    public function address_create()
    {
    	return view('admin.address.create');
    }

    public function address_details( Request $request)
    {
    	$request->validate([
    		'user_name'   => 'required',
    		'mobile'      => 'required',
    		'address1'    => 'required',
    		'address2'    => 'required',
    		'pincode'     => 'required',
    		'city'        => 'required',
    		'state'       => 'required'
    	]);

    	   $check = DB::table('address')->where('user_id',Auth::user()->id)->count();
    	     if ($check == 3) {
    	     	Session::flash('error','Your can only fill 3 address');
    	     }
    	     else{
    	     DB::table('address')->insert([
    		'user_id'     => Auth::user()->id,
    		'name'   	  => $request->user_name,
    		'mobile'      => $request->mobile,
    		'address1'    => $request->address1,
    		'address2'    => $request->address2,
    		'pincode'     => $request->pincode,
    		'city'        => $request->city,
    		'state'       => $request->state
    	   		]);
    			Session::flash('success','Your address has been created successfully');
    	     }

    	return redirect()->route('address');
    }

    public function address_edit($id)
    {
    	$address = DB::table('address')->find($id);
    	 return view('admin.address.edit',compact('address'));
    }

    public function address_update( Request $request)
    {


    	$check = DB::table('address')->where('user_id',Auth::user()->id)->count();
    	     if ($check == 3) {
    	     	Session::flash('error','Your can only fill 3 address');
    	     }
    	     else{
    	 $address_update = DB::table('address')->where('id',$request->id)->update([
    	 	'user_id'     => Auth::user()->id,
    		'name'   	  => $request->user_name,
    		'mobile'      => $request->mobile,
    		'address1'    => $request->address1,
    		'address2'    => $request->address2,
    		'pincode'     => $request->pincode,
    		'city'        => $request->city,
    		'state'       => $request->state
    	]);
          }
          
    	 Session::flash('success','Your address has been updated successfully');
    	 return redirect()->route('address');
    }

    public function wishlist()
    {
        $wishs = DB::table('wishlists')->join('products','wishlists.product_id','products.id')
                                       ->join('users','wishlists.user_id','users.id')
                                       ->select('wishlists.*','products.title as product_name','users.name as user_name')
                                       ->get();
          
        
         //dd($wishs);
        return view('admin.wishlist.index',compact('wishs'));
    }
}
