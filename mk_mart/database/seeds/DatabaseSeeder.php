<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       for ($i=0; $i < 20; $i++) { 
       	  DB::table('products')->insert([
            'category_id' => random_int (1,1),
            'sub_category_id' => random_int(1,1),
            'sub_sub_category_id'  => random_int(1,1),
            'meta_title'           => str::random(10),
            'meta_keyword'			=> str::random(10),
            'title'					=> str::random(10),
            'description'			=> str::random(20),
            'status'				=> 1,
            'main_image'			=> str::random(9),
        ]);
       }
    }
}
