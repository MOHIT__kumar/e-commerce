<!DOCTYPE html>
<html>
<head>
	<title>Footwear Ecommerce </title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('layouts.css')

</head>
<body>
	<div class="colorlib-loader"></div>
	  <div id="page">
	  	@include('layouts.navbar')
	  	 @yield('content')

	  	  @include('layouts.footer')
	  	
	  </div>
	  <div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>

 @include('layouts.js')
</body>
</html>