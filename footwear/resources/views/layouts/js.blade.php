
	<!-- jQuery -->
	<script src="{{URL::to('web/js/jquery.min.js')}}"></script>
   <!-- popper -->
   <script src="{{URL::to('web/js/popper.min.js')}}"></script>
   <!-- bootstrap 4.1 -->
   <script src="{{URL::to('web/js/bootstrap.min.js')}}"></script>
   <!-- jQuery easing -->
   <script src="{{URL::to('web/js/jquery.easing.1.3.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{URL::to('web/js/jquery.waypoints.min.js')}}"></script>
	<!-- Flexslider -->
	<script src="{{URL::to('web/js/jquery.flexslider-min.js')}}"></script>
	<!-- Owl carousel -->
	<script src="{{URL::to('web/js/owl.carousel.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{URL::to('web/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{URL::to('web/js/magnific-popup-options.js')}}"></script>
	<!-- Date Picker -->
	<script src="{{URL::to('web/js/bootstrap-datepicker.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{URL::to('web/js/jquery.stellar.min.js')}}"></script>
	<!-- Main -->
	<script src="{{URL::to('web/js/main.js')}}"></script>