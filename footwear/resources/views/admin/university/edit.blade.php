	 @extends('admin.layouts.app')

 @section('page-css')
 
 @endsection

 @section('content')
     <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Forms</a></li>
                                            <li class="breadcrumb-item active">Validation</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Update University</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Update University Here</h4>                                        

                                        <form class="needs-validation" novalidate action="{{route('university.update',$edit->id)}}" method="post">
                                        	@csrf
                                           @method('put')

                                            <div class="form-group mb-3">
                                                <label for="validationCustom03">University</label>
                                                <input type="text" class="form-control" id="validationCustom03" name="university" value="{{$edit->university_name}}" required>
                                                <div class="invalid-feedback">
                                                    Please provide a valid University Name.
                                                </div>
                                            </div>
                                            <button class="btn btn-primary" type="submit">Update</button>
                                        </form>

                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

            </div>

 @endsection

 @section('footer-js')
<!-- Validation init js-->
        <script src="{{URL::to('admin/js/pages/form-validation.init.js')}}"></script>
 @endsection