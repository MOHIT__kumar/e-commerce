@extends('admin.layouts.app')
@section('page-css')
<link href="{{URL::to('admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::to('admin/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="content-page">
   <div class="content">
      <!-- Start Content-->
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box">
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item active">Customers</li>
                     </ol>
                  </div>
                  <h4 class="page-title">Customers</h4>
               </div>
            </div>
         </div>
         <!-- end page title --> 
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row mb-2">
                        <div class="col-sm-4">
                           <a href="{{route('university.create')}}" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle mr-2"></i> Add university</a>
                        </div>
                        <div class="col-sm-8">
                           <div class="text-sm-right">
                              <button type="button" class="btn btn-success mb-2 mr-1"><i class="mdi mdi-cog"></i></button>
                              <button type="button" class="btn btn-light mb-2 mr-1">Import</button>
                              <button type="button" class="btn btn-light mb-2">Export</button>
                           </div>
                        </div>
                        <!-- end col-->
                     </div>
                     <div class="table-responsive">
                        <table class="table table-centered table-striped dt-responsive nowrap w-100" id="products-datatable">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>University Name</th>
                                 <th>Status</th>
                                 <th style="width: 75px;">Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach( $indexes as $index )
                              @php $i = 1; @endphp
                              <tr>
                                 <td class="table-user">
                                    <a href="javascript:void(0);" class="text-body font-weight-semibold">{{$i}}</a>
                                 </td>
                                 <td class="table-user">
                                    <a href="javascript:void(0);" class="text-body font-weight-semibold">{{$index->university_name}}</a>
                                 </td>
                                 <td>
                                    @if( $index->status == 1)
                                    <span class="badge badge-soft-success">Active</span>
                                    @else
                                    <span class="badge badge-soft-danger">In-Active</span>
                                    @endif
                                 </td>
                                 <td style="display: inline-flex;">
                                    <a href="{{route('university.edit',$index->id)}}" class="action-icon text-success"> <i class="mdi mdi-square-edit-outline"></i></a>
                                    <a href="{{route('university.show',$index->id)}}" class="action-icon text-danger"> <i class="mdi mdi-delete"></i></a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- end card-body-->
               </div>
               <!-- end card-->
            </div>
            <!-- end col -->
         </div>
         <!-- end row -->
      </div>
      <!-- container -->
   </div>
   <!-- content -->
</div>
@endsection
@section('footer-js')
<!-- third party js -->
<script src="{{URL::to('admin/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('admin/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('admin/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('admin/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('admin/libs/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js')}}"></script>
@endsection