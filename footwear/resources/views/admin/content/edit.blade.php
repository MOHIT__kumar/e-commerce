 @extends('admin.layouts.app')

 @section('page-css')
        <link href="{{URL::to('admin/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />
 @endsection

 @section('content')
     <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Forms</a></li>
                                            <li class="breadcrumb-item active">Validation</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Update Content</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Update Content Here</h4>                                        

                                        <form class="needs-validation" novalidate action="{{route('content.update',$edit->id)}}" method="post">
                                        	@csrf
                                            @method('put')
                                           
                                            <div class="form-group mb-3">
                                                <label for="">Title</label>
                                                <input type="text" class="form-control" id="" name="title" value="{{$edit->title}}" required>
                                                <div class="invalid-feedback">
                                                    Please provide a valid Title Name.
                                                </div>
                                            </div>
                                            <div class="form-group mb-3">
                                        <label for="product-description">Content Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control" id="product-description" name="description" rows="5" placeholder="Please enter description">{{$edit->description}}</textarea>
                                    </div>
                                            <button class="btn btn-primary" type="submit">Update</button>
                                        </form>

                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

            </div>

 @endsection

 @section('footer-js')
    <!-- Summernote js -->
        <script src="{{URL::to('admin/libs/summernote/summernote-bs4.min.js')}}"></script>
       
        <!-- Init js -->
        <script src="{{URL::to('admin/js/pages/add-product.init.js')}}"></script>
 @endsection