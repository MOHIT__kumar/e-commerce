  <!-- Vendor js -->
        <script src="{{URL::to('admin/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{URL::To('admin/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{URL::to('admin/libs/apexcharts/apexcharts.min.js')}}"></script>

        <script src="{{URL::to('admin/libs/selectize/js/standalone/selectize.min.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{URL::to('admin/js/pages/dashboard-1.init.js')}}"></script>

        <!-- App js-->
        <script src="{{URL::to('admin/js/app.min.js')}}"></script>

         <!-- Tost-->
        <script src="../assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>

        <!-- toastr init js-->
        <script src="{{URL::to('admin/js/pages/toastr.init.js')}}"></script>

        @yield('footer-js')

