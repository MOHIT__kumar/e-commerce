 <!-- App favicon -->
        <link rel="shortcut icon" href="{{URL::To('admin/images/favicon.ico')}}">

        <!-- Plugins css -->
        <link href="{{URL::to('admin/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('admin/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="{{URL::to('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="{{URL::to('admin/css/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="{{URL::to('admin/css/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
        <link href="{{URL::to('admin/css/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

        <!-- icons -->
        <link href="{{URL::to('admin/css/icons.min.css')}}" rel="stylesheet" type="text/css" />

         <!-- Jquery Toast css -->
        <link href="{{URL::to('admin/libs/jquery-toast-plugin/jquery.toast.min.css')}}" rel="stylesheet" type="text/css" />

        @yield('page-css')
