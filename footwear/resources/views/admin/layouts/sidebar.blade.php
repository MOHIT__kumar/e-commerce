  <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="h-100" data-simplebar>

                 

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul id="side-menu">

                            <li class="menu-title">Navigation</li>
                
                            <li>
                                <a href="#sidebarDashboards" data-toggle="collapse">
                                    <i data-feather="airplay"></i>
                                    <span class="badge badge-success badge-pill float-right"></span>
                                    <span> Dashboard </span>
                                </a>
                            </li>

                            <li class="menu-title mt-2">Menu's</li>

                            <li>
                                <a href="{{route('university.index')}}">
                                    <i data-feather="message-square"></i>
                                    <span> University </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('category.index')}}">
                                    <i data-feather="calendar"></i>
                                    <span> Categories </span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('content.index')}}">
                                    <i data-feather="message-square"></i>
                                    <span> Content </span>
                                </a>
                            </li>
                            
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->