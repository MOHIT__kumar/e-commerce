@extends('admin.layouts.app')

@section('page-css')
 <!-- third party css -->
        <link href="admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="admin/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="admin/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="admin/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')
 <div class="content-page">
 	 <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <a href="{{route('category.create')}}" class="btn btn-info">Create category</a>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Category List</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- <h4 class="header-title">Category LIst</h4> -->

                                        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap w-100">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Categories</th>
                                                    <th>Subcategories</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        
                                        
                                            <tbody>
                                                @php $i = 1; @endphp
                                              @foreach( $indexes as $index )
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$index->categories}}</td>
                                                    <td>{{$index->subcategory}}</td>
                                                    <td>
                                                        @if($index->status == 1)
                                                        <span class="text-success"><strong>Active</strong></span>
                                                        @else
                                                        <span class="text-danger"><strong>Inactive</strong></span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('category.edit',$index->id)}}" class="btn btn-warning btn-xs">Edit</a>

                                                        @if($index->status == 1 )
                                                        <a href="{{route('category.show',$index->id)}}" class="btn btn-danger btn-xs">Inactive</a>
                                                        @else
                                                        <a href="{{route('category.show',$index->id)}}" class="btn btn-success btn-xs">Active</a>
                                                        @endif
                                                    </td>
                                                </tr>                       
                                              @endforeach                        
                                            </tbody>
                                        </table>
                                        
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->



                        
                    </div> <!-- container -->

                </div> <!-- content -->
 </div>
@endsection

@section('footer-js')
 <!-- third party js -->
        <script src="admin/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="admin/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="admin/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="admin/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <script src="admin/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="admin/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="admin/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="admin/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="admin/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="admin/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="admin/libs/datatables.net-select/js/dataTables.select.min.js"></script>
        <script src="admin/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="admin/libs/pdfmake/build/vfs_fonts.js"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="admin/js/pages/datatables.init.js"></script>
         @endsection