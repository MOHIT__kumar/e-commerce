@extends('admin.layouts.app')
@section('page-css')
@endsection
@section('content')
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<div class="content-page">
   <div class="content">
      <!-- Start Content-->
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box">
                  <div class="page-title-right">
                     <form class="form-inline">
                        <div class="form-group">
                           <div class="input-group input-group-sm">
                              <input type="text" class="form-control border" id="dash-daterange">
                              <div class="input-group-append">
                                 <span class="input-group-text bg-blue border-blue text-white">
                                 <i class="mdi mdi-calendar-range"></i>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                        <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                        <i class="mdi mdi-filter-variant"></i>
                        </a>
                     </form>
                  </div>
                  <h4 class="page-title">Dashboard</h4>
               </div>
            </div>
         </div>
         <!-- end page title --> 
         <div class="row">
            <div class="col-lg-12" style="width:100%; height: 100%;">
               <div class="card-box pb-2">
                  <img src="{{URL::to('admin\images\bg.jpeg')}}" style="width: 1179px;
                     ">
               </div>
               <!-- end card-box -->
            </div>
            <!-- end col-->
         </div>
         <!-- end row -->
      </div>
      <!-- container -->
   </div>
   <!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->
@endsection