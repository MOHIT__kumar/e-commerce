<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
/* Admin Category Controller */
Route::resource('category','Admin\CategoryController');
/* Admin Unversity Controller */
Route::resource('university','Admin\UniversityController');
/* Admin Content Controller */
Route::resource('content','Admin\ContentController');

