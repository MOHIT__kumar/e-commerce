<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = ['title','description'];

    public $primaryKey = 'id';

    protected $table = 'contents';
}
