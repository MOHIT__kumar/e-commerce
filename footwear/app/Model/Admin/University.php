<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $fillable = ['university_name','status'];
   
    public $primaryKey = 'id';
    
    protected $table = 'universities';
}
