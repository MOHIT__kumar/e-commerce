<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
USe App\Model\Admin\Category;
USe Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indexes = Category::all();
        return view('admin.category.index',compact('indexes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
        'category'    => 'required',
        'subcategory' => 'required'
    ]);

       $category = Category::create([
        'categories'    => $request->category,
        'subcategory' => $request->subcategory
    ]);

       if ($category) {
           Session::flash('success','Category Created Successfully');
       }
       else{
        Session::flash('error','Category not created');
       }

       return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $delete = Category::find($id);

        if ($delete->status == 1 ) {
           $delete->status = '0';
           $delete->save();
        }
        else{
           $delete->status = '1';
           $delete->save();
        }
        return redirect()->route('category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $update = Category::find($id);
        return  view('admin.category.edit',compact('update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'category'    => 'required',
        'subcategory' => 'required'
    ]);

       $category = Category::where('id',$id)->update([
        'categories'  => $request->category,
        'subcategory' => $request->subcategory
    ]);

       if ($category) {
           Session::flash('success','Category Created Successfully');
       }
       else{
        Session::flash('error','Category not created');
       }
       
       return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
