<?php

namespace App\Http\Controllers\Sliders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Slider\Slider;
use Session;
use DB;

class SliderController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function view()
    {	
    	$sliders = DB::table('sliders')->get();
    	 // dd($sliders);
    	return view('sliders.view',compact('sliders'));
    }

    public function create()
    {
    	return view('sliders.create');
    }

    public function store(request $request)
    {
    	// dd($request->all());
    	$request->validate([
    		'image'  => 'required',
    	]);

    	$image_name = time().'_'.$request->image->getClientOriginalName();
    	$request->image->move('admin/image/sliders',$image_name);
    	
    	Slider::create([
    		'image' => $image_name,
    	]);

    	Session::flash('success','Image uploaded successfully');
    	return back();
    }

    public function edit($id)
    {
    	$data = Slider::find($id);
    	return view('sliders.edit',compact('data'));
    }

    public function update(request $request, $id)
    {
    	// dd($request->all());
    	$request->validate([
    		'image'  => 'required',
    	]);

    	$image_name = time().'_'.$request->image->getClientOriginalName();
    	$request->image->move('admin/image/sliders',$image_name);
    	
    	Slider::where('id',$id)->update([
    		'image' => $image_name,
    	]);

    	Session::flash('success','Image updated successfully');
    	return redirect()->route('sliders/view');
    }

    public function delete($id)
    {
    	DB::table('sliders')->where('id',$id)->delete();
    	return redirect()->route('sliders/view');
    }
}
