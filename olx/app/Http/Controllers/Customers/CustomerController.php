<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use DB;

class CustomerController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
    	$customers = DB::Table('users')->get();
    	return view('Customer.view',compact('customers'));
    }

    public function delete($id)
    {
    	$data = User::find($id);
    	 if ($data->status == 1)
    	  {
    	 	$data->status = 0;
    	 	$data->save();
    	 	Session::flash('error','Customer inactivated');
    	 }
    	 else
    	 {
    	 	$data->status = 1;
    	 	$data->save();
    	 	Session::flash('success','Customer activated');
    	 }
    	 return back();
    }
}
