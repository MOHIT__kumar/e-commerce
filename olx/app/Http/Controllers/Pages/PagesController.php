<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Pages\Pages;
use Carbon\Carbon;
use Session;
use DB;

class PagesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
    	$contents = Pages::all();
    	return view('Pages.view',compact('contents'));
    }

    public function create()
    {
    	return view('Pages.create');
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'title'  	   => 'required',
    		'description'  => 'required',
    	]);

    	Pages::create([
    		'title'        => $request->title,
    		'description'  => $request->description
    	]);

    	Session::flash('success','Content created successfully');
    	return back();
    }

    public function edit($id)
    {
    	$data = Pages::find($id);
    	return view('Pages.edit',compact('data'));
    }

    public function update(request $request,$id)
    {
    	$request->validate([
    		'title'  	  => 'required',
    		'description' => 'required'
    	]);

    	Pages::where('id',$id)->update([
    		'title'        => $request->title,
    		'description'  => $request->description,
    	]);
    	Session::flash('success','Content updated succesfully');
    	return redirect()->route('pages/view');
    }

    public function delete($id)
    {
        DB::table('pages')->where('id',$id)->delete();
    	return back();
    }

    public function faq_view()
    {
        $faqs = DB::table('faq')->get();
        return view('Pages.faq_view',compact('faqs'));
    }

    public function faq_store(request $request)
    {
        // dd($request->all());
        $request->validate([
            'question'  => 'required',
            'answer'    => 'required'
        ]);

        DB::table('faq')->insert([
            'questions'  => $request->question,
            'answers'    => $request->answer,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        Session::flash('success','Faq saved successfully');
        return back();
    }

    public function faq_edit($id)
    {
        $data = DB::table('faq')->where('id',$id)->first();
        return view('Pages.faq_edit',compact('data'));
    }

    public function faq_update(request $request,$id)
    {
        $request->validate([
            'question'   => 'required',
            'answer'     => 'required'
        ]);

        DB::table('faq')->where('id',$id)->update([
            'questions'   => $request->question,
            'answers'     => $request->answer,
        ]);
        Session::flash('success','FAQ updated successfully');
        return redirect()->route('pages/faq/view');
    }

    public function faq_delete($id)
    {
        DB::table('faq')->where('id',$id)->delete();
        Session::flash('error','Faq deleted successfully');
        return back();
    }
}
