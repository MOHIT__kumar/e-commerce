<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Category;
use App\Model\Category\Filters;
use Session;
use DB;

class FilterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
    	$filters = DB::table('filter_masters')->join('categories','filter_masters.category_id','categories.id')
    										  ->join('categories as subcategories','filter_masters.subcategory_id','subcategories.id')
    										  ->select('filter_masters.*','categories.title as category','subcategories.title as subcategory')
    										  ->get();
    	 // dd($filters);
    	return view('Filter.view',compact('filters'));
    }

    public function create()
    {
    	$categories = DB::table('categories')->where('status',1)->where('parent_id',null)->get();
    	 // dd($categories);
    	return view('Filter.create',compact('categories'));
    }

    public function subcategory($id)
    {
    	$subcategory = DB::table('categories')->where('status',1)->where('parent_id',$id)->pluck('title','id');
    	return response()->json($subcategory);
    }

    public function store(request $request)
    {
    	$request->validate([
    		'category_id'     => 'required',
    		'subcategory_id'  => 'required',
    		'filter'		  => 'required'
    	]);

    	Filters::create([
    	 'category_id'      => $request->category_id,
    	 'subcategory_id'	=> $request->subcategory_id,
    	 'name'				=> $request->filter,
    	]);
    	Session::flash('success','Filter created successfully');
    	return back();
    }

     public function edit($id)
    {
    	$data = Filters::find($id);
    	$categories = Category::where('status',1)->where('parent_id',null)->get();
    	return view('Filter.edit',compact('data','categories'));
    }

    public function update(request $request,$id)
    {
    	$request->validate([
    		'category_id'     => 'required',
    		'subcategory_id'  => 'required',
    		'filter'		  => 'required'
    	]);

    	Filters::where('id',$id)->update([
    	 'category_id'      => $request->category_id,
    	 'subcategory_id'	=> $request->subcategory_id,
    	 'name'				=> $request->filter,
    	]);
    	Session::flash('success','Filter updated successfully');
    	return redirect()->route('filters/view');
    }

    public function delete($id)
    {
    	$data = Filters::find($id);
    	 if ($data->status == 1)
    	  {
    	 	$data->status = 0;
    	 	$data->save();
    	 	Session::flash('This filter is disabled successfully');
    	 }
    	  else
    	  {
    	  	$data->status = 1;
    	 	$data->save();
    	 	Session::flash('This filter is enabled successfully');
    	  }
    	  return back();
    }
}
