<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category\Category;
use App\Model\Category\Subcategory;
use Session;
use DB;

class CategoryController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


    public function view()
    {
      $categories = Category::where('parent_id','=',Null)->get();
    	return view('Category.view',compact('categories'));
    }

    public function create()
    {
    	return view('Category.create');
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'category'   => 'required:unique:categories',
        'image'      => 'required',
    	]);

      $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$request->image->getClientOriginalName();
      $request->image->move('admin/image/category', $image_name);

    	Category::create([
    		'title'      => $request->category,
        'image'      => $image_name,
    	]);

    	Session::flash('success','Category created successfully');
    	return back();
    }

    public function edit($id)
    {
        $data = Category::find($id);
        return view('Category.edit',compact('data'));
    }

    public function update(request $request,$id)
    {
       // dd($request->all());
       $request->validate([
        'category'  => 'required',
       ]);


        if ($request->image)
         {
          $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$request->image->getClientOriginalName();
          $request->image->move('admin/image/category', $image_name);
        }
        else{
           $image = Category::find($id);
           $image_name = $image->image;
        }

       Category::where('id',$id)->update([
        'title'      => $request->category,
        'image'      => $image_name,
       ]);
       Session::flash('success','Category updated successfully');
       return redirect()->route('category/view');
    }

    public function delete($id)
    {
        $data = Category::find($id);

          if ($data->status == 1) 
          {
             $data->status = 0;
             $data->save();
             Session::flash('error','Category inactivated');
          }
          else
           {
             $data->status = 1;
             $data->save();
             Session::flash('success','Category activated');
           }
           return back();
    }

    public function subcategory_view()
    {
      $subcategories = DB::Table('categories')->where('categories.parent_id','!=',Null)
                                              ->leftJoin('categories as category','category.id','categories.parent_id')
                                              ->select('categories.*','category.title as category')
                                              ->get();

      return view('Subcategory.view',compact('subcategories'));
    }

    public function subcategory_create()
    {
      $categories = Category::where('parent_id','=',Null)->where('status',1)->get();
      return view('Subcategory.create',compact('categories'));
    }

    public function subcategory_store(request $request)
    {
      $request->validate([
        'category_id'  => 'required',
        'subcategory'  => 'required',
        'image'        => 'required',
      ]);

      $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$request->image->getClientOriginalName();
      $request->image->move('admin/image/subcategory', $image_name);

      Category::create([
        'parent_id'    => $request->category_id,
        'title'        => $request->subcategory,
        'image'        => $image_name,
      ]);

      Session::flash('success','Subcategory created successfully');
      return back();
    }

    public function subcategory_edit($id)
    {
      $data = Category::find($id);
      $categories = Category::where('parent_id','=',Null)->where('status',1)->get();
      return view('Subcategory.edit',compact('data','categories'));
    }

    public function subcategory_update(request $request,$id)
    {

      $request->validate([
        'category_id'  => 'required',
        'subcategory'  => 'required',
      ]);

      if ($request->image)
       {
        $image_name  =strtotime(date('Y-m-d H:i:s')).'_'.$request->image->getClientOriginalName();
        $request->image->move('admin/image/subcategory', $image_name);
      }
      else{
        $image = Category::where('id',$id)->first();
         $image_name = $image->image;
      }

      

      Category::where('id',$id)->update([
        'parent_id'    => $request->category_id,
        'title'        => $request->subcategory,
        'image'        => $image_name,
      ]);

      Session::flash('success','Subcategory updated successfully');
      return redirect()->route('subcategory/view');
    }
    public function subcategory_delete($id)
    {
      $data = Category::find($id);
       if ($data->status == 1)
        {
          $data->status = 0;
          $data->save();
          Session::flash('error','Subcategory inactivated');
       }
       else
       {
        $data->status = 1;
        $data->save();
        Session::flash('success','Subcategory activated');
       }
       return redirect()->route('subcategory/view');
    }
}
