<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use DB;

class PDFController extends Controller
{
     public function generatePDF()
    {
        $customers = DB::table('users')->get();
         // dd($data);
          
        $pdf = PDF::loadView('Customer.view', $customers);
        dd($pdf);
        return $pdf->download('itsolutionstuff.pdf');
    }
}
