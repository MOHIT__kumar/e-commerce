<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id','title','image','description','status'];

    protected $table = 'categories';

    public $primaryKey = 'id';
}
