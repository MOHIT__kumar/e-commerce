<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class Filters extends Model
{
    protected $fillable = ['category_id','subcategory_id','name','status'];

    protected $table = 'filter_masters';

    public $primaryKey = 'id';
}
