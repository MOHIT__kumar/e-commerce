<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable = ['page_type','title','image','description','status'];

    protected $table = 'pages';

    public $primaryKey = 'id';
}
