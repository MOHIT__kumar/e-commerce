<?php

namespace App\Model\Slider;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['image'];

    protected $table = 'sliders';

    public $primaryKey = 'id';
}
