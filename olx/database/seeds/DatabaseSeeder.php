<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        for ($i=0; $i <10 ; $i++) 
        { 
        	DB::table('users')->insert([
        		'name'      => Str::random(6),
        		'email'     => Str::random(5).'@admin.com',
        		'mobile'    => rand(7000000000,9999999999),
        		'address'   => Str::random(7),
        		'address2'  => Str::random(10),
        		'pincode'   => rand(1000000,9999999),
        		'password'  => 'admin',
        	]);
        }
    }
}
