<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('is_admin')->default(0);
            $table->string('name');
            $table->string('mobile');
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('address2')->nullable();
            $table->string('pincode')->nullable();
            $table->string('status')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name'       => 'Admin',
            'mobile'     => '9988776655',
            'email'      => 'admin@admin.com',
            'address'    => 'test address line',
            'address2'   => 'address, address-city',
            'pincode'    => '123456',
            'password'   => Hash::make('admin'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
