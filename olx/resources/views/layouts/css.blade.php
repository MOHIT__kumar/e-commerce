    <link rel="icon" type="image/x-icon" href="{{URL::to('admin/assets/img/favicon.ico')}}"/>
    <link href="{{URL::to('admin/assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{URL::to('admin/assets/js/loader.js')}}"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonadmin/ts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{URL::to('admin/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::to('admin/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{URL::to('admin/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::to('admin/assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />

      @yield('page-css')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{URL::to('admin/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <!-- toastr -->
    <link href="{{URL::to('admin/plugins/notification/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    
