 <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{URL::to('admin/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{URL::to('admin/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{URL::to('admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{URL::to('admin/assets/js/app.js')}}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{URL::to('admin/assets/js/custom.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="{{URL::to('admin/plugins/apex/apexcharts.min.js')}}"></script>
    <script src="{{URL::to('admin/assets/js/dashboard/dash_1.js')}}"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

      @yield('page-js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{URL::to('admin/assets/js/scrollspyNav.js')}}"></script>
    <!-- toastr -->
    <script src="{{URL::to('admin/plugins/notification/snackbar/snackbar.min.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    <script src="{{URL::to('admin/assets/js/components/notification/custom-snackbar.js')}}"></script>
    <!--  END CUSTOM SCRIPTS FILE  -->

      @include('layouts.notification')
