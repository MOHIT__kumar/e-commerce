<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
      @include('layouts.css')
</head>
<body>

    <!-- BEGIN LOADER -->
   <!--  <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div> -->
    <!--  END LOADER -->

     @include('layouts.navbar')

      @yield('breadcrumb')

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

         @include('layouts.sidebar')
        
           <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
             @yield('content')
             @include('layouts.footer')
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

   @include('layouts.js')
</body>
</html>