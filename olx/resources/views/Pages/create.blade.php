  @extends('layouts.app')

  @section('title','Content | Create')

  @section('page-css')
   <link href="{{URL::to('admin/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
   <!-- BEGIN PAGE LEVEL STYLE -->
    <link rel="stylesheet" href="{{URL::to('admin/plugins/editors/markdown/simplemde.min.css')}}">
    <!-- END PAGE LEVEL STYLE -->
  @endsection

  @section('breadcrumb')
     <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Create</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><span>Pages</span></li>
                            </ol>
                        </nav>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav flex-row ml-auto">
             <li>
              <div class="page-header mr-4">
                <nav class="breadcrumb-one" aria-label="breadcrumb">
                 <ol class="breadcrumb">
                   <a href="{{URL::to('pages/view')}}" class="btn btn-info btn-sm ml-5">Pages</a> 
                 </ol>
                </nav>
              </div>
             </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
  @endsection

  @section('content')
    <!--  BEGIN CONTENT AREA  -->
            <div class="container mt-4">
                <div class="container">
                    <div class="row">
                        <div id="flRegistrationForm" class="col-lg-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">                                
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4>Add Pages</h4>
                                        </div>                                                                        
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
                                    <form action="{{URL::to('pages/store')}}" method="post" enctype="multipart/form-data">
                                      @csrf
                                      <div class="form-group">
                                      <input type="text" name="page_type" placeholder="Enter Page Type*" class="form-control">
                                      </div>
                                        <div class="form-group mb-4">
                                            <input type="text" name="title" class="form-control" placeholder="Enter Title *">
                                        </div>
                                          
                                        <div class="form-group mb-4">
                                         <div class="widget-content widget-content-area">
                                          <textarea id="demo1" type="text" name="description" class="form-control" placeholder="Enter Description *">   
                                          </textarea>

                                         </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary mt-3">Create</button>
                                    </form>
                                </div>
                            </div>
                        </div>                       
                    </div>                    
                </div>
            </div>            
        <!--  END CONTENT AREA  -->
  @endsection

  @section('page-js')
    <script src="{{URL::to('admin/plugins/highlight/highlight.pack.js')}}"></script>
    <script src="{{URL::to('admin/assets/js/scrollspyNav.js')}}"></script>
     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{URL::to('admin/plugins/editors/markdown/simplemde.min.js')}}"></script>
    <script src="{{URL::to('admin/plugins/editors/markdown/custom-markdown.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
  @endsection