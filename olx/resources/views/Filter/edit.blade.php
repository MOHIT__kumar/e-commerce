  @extends('layouts.app')

  @section('title','Filters | Update')

  @section('page-css')
   <link href="{{URL::to('admin/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
  @endsection

  @section('breadcrumb')
     <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Update</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><span>Filters</span></li>
                            </ol>
                        </nav>
                    </div>
                </li>
            </ul>
             <ul class="navbar-nav flex-row ml-auto">
                <li>
                    <div class="page-header mr-4">
                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <a href="{{URL::to('filters/view')}}" class="btn btn-info btn-sm ml-5">Filters</a> 
                            </ol>
                        </nav>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
  @endsection

  @section('content')
    <!--  BEGIN CONTENT AREA  -->
            <div class="container mt-4">
                <div class="container">
                    <div class="row">
                        <div id="flRegistrationForm" class="col-lg-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">                                
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4>Update Filters</h4>
                                        </div>                                                                        
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
                                    <form action="{{URL::to('filters/update',$data->id)}}" method="post" enctype="multipart/form-data">
                                      @csrf
                                        <div class="form-group mb-4">
                                            <select type="text" name="category_id" id="category" class="form-control">
                                              <option>Select Category</option>
                                               @foreach($categories as $category)
                                                @if($category->id == $data->category_id)
                                                 <option value="{{$category->id}}" selected="">{{$category->title}}</option>
                                                @else
                                                 <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endif
                                               @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group mb-4">
                                            <select type="text" name="subcategory_id" id="subcategory" class="form-control">
                                               @php $subcats = DB::table('categories')->whereNotNull('parent_id')->get(); @endphp
                                                @foreach($subcats as $subcat)
                                                 @if($data->subcategory_id == $subcat->id)
                                                  <option value="{{$subcat->id}}" selected="">{{$subcat->title}}</option>
                                                 @else
                                                  <option value="{{$subcat->id}}">{{$subcat->title}}</option>
                                                 @endif
                                                @endforeach
                                            </select>
                                        </div>

                                         <div class="form-group mb-4">
                                          <input type="text" name="filter" value="{{$data->name}}" class="form-control" placeholder="Filter Name*">
                                        </div>

                                        <button type="submit" class="btn btn-primary mt-3">Create</button>
                                    </form>
                                </div>
                            </div>
                        </div>                       
                    </div>                    
                </div>
            </div>            
        <!--  END CONTENT AREA  -->
  @endsection

  @section('page-js')
    <script src="{{URL::to('admin/plugins/highlight/highlight.pack.js')}}"></script>
    <script src="{{URL::to('admin/assets/js/scrollspyNav.js')}}"></script>
    <script type="text/javascript">
      $('#category').change(function(){
        var id = $('#category').val();
         $.ajax({
            url : "{{URL::to('filters/subcategory')}}"+'/'+id,
            type:"GET",
            success:function(data)
            {
                $('#subcategory').empty();
                $.each(data,function(key,value){
                $('#subcategory').append("<option>Select Subcategory</option>");
                $('#subcategory').append("<option value="+key+">"+value+"</option>");
              });
            }
         });
      });
    </script>
  @endsection