<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

// Route::get('users/pdf', [PDFController::class, 'generatePDF']);

 // Categories Manage
Route::get('category/view','Category\CategoryController@view')->name('category/view');
Route::get('category/create','Category\CategoryController@create');
Route::post('category/store','Category\CategoryController@store');
Route::get('category/edit/{id}','Category\CategoryController@edit');
Route::put('category/update/{id}','Category\CategoryController@update');
Route::get('category/delete/{id}','Category\CategoryController@delete');
  //  Sub Category Management
Route::get('subcategory/view','Category\CategoryController@subcategory_view')->name('subcategory/view');
Route::get('subcategory/create','Category\CategoryController@subcategory_create');
Route::post('subcategory/store','Category\CategoryController@subcategory_store');
Route::get('subcategory/edit/{id}','Category\CategoryController@subcategory_edit');
Route::put('subcategory/update/{id}','Category\CategoryController@subcategory_update');
Route::get('subcategory/delete/{id}','Category\CategoryController@subcategory_delete');
 // Users
Route::get('users/view','Customers\CustomerController@view')->name('users/view');
Route::get('customer/delete/{id}','Customers\CustomerController@delete');
 // Pages
Route::get('pages/view','Pages\PagesController@view')->name('pages/view');
Route::get('pages/create','Pages\PagesController@create');
Route::post('pages/store','Pages\PagesController@store');
Route::get('pages/edit/{id}','Pages\PagesController@edit');
Route::put('pages/update/{id}','Pages\PagesController@update');
Route::get('pages/delete/{id}','Pages\PagesController@delete');
 // Faq Pages
 Route::get('pages/faq/view','Pages\PagesController@faq_view')->name('pages/faq/view');
 Route::post('pages/faq/store','Pages\PagesController@faq_store');
 Route::get('pages/faq/edit/{id}','Pages\PagesController@faq_edit');
 Route::put('pages/faq/update/{id}','Pages\PagesController@faq_update');
 Route::get('pages/faq/delete/{id}','Pages\PagesController@faq_delete');
 // Filters
 Route::get('filters/view','Category\FilterController@view')->name('filters/view');
 Route::get('filters/create','Category\FilterController@create');
 Route::post('filters/store','Category\FilterController@store');
 Route::get('filters/edit/{id}','Category\FilterController@edit');
 Route::post('filters/update/{id}','Category\FilterController@update');
 Route::get('filters/delete/{id}','Category\FilterController@delete');
 Route::get('filters/subcategory/{id}','Category\FilterController@subcategory');
 // Sliders
 Route::get('sliders/view','Sliders\SliderController@view')->name('sliders/view');
 Route::get('sliders/create','Sliders\SliderController@create');
 Route::post('sliders/store','Sliders\SliderController@store');
 Route::get('sliders/edit/{id}','Sliders\SliderController@edit');
 Route::post('sliders/update/{id}','Sliders\SliderController@update');
 Route::get('sliders/delete/{id}','Sliders\SliderController@delete');
